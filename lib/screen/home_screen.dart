import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/data_core.dart';
import 'package:flutter_app/screen/firstdate/canlamsan_screen.dart';
import 'package:flutter_app/screen/firstdate/chamsocchung_tongquat_screen.dart';
import 'package:flutter_app/screen/firstdate/chamsocchuyenbiet_screen.dart';
import 'package:flutter_app/screen/firstdate/chucnangnuot_screen.dart';
import 'package:flutter_app/screen/firstdate/danhgia_screen.dart';
import 'package:flutter_app/screen/firstdate/dieutricap_screen.dart';
import 'package:flutter_app/screen/firstdate/empty_screen.dart';
import 'package:flutter_app/screen/firstdate/thamkham_screen.dart';
import 'package:flutter_app/screen/general/nihss_screen.dart';
import 'package:flutter_app/screen/right_bar_screen.dart';
import 'package:flutter_app/widget/style.dart';
import 'package:responsive_scaffold/responsive_scaffold.dart';

class HomeScreen extends StatefulWidget {
  @override
  State createState() {
    return HomeScreenState();
  }
}

class HomeScreenState extends State<HomeScreen> {
  List<Widget> bodyList = [
    Container(),
    DanhGiaScreen(),
    ThamKhamScreen(),
    CanLamSanScreen(),
    ChucNangNuotScreen(),
    ChamSocChungTongQuat(),
    ChamSocChuyenBietScreen(),
    DieuTriCapScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
    EmptyScreen(),
  ];

  ValueNotifier<int> _pageIndex = ValueNotifier<int>(0);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageIndex.value = 1;
  }

  @override
  Widget build(BuildContext context) {
    return ResponsiveScaffold(
      title: ValueListenableBuilder<int>(
          valueListenable: _pageIndex,
          builder: (_, value, child) {
            return Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Text('BN: Le Quang Linh'),
                ),
                Text(DataCore().get1MenuItems()[value].title),
                Container()
              ],
            );
          }),
      body: Container(

        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [_leftNavigationBar(), _bodyContain(), _rightNavigateBar()],
        ),
      ),
    );
  }

  _bodyContain() {
    return Expanded(
        child: ValueListenableBuilder<int>(
            valueListenable: _pageIndex,
            builder: (_, value, child) {
              if (value == 0 || value == 1) {
                return DanhGiaScreen();
              }else if(value == 2 ){
                return ThamKhamScreen();
              }else if(value == 3){
                return CanLamSanScreen();
              }else if(value == 4){
                return ChucNangNuotScreen();
              }else if(value == 5){
                return ChamSocChungTongQuat();
              }else if(value == 6){
                return ChamSocChuyenBietScreen();
              }else if(value == 7){
                return DieuTriCapScreen();
              }
              return EmptyScreen();
            }));
  }

  _rightNavigateBar(){
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      width: 300,
      child: RightBarScreen(),
    );
  }

  _leftNavigationBar() {
    return Container(
      width: 250,
      height: MediaQuery
          .of(context)
          .size
          .height,
      child: SingleChildScrollView(
        child: Column(
          children: List.generate(DataCore()
              .get1MenuItems()
              .length, (index) {
            var item = DataCore().get1MenuItems()[index];

            return ValueListenableBuilder<int>(
                valueListenable: _pageIndex,
                builder: (_, value, child) {
                  return FlatButton(
                    color: item.isSection ? Colors.blue[200] : value == index ? Colors.orange : Colors.grey[100],
                    shape: RoundedRectangleBorder(
                      side: BorderSide(width: 0.4, color: Colors.grey),
                      borderRadius: new BorderRadius.circular(0.0),
                    ),
                    onPressed: () {
                      _pageIndex.value = index;
                    },
                    child: Container(
                      alignment: Alignment.centerLeft,
                      margin: EdgeInsets.only(left: item.isSection ? 0 : 16),
                      padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
                      child: Text(
                        DataCore().get1MenuItems()[index].title,
                        style: TextStyle(color: Colors.black),
                      ),
                    ),
                  );
                });
          }),
        ),
      ),
    );
  }
}
