import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_yn_show_textfield_widget.dart';
import 'package:flutter_app/widget/save_button.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class CanLamSanScreen extends StatefulWidget {
  VoidCallback callback;

  CanLamSanScreen({this.callback});

  @override
  State createState() {
    return _CanLamSanScreenState();
  }
}

class _CanLamSanScreenState extends State<CanLamSanScreen> {
  ValueNotifier<bool> _ctm = ValueNotifier<bool>(false);
  ValueNotifier<bool> _batthuong = ValueNotifier<bool>(false);
  ValueNotifier<bool> _vs = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dmtb = ValueNotifier<bool>(false);
  ValueNotifier<bool> _batthuongdmtb = ValueNotifier<bool>(false);
  ValueNotifier<bool> _mentim = ValueNotifier<bool>(false);
  ValueNotifier<bool> _kmdm = ValueNotifier<bool>(false);
  ValueNotifier<bool> _sgot = ValueNotifier<bool>(false);
  ValueNotifier<bool> _bun = ValueNotifier<bool>(false);
  ValueNotifier<bool> _glucose = ValueNotifier<bool>(false);
  ValueNotifier<bool> _batthuongglucose = ValueNotifier<bool>(false);
  ValueNotifier<bool> _batthuonggdiengiaido = ValueNotifier<bool>(false);
  ValueNotifier<bool> _diengiaido = ValueNotifier<bool>(false);
  ValueNotifier<bool> _khac = ValueNotifier<bool>(false);

  ValueNotifier<bool> _khacLactacte = ValueNotifier<bool>(false);
  ValueNotifier<bool> _khacCrp = ValueNotifier<bool>(false);
  ValueNotifier<bool> _khacDocChat = ValueNotifier<bool>(false);
  ValueNotifier<bool> _khacT = ValueNotifier<bool>(false);



  ValueNotifier<bool> _vomhoanh = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              _ctmUI(),
              _vsUI(),
              _dmtbUI(),
              _mentinUI(),
              _kmdbUI(),
              _sgopUI(),
              _bunUI(),
              _glucoseUI(),
              _diengiaidoUI(),
              _xnKhacUI(),
              _ecgUI(),
              _xqPhoiUI(),
              _hinhanhnaoUI(),
              SaveWidget(
                callback: () {},
              )
            ],
          ),
        ),
      ),
    );
  }

  _hinhanhnaoUI() {
    ValueNotifier<bool> hiem = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Hình ảnh não và mạch máu não',
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: 'Chẩn đoán',
                options: ['Nhồi máu não', 'Xuất huyết não ', 'Xuất huyết màng não', 'Huyết khối tĩnh mạch não'],
                id: "1006"),
            callback: (idx, select, indexes) {},
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: 'Loại trừ',
                options: [
                  'Epidural hematoma',
                  'Subdural hematoma',
                  'Traumatic brain hemorrhage',
                  'Encephalitis',
                  'Abscess and cyst',
                  'Brain tumor',
                  'Multiple sclerosis',
                  'Migraine with aur',
                  'Syncope',
                  'Transient global am',
                  'Toxic and metabolic disorders',
                  'Hyponatremia',
                  'Hypoglycemia',
                  'Seizure and postictal state',
                  'Peripheral nerve diseases (rare)',
                  'Sepsis',
                  'Other rare conditions'
                ],
                id: "1006"),
            callback: (idx, select, indexes) {
              if (indexes.contains(16)) {
                hiem.value = true;
              } else {
                hiem.value = false;
              }
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
              valueListenable: hiem,
              builder: (_, value, child) {
                return value
                    ? QuestionTextWidget(
                        questionItem: QuestionItem(title: "Chẩn đoán"),
                        callback: (value) {},
                      )
                    : Container();
              },
            ),
          ),
        ],
      ),
    );
  }

  _ctmUI() {
    return Container(
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'CTM', options: ['Có', 'Chỉ định XN'])),
    );
  }

  _vsUI() {
    ValueNotifier<bool> giatri = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'VS', options: ['Có', 'Chỉ định XN'])),
    );
  }

  _mentinUI() {
    ValueNotifier<bool> giatri = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'Men tim', options: ['Có', 'Chỉ định XN'])),

    );
  }

  _kmdbUI() {
    ValueNotifier<bool> giatri = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'KMĐM', options: ['Có', 'Chỉ định XN'])),

    );
  }

  _sgopUI() {
    ValueNotifier<bool> giatri = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'SGOP', options: ['Có', 'Chỉ định XN'])),

    );
  }

  _bunUI() {
    ValueNotifier<bool> giatri = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'BUN, Creatinine, eGFR', options: ['Có', 'Chỉ định XN'])),
    );
  }

  _glucoseUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'Glucose', options: ['Có', 'Chỉ định XN'])),

    );
  }

  _diengiaidoUI() {
    ValueNotifier<bool> giatri = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          QuestionMultipleWidget(questionItem: QuestionItem(title: 'Điện giải đồ   ', options: ['Có', 'Chỉ định XN']),),
        ],
      ),
    );
  }

  _xnKhacUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          QuestionMultipleWidget(
            questionItem: QuestionItem(title: 'Xét nghiệm khác', options: ['Có', 'Chỉ định XN']),
            callback: (index, select, indexes) {
              if (indexes.contains(index)) {
                _khac.value = true;
              }else{
                _khac.value = false;
              }
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
                valueListenable: _khac,
                builder: (_, value, child) {
                  return value
                      ? Column(
                          children: [
                            Row(
                              children: [
                                QuestionMultipleWidget(
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'Lactate', 'CRP','Độc chất máu', 'Độc chất nước tiểu', 'T3T4TSH',
                                      ],
                                      id: "1006"),
                                ),

                              ],
                            ),
                          ],
                        )
                      : Container();
                }),
          )
        ],
      ),
    );
  }

  _ecgUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          QuestionMultipleWidget(
            questionItem: QuestionItem(title: 'ECG', options: ['Có', 'Chỉ định XN']),
            callback: (index, select, indexes) {

            },
          ),
        ],
      ),
    );
  }

  _xqPhoiUI() {

    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          QuestionMultipleWidget(
            questionItem: QuestionItem(title: 'XQ tim phổi', options: ['Có', 'Chỉ định XN']),
            callback: (index, select, indexes) {

            },
          ),
        ],
      ),
    );
  }



  _dmtbUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: QuestionMultipleWidget(questionItem: QuestionItem(title:'ĐMTB', options: ['Có', 'Chỉ định XN'])),
    );
  }
}
