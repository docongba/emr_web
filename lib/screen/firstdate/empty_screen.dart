import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class EmptyScreen extends StatefulWidget {

  VoidCallback callback;
  EmptyScreen({this.callback});
  @override
  State createState() {
    return _EmptyScreenState();
  }
}

class _EmptyScreenState extends State<EmptyScreen> {
  ValueNotifier<bool> _monitor = ValueNotifier<bool>(false);
  ValueNotifier<bool> _kieutho = ValueNotifier<bool>(false);
  ValueNotifier<bool> _nkq = ValueNotifier<bool>(false);
  ValueNotifier<bool> _httk = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tim = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tuchi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _amthoi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _amthoivungmat = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tuongdong  = ValueNotifier<bool>(false);
  ValueNotifier<bool> _hatb  = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),

        ),
      ),
    );
  }

  _machChilds(){
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        QuestionMultipleWidget(
          questionItem: QuestionItem(
              type: QuestionType.MULTIPLE_CHOICE,
              title: 'Mạch có ',
              options: [
                'Đồng bộ nhịp tim ',
                'Ảnh hưởng huyết động',
                'Bị ảnh hưởng thân nhiệt',
                'Bị ảnh hưởng hô hấp',
                'Vết tiêm chích bất thường',
                'Không đồng đều tứ chi',
              ],
              id: "1009"),
          callback: (index, select, indexes) {

            if(index == 5 && select){
              _tuchi.value = true;
            }else{
              _tuchi.value = false;
            }
            print("SHOW SELECTED INDEX $index");
          },
        ),
        Container(
          margin: EdgeInsets.only(left: 50),
          child: ValueListenableBuilder<bool>(
            valueListenable: _tuchi,
            builder: (_, value, child){
              return value? QuestionTextWidget(questionItem: QuestionItem(title: 'Chi giảm hoặc mất mạch')): Container();
            }
          ),
        ),
        QuestionMultipleWidget(
          questionItem: QuestionItem(
              type: QuestionType.MULTIPLE_CHOICE,
              title: 'Có biểu hiện của XVĐM? ',
              options: [
                'Cứng  ',
                'ngoằn ngoèo ',
                'Đáy mắt ',
              ],
              id: "1009"),
          callback: (index, select, indexes) {

          },
        ),

        QuestionMultipleWidget(
          questionItem: QuestionItem(
              type: QuestionType.MULTIPLE_CHOICE,
              title: 'ĐM Cảnh ',
              options: [
                'Nảy đều 2 bên  ',
                'Âm thổi ',
              ],
              id: "1009"),
          callback: (index, select, indexes) {
            if(indexes.contains(1)){
              _amthoi.value = true;
            }else{
              _amthoi.value = false;
            }

          },
        ),
        Container(
          margin: EdgeInsets.only(left: 40),
          child: ValueListenableBuilder(
            valueListenable: _amthoi,
              builder: (_, value, child){
              return value?QuestionTextWidget(questionItem: QuestionItem(title: "bên (1:trái, 2:phải, 3: 2 bên)")): Container();
              }),
        ),
        QuestionMultipleWidget(
          questionItem: QuestionItem(
              type: QuestionType.MULTIPLE_CHOICE,
              title: '',
              options: [
                'Âm thổi vùng mắt ',
              ],
              id: "1009"),
          callback: (index, select, indexes) {
            if(indexes.contains(0)){
              _amthoivungmat.value = true;
            }else{
              _amthoivungmat.value = false;
            }

          },
        ),
        Container(
          margin: EdgeInsets.only(left: 40),
          child: ValueListenableBuilder(
              valueListenable: _amthoivungmat,
              builder: (_, value, child){
                return value?QuestionTextWidget(questionItem: QuestionItem(title: "bên (1:trái, 2:phải, 3: 2 bên)")): Container();
              }),
        ),
      ],
    );
  }

  _monitorChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _monitor,
      builder: (_, value, child) {
        return _monitor.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: QuestionSingleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.SINGLE_CHOICE,
                      title: '',
                      options: [
                        '1h',
                        '2h',
                        '4h',
                        '6h',
                        '8h',
                        '12h',
                        '24h',
                      ],
                      id: "1006"),
                  callback: (index) {
                    print("SHOW SELECTED INDEX $index");
                  },
                ),
              )
            : Container();
      },
    );
  }

  _kieuThoChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _kieutho,
      builder: (_, value, child) {
        return _kieutho.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: QuestionMultipleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: '',
                      options: [
                        'Gắng sức',
                        'Đảo ngược',
                        'Nhiều đàm',
                        'Giọng nói ứ đọng nước bọt',
                        'Phập phồng mũi',
                        'Tiếng rít thanh quản',
                        'Tụt lưỡi',
                        'Co kéo các cơ hô hấp khác',
                        'Ho không hiệu quả',
                        'Tiếng khò khè do co thắt PQ',
                        'Lồng ngực hình thùng',
                      ],
                      id: "1006"),
                  callback: (index, select, indexes) {
                    print("SHOW SELECTED INDEX $index");
                  },
                ),
              )
            : Container();
      },
    );
  }

  _nkqChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _nkq,
      builder: (_, value, child) {
        return _nkq.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionDateWidget(
                      questionItem: QuestionItem(title: 'Thời gian', type: QuestionType.DATE),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'TANLS',
                            'Tổn thương não',
                            'Bảo vệ đường thở',
                            'Giảm chức năng tim ',
                            'Giảm chức năng phổi',
                            'Yếu cơ hô hấp',
                            'Sử dụng thuốc(an thần, giảm đau, chống thần kinh...)',
                            'Thở máy ',
                          ],
                          id: "1006"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  _httkChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _httk,
      builder: (_, value, child) {
        return _httk.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionDateWidget(
                      questionItem: QuestionItem(title: 'Thời gian', type: QuestionType.DATE),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'TANLS',
                            'Tổn thương não',
                            'Giảm chức năng tim',
                            'Yếu cơ hô hấp ',
                            'TKMP',
                            'TDMP',
                            'Co thắt phế quản',
                            'Tổn thương phế nang',
                            'Thuyên tắc phổi',
                            'Sử dụng thuốc (an thần, giảm đau, thần kinh...',
                          ],
                          id: "1006"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  _timChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _tim,
      builder: (_, value, child) {
        return _tim.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionTextWidget(
                      questionItem: QuestionItem(title: 'Tên loạn nhịp', type: QuestionType.TEXT),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'Bị ảnh hưởng thân nhiệt',
                            'Ảnh hưởng đến huyết động ',
                            'Cần HC tim mạch',
                          ],
                          id: "1006"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: 'Cần Monitor dự trù',
                          options: [
                            '1d',
                            '2d',
                            '3d',
                            '>3d',
                          ],
                          id: "1009"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: 'Ảnh hưởng bởi',
                          options: [
                            'Bệnh nền',
                            'RL nước-dg',
                            'KR',
                          ],
                          id: "1009"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  _bodyInfo() {
    var list = ['Cân nặng (kg)', 'Chiều cao(cm)', 'BMI'];
    return Container(
      child: Wrap(
        children: List.generate(list.length, (index) {
          return QuestionTextWidget(questionItem: QuestionItem(title: list[index]));
        }),
      ),
    );
  }
}
