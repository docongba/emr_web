import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/widget/question_button_widget.dart';
import 'package:flutter_app/widget/question_check_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_check_show_singlechoice_widget.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multi_textfield_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_yn_show_textfield_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';
import 'package:flutter_app/widget/sub_section_widget.dart';
import 'package:flutter_app/widget/table_cell.dart';

class ChamSocChuyenBietScreen extends StatefulWidget {
  VoidCallback callback;

  ChamSocChuyenBietScreen({this.callback});

  @override
  State createState() {
    return _ChamSocChuyenBietScreenState();
  }
}

class _ChamSocChuyenBietScreenState extends State<ChamSocChuyenBietScreen> {
  ValueNotifier<bool> _xacdinh = ValueNotifier<bool>(false);
  ValueNotifier<bool> _nhieuKhanang = ValueNotifier<bool>(false);
  ValueNotifier<bool> _chandoanxhn = ValueNotifier<bool>(false);
  ValueNotifier<bool> _chandoanxhnkhac = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dieuchinhhuyetap = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dieuchinhdongmau = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dieuchinhduonghuyet = ValueNotifier<bool>(false);
  ValueNotifier<bool> _cactinhtrangnoikhoachuy = ValueNotifier<bool>(false);
  ValueNotifier<bool> _vitrixhn = ValueNotifier<bool>(false);
  ValueNotifier<bool> _trongnhumonao = ValueNotifier<bool>(false);
  ValueNotifier<bool> _xhmn = ValueNotifier<bool>(false);
  ValueNotifier<bool> _vxhn = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tutnao = ValueNotifier<bool>(false);
  ValueNotifier<bool> _canuongthuot = ValueNotifier<bool>(false);
  ValueNotifier<bool> _testnuot = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dientienxauhon = ValueNotifier<bool>(false);

  ValueNotifier<int> canthiepdieutriNoti = ValueNotifier<int>(1);
  ValueNotifier<int> canthiepmentimNoti = ValueNotifier<int>(1);
  ValueNotifier<int> canthiepDienGiaiNoti = ValueNotifier<int>(1);

  int canthiep = 1;
  int canthiepmentim = 1;
  int canthiepDienGiaiDo = 1;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            children: [
              Column(
                children: [
                  SectionWidget(
                    title: 'Chẩn đoán',
                  ),
                  _hinhanhUI(),
                  _xnmauUI(),
                  _xnkhac(),
                ],
              ),
              _chanDoanUI(),
            ],
          ),
        ),
      ),
    );
  }

  _xnkhac() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Xét nghiệm khác ',
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Áp lực thẩm thấu máu(Posm) ',
            subs: ['Thòi điểm xét nghiệm', 'Thời điểm kết quả', 'Kết quả'],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Điện giải đồ nước tiểu',
            subs: ['Thòi điểm xét nghiệm', 'Thời điểm kết quả', 'Kết quả Na', 'kết quả K'],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Áp lực thẩm thấu nước tiểu(Uosm)',
            subs: ['Thòi điểm xét nghiệm', 'Thời điểm kết quả', 'Kết quả'],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Rượu',
            subs: ['Thòi điểm xét nghiệm', 'Thời điểm kết quả', 'Kết quả'],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Độc chất máu',
            subs: ['Thòi điểm xét nghiệm', 'Thời điểm kết quả', 'Kết quả'],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Độc chất nước tiểu',
            subs: ['Thòi điểm xét nghiệm', 'Thời điểm kết quả', 'Kết quả'],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Siêu âm tim ',
            subs: [''],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Khác',
            subs: ['Lactate', 'CRP', 'FT3', 'FT4', 'TSH'],
          ),
        ],
      ),
    );
  }

  _nuoctieuUI() {
    ValueNotifier<bool> tptnt = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: Column(
              children: [
                QuestionSingleWidget(
                  questionItem: QuestionItem(title: 'TPTNT', options: ['Có', 'Không']),
                  callback: (index) {
                    if (index == 0) {
                      tptnt.value = true;
                    } else {
                      tptnt.value = false;
                    }
                  },
                ),
                ValueListenableBuilder<bool>(
                  valueListenable: tptnt,
                  builder: (_, value, child) {
                    return value
                        ? Container(
                            margin: EdgeInsets.only(left: 40),
                            child: QuestionYNShowTextFieldWidget(
                              title: '',
                              yesno: ['Bất thường', 'Bình thường'],
                              correctIndex: 0,
                              subs: ['Mô tả', 'Xử trí'],
                            ),
                          )
                        : Container();
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _xqUI() {
    ValueNotifier<bool> khac = ValueNotifier<bool>(false);
    ValueNotifier<bool> uuthe = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'XQ tim phổi',
          ),
          QuestionDateWidget(questionItem: QuestionItem(title: 'Thòi điểm xét nghiệm')),
          QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm kết quả')),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: [
                  'TDMP ',
                  'TKMP ',
                ],
                id: "1006"),
            callback: (idx, select, indexes) {},
          ),
          _xqPhoiTabletUI(),
          Container(
            child: QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: 'Phổi ',
                  options: [
                    'Xơ  ',
                    'Tổn thương cũ ',
                    'Đông đặc',
                    'Giản phế quản',
                    'Khí phế thủng',
                    'Xẹp phổ',
                    'u phổi',
                    'Khác '
                  ],
                  id: "1006"),
              callback: (idx, select, indexes) {
                if (indexes.contains(7)) {
                  khac.value = true;
                } else {
                  khac.value = false;
                }
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
              valueListenable: khac,
              builder: (_, value, child) {
                return value
                    ? QuestionTextWidget(
                        questionItem: QuestionItem(title: ""),
                        callback: (value) {},
                      )
                    : Container();
              },
            ),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: 'Vị trí  ',
                options: [
                  'Đỉnh   ',
                  'Đáy ',
                  'Rốn phổi',
                  'Ngoại biên ',
                  'Ưu thế ',
                ],
                id: "1006"),
            callback: (idx, select, indexes) {
              if (indexes.contains(4)) {
                uuthe.value = true;
              } else {
                uuthe.value = false;
              }
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
              valueListenable: uuthe,
              builder: (_, value, child) {
                return value
                    ? QuestionSingleWidget(
                        questionItem: QuestionItem(title: "", options: [
                          'Đỉnh   ',
                          'Đáy ',
                          'Rốn phổi',
                          'Ngoại biên ',
                        ]),
                        callback: (value) {},
                      )
                    : Container();
              },
            ),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: 'Vị trí  ',
                options: ['Bên trái ', 'Bên phải ', 'Cả 2 bên'],
                id: "1006"),
            callback: (idx, select, indexes) {},
          ),
        ],
      ),
    );
  }

  _xqPhoiTabletUI() {
    List<String> coquans = ['Vòm hoành', 'Tim ', 'Trung thất  '];

    return Container(
      child: Column(
        children: List.generate(coquans.length, (index) {
          return Container(
            child: QuestionYNShowTextFieldWidget(
              title: coquans[index],
              yesno: ['Bình thường', 'Bất thường'],
              correctIndex: 1,
              subs: ['Mô tả'],
            ),
          );
        }),
      ),
    );
  }

  _ecgUI() {
    ValueNotifier<bool> _ecgLoanNhip = ValueNotifier<bool>(false);
    ValueNotifier<bool> _ecgLontim = ValueNotifier<bool>(false);
    ValueNotifier<bool> _ecgThieuMau = ValueNotifier<bool>(false);

    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
//          QuestionYNShowTextFieldWidget(
//            title: 'ECG',
//            yesno: ['Bình thường', 'Bất thường'],
//            correctIndex: 1,
//            subs: ['Mô tả', 'Xử trí'],
//
          SectionWidget(
            title: 'ECG',
          ),
//          ),
          Container(
              margin: EdgeInsets.only(left: 40),
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionDateWidget(questionItem: QuestionItem(title: 'Thòi điểm xét nghiệm')),
                    QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm kết quả')),
//                SubSectionWidget(
//                  title: 'Kết quả',
//                ),
                    Row(
                      children: [
                        QuestionYNShowMultipleWidget(
                          title: 'Loạn nhịp?',
                          callbackYN: (idx) {
                            if (idx == 0) {
                              _ecgLoanNhip.value = true;
                            } else {
                              _ecgLoanNhip.value = false;
                            }
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 40),
                          child: ValueListenableBuilder<bool>(
                            valueListenable: _ecgLoanNhip,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionTextWidget(
                                      questionItem: QuestionItem(title: "Mô tả"),
                                      callback: (value) {},
                                    )
                                  : Container();
                            },
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        QuestionYNShowMultipleWidget(
                          title: 'Lớn tim?',
                          callbackYN: (idx) {
                            if (idx == 0) {
                              _ecgLontim.value = true;
                            } else {
                              _ecgLontim.value = false;
                            }
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 40),
                          child: ValueListenableBuilder<bool>(
                            valueListenable: _ecgLontim,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionTextWidget(
                                      questionItem: QuestionItem(title: "Mô tả"),
                                      callback: (value) {},
                                    )
                                  : Container();
                            },
                          ),
                        ),
                      ],
                    ),
                    Row(
                      children: [
                        QuestionYNShowMultipleWidget(
                          title: 'Thiếu máu, NMCT?',
                          callbackYN: (idx) {
                            if (idx == 0) {
                              _ecgThieuMau.value = true;
                            } else {
                              _ecgThieuMau.value = false;
                            }
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 40),
                          child: ValueListenableBuilder<bool>(
                            valueListenable: _ecgThieuMau,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionTextWidget(
                                      questionItem: QuestionItem(title: "Vị trí"),
                                      callback: (value) {},
                                    )
                                  : Container();
                            },
                          ),
                        ),
                      ],
                    ),
                  ]))
        ],
      ),
    );
  }

  _xnmauUI() {
    ValueNotifier<bool> batThuong1 = ValueNotifier<bool>(false);
    ValueNotifier<bool> batThuong2 = ValueNotifier<bool>(false);
    ValueNotifier<bool> batThuongdiengiaido1 = ValueNotifier<bool>(false);
    ValueNotifier<bool> batThuongdiengiaido2 = ValueNotifier<bool>(false);
    ValueNotifier<bool> batThuongmentim1 = ValueNotifier<bool>(false);
    ValueNotifier<bool> batThuongmentim2 = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 10),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(title: 'Kết quả Xét nghiệm'),
          QuestionMultiTextFieldWidget(
            title: 'CTM',
            subs: ['HC M', 'Hb g/', 'BC (N % L %)', 'TC'],
          ),
          QuestionMultiTextFieldWidget(
            title: 'VS',
            subs: ['H1', 'H2'],
          ),
          QuestionMultiTextFieldWidget(
            title: 'Nhóm máu',
            subs: [''],
          ),
          QuestionMultiTextFieldWidget(
            title: 'Chức năng tiểu cầu',
            subs: [''],
          ),
          Container(
            decoration: containerDecoration(),
            child: Column(
              children: [
                QuestionMultiTextFieldWidget(
                  title: 'ĐMTB',
                  subs: ['aPTT', 'INR', 'PT', 'Fibrinogen '],
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Bất thường ',
                  extraOptions: ['Kiểm tra để Xác định chẩn đoán', 'Kiểm tra sau can thiệp điều trị'],
                  callBackChildOption: (indexes) {
                    if (indexes.contains(0)) {
                      batThuong1.value = true;
                    } else {
                      batThuong1.value = false;
                    }

                    if (indexes.contains(1)) {
                      batThuong2.value = true;
                    } else {
                      batThuong2.value = false;
                    }
                  },
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: 400,
                      decoration: containerDecoration(),
                      child: ValueListenableBuilder<bool>(
                        valueListenable: batThuong1,
                        builder: (_, value, child) {
                          return value
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kiểm tra')),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kết quả')),
                                    QuestionMultiTextFieldWidget(
                                      horizontal: false,
                                      title: 'Kết quả',
                                      subs: ['aPTT', 'INR', 'PT', 'Fibrinogen '],
                                    ),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Kết luận ')),
                                  ],
                                )
                              : Container();
                        },
                      ),
                    ),
                    Container(
                      width: 400,
                      decoration: containerDecoration(),
                      child: ValueListenableBuilder<bool>(
                          valueListenable: batThuong2,
                          builder: (_, value, child) {
                            return value
                                ? Container(
                                    margin: EdgeInsets.all(10),
                                    decoration: containerDecoration(),
                                    child: Column(
                                      children: [
                                        ValueListenableBuilder<int>(
                                          valueListenable: canthiepdieutriNoti,
                                          builder: (_, value, child) {
                                            return Column(
                                                children: List.generate(value, (index) {
                                              return Container(
//                                            margin: EdgeInsets.all(10),
                                                decoration: containerDecoration(),
                                                child: Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.start,
                                                  children: [
                                                    QuestionTextWidget(
                                                        questionItem: QuestionItem(title: 'Thời điểm kiểm tra')),
                                                    QuestionTextWidget(
                                                        questionItem: QuestionItem(title: 'Thời điểm kết quả')),
                                                    QuestionMultiTextFieldWidget(
                                                      horizontal: false,
                                                      title: 'Kết quả',
                                                      subs: ['aPTT', 'INR', 'PT', 'Fibrinogen '],
                                                    ),
                                                    QuestionSingleWidget(
                                                        questionItem: QuestionItem(
                                                            title: 'Kết luận Kiểm soát ĐM',
                                                            options: ['Phù hợp', 'Chưa đủ'])),
                                                  ],
                                                ),
                                              );
                                            }));
                                          },
                                        ),
                                        _addButotn(() {
                                          canthiep++;
                                          canthiepdieutriNoti.value = canthiep;
                                        })
                                      ],
                                    ),
                                  )
                                : Container();
                          }),
                    )
                  ],
                )
              ],
            ),
          ),
          Row(
            children: [
              QuestionMultiTextFieldWidget(
                title: 'Đường huyết (Glucose)',
                subs: ['Glucose      mg%'],
              ),
              QuestionMultiTextFieldWidget(
                title: 'HbA1',
                subs: [''],
              ),
            ],
          ),
          Container(
            decoration: containerDecoration(),
            child: Column(
              children: [
                QuestionMultiTextFieldWidget(
                  title: 'Men tim ',
                  subs: ['CKMB ', 'Troponin'],
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Bất thường ',
                  extraOptions: ['Kiểm tra để Xác định chẩn đoán', 'Kiểm tra sau can thiệp điều trị'],
                  callBackChildOption: (indexes) {
                    if (indexes.contains(0)) {
                      batThuongmentim1.value = true;
                    } else {
                      batThuongmentim1.value = false;
                    }

                    if (indexes.contains(1)) {
                      batThuongmentim2.value = true;
                    } else {
                      batThuongmentim2.value = true;
                    }
                  },
                ),
                Row(
                  children: [
                    Container(
                      width: 400,
                      child: ValueListenableBuilder<bool>(
                        valueListenable: batThuongmentim1,
                        builder: (_, value, child) {
                          return value
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kiểm tra')),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kết quả')),
                                    QuestionMultiTextFieldWidget(
                                      title: 'Kết quả',
                                      subs: ['CKMB ', 'Troponin'],
                                    ),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Kết luận ')),
                                  ],
                                )
                              : Container();
                        },
                      ),
                    ),

                    Container(
                      width: 400,
                      child: ValueListenableBuilder<bool>(
                          valueListenable: batThuongmentim2,
                          builder: (_, value, child) {
                            return value
                                ? Column(
                              children: [
                                ValueListenableBuilder<int>(
                                  valueListenable: canthiepmentimNoti,
                                  builder: (_, value, child) {
                                    return Column(
                                        children: List.generate(value, (index) {
                                          return Container(
                                            margin: EdgeInsets.all(10),
                                            decoration: containerDecoration(),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kiểm tra')),
                                                QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kết quả')),
                                                QuestionMultiTextFieldWidget(
                                                  title: 'Kết quả',
                                                  subs: ['CKMB ', 'Troponin'],
                                                ),
                                                QuestionSingleWidget(
                                                    questionItem: QuestionItem(
                                                        title: 'Kết quả can thiệp', options: ['Phù hợp', 'Chưa đủ'])),
                                              ],
                                            ),
                                          );
                                        }));
                                  },
                                ),
                                _addButotn(() {
                                  canthiepmentim++;
                                  canthiepmentimNoti.value = canthiepmentim;
                                })
                              ],
                            )
                                : Container();
                          }),
                    )
                  ],
                ),

              ],
            ),
          ),
          QuestionMultiTextFieldWidget(
            title: 'KMĐM ',
            subs: ['pH', 'paO2', 'paCO2', 'HCO3', '∆(A-a)O2'],
          ),
          QuestionMultiTextFieldWidget(
            title: 'CN gan ',
            subs: ['SGOT ', 'SGPT ', 'GGT ', 'NH3 ', 'Bilirubin', 'TT ', 'GT'],
          ),
          QuestionMultiTextFieldWidget(
            title: 'CN Thận',
            subs: ['BUN    mg% ', 'Creatinine  mg% ', 'eGFR  '],
          ),
          Container(
            decoration: containerDecoration(),
            child: Column(
              children: [
                QuestionMultiTextFieldWidget(
                  title: 'Điện giải đồ ',
                  subs: ['Na ', 'K', 'Ca', 'Mg'],
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Bất thường ',
                  extraOptions: ['Kiểm tra để Xác định chẩn đoán', 'Kiểm tra sau can thiệp điều trị'],
                  callBackChildOption: (indexes) {
                    if (indexes.contains(0)) {
                      batThuongdiengiaido1.value = true;
                    } else {
                      batThuongdiengiaido1.value = false;
                    }

                    if (indexes.contains(1)) {
                      batThuongdiengiaido2.value = true;
                    } else {
                      batThuongdiengiaido2.value = false;
                    }
                  },
                ),
                Row(
                  children: [
                    Container(
                      width: 400,
                      child: ValueListenableBuilder<bool>(
                        valueListenable: batThuongdiengiaido1,
                        builder: (_, value, child) {
                          return value
                              ? Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kiểm tra')),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm kết quả')),
                                    QuestionMultiTextFieldWidget(
                                      title: 'Kết quả',
                                      subs: ['Na ', 'K', 'Ca', 'Mg'],
                                    ),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Kết luận ')),
                                  ],
                                )
                              : Container();
                        },
                      ),
                    ),
                    Container(
                      width: 400,
                      child: ValueListenableBuilder<bool>(
                        valueListenable: batThuongdiengiaido2,
                        builder: (_, value, child) {
                          return value
                              ? Column(
                                  children: [
                                    ValueListenableBuilder<int>(
                                      valueListenable: canthiepDienGiaiNoti,
                                      builder: (_, value, child) {
                                        return Column(
                                            children: List.generate(value, (index) {
                                          return Container(
                                            margin: EdgeInsets.all(10),
                                            decoration: containerDecoration(),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                QuestionTextWidget(
                                                    questionItem: QuestionItem(title: 'Thời điểm kiểm tra')),
                                                QuestionTextWidget(
                                                    questionItem: QuestionItem(title: 'Thời điểm kết quả')),
                                                QuestionMultiTextFieldWidget(
                                                  title: 'Kết quả',
                                                  subs: ['Na ', 'K', 'Ca', 'Mg'],
                                                ),
                                                QuestionSingleWidget(
                                                    questionItem: QuestionItem(
                                                        title: 'Kết luận điều chỉnh điện giải đồ',
                                                        options: ['Phù hợp', 'Chưa đủ'])),
                                              ],
                                            ),
                                          );
                                        }));
                                      },
                                    ),
                                    _addButotn(() {
                                      canthiepDienGiaiDo++;
                                      canthiepDienGiaiNoti.value = canthiepDienGiaiDo;
                                    })
                                  ],
                                )
                              : Container();
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
          _ecgUI(),
          _nuoctieuUI(),
          _xqUI(),
        ],
      ),
    );
  }

  _addButotn(callback) {
    return FlatButton(
      color: Colors.blue,
      onPressed: callback,
      child: Text("Thêm"),
    );
  }

  _groupCTScan() {
    ValueNotifier<bool> hydro = ValueNotifier<bool>(false);
    ValueNotifier<bool> xhn = ValueNotifier<bool>(false);
    ValueNotifier<bool> vxhn = ValueNotifier<bool>(false);

    ValueNotifier<bool> bcTrai = ValueNotifier<bool>(false);
    ValueNotifier<bool> bcPhai = ValueNotifier<bool>(false);
    ValueNotifier<bool> hosau = ValueNotifier<bool>(false);

    ValueNotifier<bool> tieunao = ValueNotifier<bool>(false);
    ValueNotifier<bool> thannao = ValueNotifier<bool>(false);
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text("Nơi XHN bắt đầu: "),
        Row(
          children: [
            QuestionMultipleWidget(
              questionItem: QuestionItem(title: '', options: ['BC Trái']),
              callback: (idx, select, indexes) {
                if (indexes.contains(idx)) {
                  bcTrai.value = true;
                } else {
                  bcTrai.value = false;
                }
              },
            ),
            ValueListenableBuilder<bool>(
              valueListenable: bcTrai,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
              },
            ),
          ],
        ),
        Row(
          children: [
            QuestionMultipleWidget(
              questionItem: QuestionItem(title: '', options: ['BC Phải']),
              callback: (idx, select, indexes) {
                if (indexes.contains(idx)) {
                  bcPhai.value = true;
                } else {
                  bcPhai.value = false;
                }
              },
            ),
            ValueListenableBuilder<bool>(
              valueListenable: bcPhai,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
              },
            ),
          ],
        ),
        Column(
          children: [
            QuestionMultipleWidget(
              questionItem: QuestionItem(title: '', options: ['Hố sau']),
              callback: (idx, select, indexes) {
                if (indexes.contains(idx)) {
                  hosau.value = true;
                } else {
                  hosau.value = false;
                }
              },
            ),
            Container(
              margin: EdgeInsets.only(left: 40),
              child: ValueListenableBuilder<bool>(
                valueListenable: hosau,
                builder: (_, value, child) {
                  return value
                      ? Column(
                          children: [
                            Row(
                              children: [
                                QuestionMultipleWidget(
                                  questionItem: QuestionItem(title: '', options: ['Tiểu não ']),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      tieunao.value = true;
                                    } else {
                                      tieunao.value = false;
                                    }
                                  },
                                ),
                                ValueListenableBuilder<bool>(
                                  valueListenable: tieunao,
                                  builder: (_, value, child) {
                                    return value
                                        ? QuestionTextWidget(questionItem: QuestionItem(title: ''))
                                        : Container();
                                  },
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                QuestionMultipleWidget(
                                  questionItem: QuestionItem(title: '', options: ['Thân não']),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      thannao.value = true;
                                    } else {
                                      thannao.value = false;
                                    }
                                  },
                                ),
                                ValueListenableBuilder<bool>(
                                  valueListenable: thannao,
                                  builder: (_, value, child) {
                                    return value
                                        ? QuestionTextWidget(questionItem: QuestionItem(title: ''))
                                        : Container();
                                  },
                                ),
                              ],
                            ),
                          ],
                        )
                      : Container();
                },
              ),
            ),
          ],
        ),
        QuestionMultipleWidget(
          questionItem: QuestionItem(title: 'Ảnh hưởng', options: [
            'Xuất huyết não thất',
            'phù não quanh ổ XHN',
            'Hiệu ứng choán chổ nặng',
            'Ổ XH không đồng nhất',
            'Có mực nước (máu)',
            'Hydrochepalie ',
            'gợi ý tụt não',
            'Nhiều ổ XHN',
            'VXHN'
          ]),
          callback: (idx, select, indexes) {
            if (indexes.contains(5)) {
              hydro.value = true;
            } else {
              hydro.value = false;
            }

            if (indexes.contains(7)) {
              xhn.value = true;
            } else {
              xhn.value = false;
            }

            if (indexes.contains(8)) {
              vxhn.value = true;
            } else {
              vxhn.value = false;
            }
          },
        ),
        ValueListenableBuilder<bool>(
          valueListenable: hydro,
          builder: (_, value, child) {
            return value ? QuestionTextWidget(questionItem: QuestionItem(title: 'Loại Hydrochepalie ')) : Container();
          },
        ),
        ValueListenableBuilder<bool>(
          valueListenable: xhn,
          builder: (_, value, child) {
            return value ? QuestionTextWidget(questionItem: QuestionItem(title: 'Nhiều ổ XHN ')) : Container();
          },
        ),
        ValueListenableBuilder<bool>(
          valueListenable: vxhn,
          builder: (_, value, child) {
            return value ? QuestionTextWidget(questionItem: QuestionItem(title: 'VXHN (ml)')) : Container();
          },
        ),
      ],
    );
  }

  _hinhanhUI() {
    ValueNotifier<bool> cta = ValueNotifier<bool>(false);
    ValueNotifier<bool> mri = ValueNotifier<bool>(false);
    ValueNotifier<bool> mra = ValueNotifier<bool>(false);
    ValueNotifier<bool> dsa = ValueNotifier<bool>(false);

    ValueNotifier<bool> khaosat = ValueNotifier<bool>(false);
    ValueNotifier<bool> khaosatcon = ValueNotifier<bool>(false);

    ValueNotifier<int> ctScan = ValueNotifier<int>(-1);

    List<String> scans = ['CTscan (PL) gđ cấp/tối cấp ', 'CTscan (Kiểm tra)', 'CTscan (Kiểm tra)'];

    return Container(
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SectionWidget(
            title: 'Hình ảnh',
          ),
          QuestionYNShowMultipleWidget(
            title: 'Xác định XHN',
          ),
          Column(
            children: List.generate(scans.length, (index) {
              ValueNotifier<int> scanPL = ValueNotifier<int>(-1);

              return Container(
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                QuestionYNShowMultipleWidget(
                  title: scans[index],
                  callbackYN: (index) {
                    if (index == 0) {
                      scanPL.value = 0;
                    } else {
                      scanPL.value = 1;
                    }
                  },
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<int>(
                    valueListenable: scanPL,
                    builder: (_, value, child) {
                      if (value == 0) {
                        return Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                            _groupCTScan()
                          ],
                        );
                      } else if (value == 1) {
                        if (index == 0) {
                          ValueNotifier<int> chidinh = ValueNotifier<int>(-1);
                          return Container(
                            child: Column(
                              children: [
                                QuestionYNShowMultipleWidget(
                                  title: 'Chỉ định',
                                  callbackYN: (index) {
                                    if (index == 0) {
                                      chidinh.value = 0;
                                    } else {
                                      chidinh.value = 1;
                                    }
                                  },
                                ),
                                ValueListenableBuilder<int>(
                                    valueListenable: chidinh,
                                    builder: (_, value, child) {
                                      if (value == 0) {
                                        return Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                            _groupCTScan()
                                          ],
                                        );
                                      } else {
                                        return Container();
                                      }
                                    })
                              ],
                            ),
                          );
                        } else {
                          return QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm'));
                        }
                      } else {
                        return Container();
                      }
                    },
                  ),
                )
              ]));
            }),
          ),
          Container(
              child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
            QuestionYNShowMultipleWidget(
              title: 'MRI',
              callbackYN: (index) {
                if (index == 0) {
                  ctScan.value = 0;
                } else {
                  ctScan.value = 1;
                }
              },
            ),
            Container(
              margin: EdgeInsets.only(left: 40),
              child: ValueListenableBuilder<int>(
                valueListenable: ctScan,
                builder: (_, value, child) {
                  if (value == 0) {
                    return Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')), _groupCTScan()],
                    );
                  } else if (value == 1) {
                    ValueNotifier<int> chidinh = ValueNotifier<int>(-1);
                    return Container(
                      child: Column(
                        children: [
                          QuestionYNShowMultipleWidget(
                            title: 'Chỉ định',
                            callbackYN: (index) {
                              if (index == 0) {
                                chidinh.value = 0;
                              } else {
                                chidinh.value = 1;
                              }
                            },
                          ),
                          ValueListenableBuilder<int>(
                              valueListenable: chidinh,
                              builder: (_, value, child) {
                                if (value == 0) {
                                  return Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                      _groupCTScan()
                                    ],
                                  );
                                } else {
                                  return Container();
                                }
                              })
                        ],
                      ),
                    );
                  } else {
                    return Container();
                  }
                },
              ),
            )
          ])),
          SectionWidget(
            title: 'Khảo sát hình ảnh mạch máu não',
          ),
          ValueListenableBuilder<bool>(
            valueListenable: khaosat,
            builder: (_, value, child) {
              return value
                  ? QuestionYNShowMultipleWidget(
                      title: 'Khảo sát hình ảnh mạch máu não',
                      callbackYN: (index) {
                        if (index == 0) {
                          khaosatcon.value = true;
                        } else {
                          khaosatcon.value = false;
                        }
                      },
                    )
                  : Container();
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: QuestionMultipleWidget(
              hasDecor: true,
              questionItem: QuestionItem(
                  title: 'Dựa vào Lâm sàng và hình ảnh hướng đến chẩn đoán',
                  options: ['AVM', 'Aneurysm', 'Dural AV fistula', 'Carvernoma', 'HKTMN', 'Mặc định']),
              callback: (ind, select, indexes) {
                if (indexes.contains(ind)) {
                  khaosat.value = true;
                } else {
                  khaosat.value = false;
                }
              },
            ),
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
                valueListenable: khaosatcon,
                builder: (_, value, child) {
                  return value
                      ? Column(
                          children: [
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  QuestionYNShowMultipleWidget(
                                    title: 'CTA (CE)',
                                    callbackYN: (index) {
                                      if (index == 0) {
                                        cta.value = true;
                                      } else {
                                        cta.value = false;
                                      }
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: cta,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    questionItem: QuestionItem(title: '', options: [
                                                      '“spot sign”',
                                                      'AVN',
                                                      'Aneurysm',
                                                      'Dural AV fistula',
                                                      'Carvernoma',
                                                      'HKTM',
                                                      'Vị trí'
                                                    ]),
                                                  ),
//
                                                ],
                                              )
                                            : Container();
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  QuestionYNShowMultipleWidget(
                                    title: 'MRA',
                                    callbackYN: (idx) {
                                      if (idx == 0) {
                                        mra.value = true;
                                      } else {
                                        mra.value = false;
                                      }
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: mra,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    questionItem: QuestionItem(title: 'Kết quả', options: [
                                                      'AVM',
                                                      'Aneurysm',
                                                      'Dural AV fistula',
                                                      'Carvernoma',
                                                      'HKTM'
                                                    ]),
                                                  ),
                                                  QuestionTextWidget(questionItem: QuestionItem(title: 'Vị trí')),
                                                ],
                                              )
                                            : Container();
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  QuestionYNShowMultipleWidget(
                                    title: 'DSA',
                                    callbackYN: (idx) {
                                      if (idx == 0) {
                                        dsa.value = true;
                                      } else {
                                        dsa.value = false;
                                      }
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: dsa,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    questionItem: QuestionItem(title: 'Kết quả', options: [
                                                      'AVM',
                                                      'Aneurysm',
                                                      'Dural AV fistula',
                                                      'Carvernoma',
                                                      'HKTM'
                                                    ]),
                                                  ),
                                                  QuestionTextWidget(questionItem: QuestionItem(title: 'Vị trí')),
                                                ],
                                              )
                                            : Container();
                                      },
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ],
                        )
                      : Container();
                }),
          ),
        ]));
  }

  _chanDoanUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          QuestionYNShowMultipleWidget(
            title: 'Xác định Chẩn đoán XHN',
            extraOptions: ['Lâm sàng', 'Hình ảnh não ', 'Mạch máu não '],
          ),
          QuestionYNShowTextFieldWidget(
            title: 'Nhiều khả năng ',
            subs: ['Cần đánh giá thêm'],
          ),
          Row(
            children: [
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'Chẩn đoán phân biệt '),
              ),
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'Cần đánh giá thêm'),
              ),
            ],
          ),
          Row(
            children: [
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'Chẩn đoán phân biệt '),
              ),
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'Cần đánh giá thêm'),
              ),
            ],
          ),
          Row(
            children: [
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'Thời gian từ khởi phát đến BVCR (Nhập)'),
              ),
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'KP-9B3: (Thời gian)'),
              ),
              QuestionTextWidget(
                questionItem: QuestionItem(title: 'KP-đánh giá:'),
              ),
            ],
          ),
          Container(
            decoration: containerDecoration(),
            child: Column(
              children: [
                SectionWidget(
                  title: 'Chẩn đoán XHN',
                ),
                Row(
                  children: [
                    QuestionTextWidget(
                      questionItem: QuestionItem(title: 'Chẩn đoán XHN'),
                    ),
                    QuestionTextWidget(
                      questionItem: QuestionItem(title: 'Ngày thứ'),
                    ),
                    QuestionTextWidget(
                      questionItem: QuestionItem(title: 'Giờ thứ'),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _chandoanxhn,
                    builder: (_, value, child) {
                      return !value
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                QuestionMultipleWidget(
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: 'Nguyên nhân',
                                      options: [
                                        'Tăng HA',
                                        'DDMMN',
                                        'Phình mạch',
                                        'HHTMN',
                                        'Khác',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(4)) {
                                      _chandoanxhnkhac.value = true;
                                    } else {
                                      _chandoanxhnkhac.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder(
                                    valueListenable: _chandoanxhnkhac,
                                    builder: (_, value, child) {
                                      return value
                                          ? Container(
                                              child: QuestionTextWidget(
                                                questionItem: QuestionItem(title: ''),
                                              ),
                                            )
                                          : Container();
                                    },
                                  ),
                                ),
                                Row(children: [
                                  QuestionTextWidget(
                                    questionItem: QuestionItem(title: 'Đã điều trị băng phương pháp'),
                                  ),
                                  QuestionTextWidget(
                                    questionItem: QuestionItem(title: 'Tại bệnh viện'),
                                  ),
                                  QuestionDateWidget(
                                    questionItem: QuestionItem(title: 'Thời điểm'),
                                  ),
                                ]),
                              ],
                            )
                          : Container();
                    },
                  ),
                ),
              ],
            ),
          ),
          _tinhtrangbenh()
        ],
      ),
    );
  }

  _tableCellCheck({double height = 30}) {
    return Expanded(
      child: Container(
        decoration: containerDecoration(radius: 0, borderColor: Colors.blue),
        height: height,
        alignment: Alignment.centerLeft,
        child: QuestionMultipleWidget(
          questionItem: QuestionItem(title: '', options: ['']),
        ),
      ),
    );
  }

  _tinhtrangbenh() {
    return Container(
      margin: EdgeInsets.all(8),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        children: [
          SectionWidget(
            title: "Tình trạng bệnh",
          ),
          Container(
            height: 0.5,
            color: Colors.blue,
          ),
          Container(
              child: Column(children: [
            Container(
//                  decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                TableCellItem(title: 'Thời điểm'),
                TableCellItem(title: 'M'),
                TableCellItem(title: 'T(o)'),
                TableCellItem(title: 'HA tthu mmHg'),
                TableCellItem(title: 'HA ttrương mmHg'),
                TableCellItem(title: 'HATB mmHg'),
                TableCellItem(title: 'NT l/p'),
                TableCellItem(title: 'Kiểu thở'),
                TableCellItem(title: 'HA tthu mmHg'),
                TableCellItem(title: 'HA tthu mmHg'),
                TableCellItem(title: 'NKQ'),
                TableCellItem(title: 'HTTK'),
              ]),
            ),
            Container(
//                  decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
              child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                TableCellItem(title: '', height: 40),
                _tableCellCheck(height: 40),
                TableCellItem(title: '', height: 40),
              ]),
            ),
            Container(
              alignment: Alignment.centerRight,
              child: FlatButton(
                color: Colors.blue,
                onPressed: () {},
                child: Text('Thêm'),
              ),
            )
          ])),
          Row(
            children: [
              Container(
                child: Text('Thần kinh '),
                padding: EdgeInsets.all(6),
              ),
              Container(
                child: Text('Nội khoa'),
                padding: EdgeInsets.all(6),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
          Container(
            height: 0.5,
            color: Colors.blue,
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                width: (MediaQuery.of(context).size.width / 2 - 300),
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionYNShowMultipleWidget(
                      title: 'RL chức năng thần kinh và TALNS',
                      extraOptions: [
                        'Đau đầu',
                        'Nôn ói',
                        'Giảm LOC',
                        'Thay đổi sinh hiệu',
                        'THA',
                        'Tăng nhịp tim',
                        'Chậm nhịp tim',
                        'Kiểu hô hấp bất thường',
                        'Co giật',
                        'Co giật',
                        'Co giật dưới lâm sàng'
                      ],
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Wrap(
                            children: [
                              QuestionButtonWidget(
                                questionItem: QuestionItem(title: "GCS"),
                              ),
                              QuestionDateWidget(questionItem: QuestionItem(title: 'So với đánh giá lúc')),
                              QuestionButtonWidget(
                                questionItem: QuestionItem(title: "GCS trước đó"),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              QuestionSingleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: '',
                                    options: [
                                      'Tăng ',
                                      'Giảm ',
                                    ],
                                    id: "1009"),
                                callback: (index) {},
                              ),
                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nhập điểm '))
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Wrap(
                            children: [
                              QuestionButtonWidget(
                                questionItem: QuestionItem(title: "NIHSS"),
                              ),
                              QuestionDateWidget(questionItem: QuestionItem(title: 'so với đánh giá lúc')),
                              QuestionButtonWidget(
                                questionItem: QuestionItem(title: "NIHSS trước đó"),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              QuestionSingleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: '',
                                    options: [
                                      'Tăng ',
                                      'Giảm ',
                                    ],
                                    id: "1009"),
                                callback: (index) {},
                              ),
                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nhập điểm '))
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      margin: EdgeInsets.all(10),
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          QuestionTextWidget(questionItem: QuestionItem(title: 'VXHN cm3')),
                          QuestionSingleWidget(questionItem: QuestionItem(title: '', options: ['Tăng', 'Giảm'])),
                          QuestionDateWidget(questionItem: QuestionItem(title: 'so với đánh giá lúc'))
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 5),
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Wrap(
                            children: [
                              QuestionButtonWidget(
                                questionItem: QuestionItem(title: "ICH Score"),
                              ),
                              QuestionDateWidget(questionItem: QuestionItem(title: 'so với đánh giá lúc')),
                              QuestionButtonWidget(
                                questionItem: QuestionItem(title: "ICH Score trước đó"),
                              ),
                            ],
                          ),
                          Row(
                            children: [
                              QuestionSingleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: '',
                                    options: [
                                      'Tăng ',
                                      'Giảm ',
                                    ],
                                    id: "1009"),
                                callback: (index) {},
                              ),
                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nhập điểm '))
                            ],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: containerDecoration(),
                      child: Column(
                        children: [
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: '',
                                options: [
                                  'Vị trí XHN',
                                ],
                                id: "1009"),
                            callback: (index, select, indexes) {
                              if (indexes.contains(index)) {
                                _vitrixhn.value = true;
                              } else {
                                _vitrixhn.value = false;
                              }
                            },
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 40),
                            child: ValueListenableBuilder<bool>(
                                valueListenable: _vitrixhn,
                                builder: (_, value, child) {
                                  return value
                                      ? QuestionMultipleWidget(
                                          optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'Trong nhu mô não',
                                              ],
                                              id: "1009"),
                                          callback: (index, select, indexes) {
                                            if (indexes.contains(index)) {
                                              _trongnhumonao.value = true;
                                            } else {
                                              _trongnhumonao.value = false;
                                            }
                                          },
                                        )
                                      : Container();
                                }),
                          ),
                          ValueListenableBuilder<bool>(
                            valueListenable: _trongnhumonao,
                            builder: (_, value, child) {
                              return value
                                  ? Container(
                                      margin: EdgeInsets.only(left: 80),
                                      child: QuestionMultipleWidget(
                                        optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: [
                                              'Dưới vỏ',
                                              'basal ganglia,',
                                              'thalamus',
                                              'brainstem,',
                                              'cerebellum;',
                                              'Vỏ não'
                                            ],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      ),
                                    )
                                  : Container();
                            },
                          ),
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: '',
                                options: ['XHMN', 'Dưới màng cứng', 'Ngoài màng cứng', 'Não thất.'],
                                id: "1009"),
                            callback: (index, select, indexes) {
                              if (indexes.contains(index)) {
                                _xhmn.value = true;
                              } else {
                                _xhmn.value = false;
                              }
                            },
                          ),
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: '',
                                options: ['Spot sign', 'Đầy lệch đường giữa', 'Não úng thủy', 'gợi ý tụt não'],
                                id: "1009"),
                            callback: (index, select, indexes) {},
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: containerDecoration(borderColor: Colors.blue),
                      margin: EdgeInsets.only(top: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: 'Yếu tố dự đoán diễn tiến xấu',
                                options: [
                                  'Tuổi > 80',
                                  'GCS thấp (<9)',
                                  'NIHSS cao',
                                  'ICH cao',
                                  'THA khó kiểm soát',
                                  'Đang dùng kháng đông hoặc KKTTC',
                                ],
                                id: "1009"),
                            callback: (index, select, indexes) {},
                          ),
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE, title: '', options: ['VXHN lớn'], id: "1009"),
                            callback: (index, select, indexes) {
                              if (indexes.contains(index)) {
                                _vxhn.value = true;
                              } else {
                                _vxhn.value = false;
                              }
                            },
                          ),
                          ValueListenableBuilder<bool>(
                            valueListenable: _vxhn,
                            builder: (_, value, child) {
                              return value
                                  ? Container(
                                      margin: EdgeInsets.only(left: 40),
                                      child: QuestionMultipleWidget(
                                        optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: ['>30ml', '30-60ml', '>60ml'],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      ),
                                    )
                                  : Container();
                            },
                          ),
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: '',
                                options: [
                                  'Ổ XHN không đồng nhất/CT scan ban đầu',
                                  'Thân não',
                                  'đồi thị',
                                  'XHN và XHNT',
                                  '“Spot sign” / CT angiography',
                                  'Giảm tiểu cầu',
                                  'Giảm chức năng tiểu cầu',
                                  'Tăng Creatinine'
                                ],
                                id: "1009"),
                            callback: (index, select, indexes) {},
                          ),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(10),
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        children: [
                          QuestionYNShowMultipleWidget(
                            title: 'Biến chứng',
                            extraOptions: ['Co giật', 'TALNS', 'Tụt não '],
                          ),
                        ],
                      ),
                    ),
                    Container(
                      decoration: containerDecoration(borderColor: Colors.blue),
                      margin: EdgeInsets.all(10),
                      child: Column(
                        children: [
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: '',
                                options: ['Cần uống thuốc hoặc Dinh dưỡng qua đường miệng'],
                                id: "1009"),
                            callback: (index, select, indexes) {
                              if (indexes.contains(index)) {
                                _canuongthuot.value = true;
                              } else {
                                _canuongthuot.value = false;
                              }
                            },
                          ),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: ValueListenableBuilder<bool>(
                              valueListenable: _canuongthuot,
                              builder: (_, value, child) {
                                return value
                                    ? Container(
                                        margin: EdgeInsets.only(left: 40),
                                        child: QuestionMultipleWidget(
                                          optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'Test nuốt',
                                              ],
                                              id: "1009"),
                                          callback: (index, select, indexes) {
                                            if (indexes.contains(index)) {
                                              _testnuot.value = true;
                                            } else {
                                              _testnuot.value = false;
                                            }
                                          },
                                        ),
                                      )
                                    : Container();
                              },
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.all(10),
                            child: ValueListenableBuilder<bool>(
                              valueListenable: _testnuot,
                              builder: (_, value, child) {
                                return value
                                    ? Container(
                                        margin: EdgeInsets.only(left: 80),
                                        child: QuestionMultipleWidget(
                                          optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'không qua được',
                                                'Đặt ống thông mũi miệng',
                                                'Đạt',
                                                'Dinh dưỡng đường miệng'
                                              ],
                                              id: "1009"),
                                          callback: (index, select, indexes) {},
                                        ),
                                      )
                                    : Container();
                              },
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
              Container(
                width: (MediaQuery.of(context).size.width / 2 - 300),
                alignment: Alignment.topLeft,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionYNShowMultipleWidget(
                      title: 'Cần điều chỉnh huyết áp vì',
                      callbackYN: (id) {
                        if (id == 0) {
                          _dieuchinhhuyetap.value = true;
                        } else {
                          _dieuchinhhuyetap.value = false;
                        }
                      },
                    ),
                    ValueListenableBuilder<bool>(
                      valueListenable: _dieuchinhhuyetap,
                      builder: (_, value, child) {
                        return value
                            ? Container(
                                margin: EdgeInsets.only(left: 40),
                                child: Column(
                                  children: [
                                    QuestionMultipleWidget(
                                      questionItem: QuestionItem(
                                          type: QuestionType.SINGLE_CHOICE,
                                          title: 'i)',
                                          options: ['HA tthu > 200mmHg ', 'HATB > 140mmHg (dùng đường TM )'],
                                          id: "1005"),
                                    ),
                                    QuestionMultipleWidget(
                                      questionItem: QuestionItem(
                                          type: QuestionType.SINGLE_CHOICE,
                                          title: 'ii)',
                                          options: [
                                            'HA tthu > 180mmHg ',
                                            'HATB > 130mmHg và nghi ngờ TALNS - điều trị làm ALNS, theo dỏi ALNS và kiểm soát HA (đường TM hoặc đường uống) – Duy trì ALTMN > 60 – 80 mmHg'
                                          ],
                                          id: "1005"),
                                    ),
                                    QuestionMultipleWidget(
                                      questionItem: QuestionItem(
                                          type: QuestionType.SINGLE_CHOICE,
                                          title: 'iii)',
                                          options: [
                                            'HA tthu > 180mmHg',
                                            'HATB > 130mmHg và Không TALNS - kiểm soát HA (đường TM hoặc đường uống) – Duy trì HA Tthu 140 - 160mmHg hoặc HATB 90 – 110mmHg'
                                          ],
                                          id: "1005"),
                                    ),
                                    QuestionMultipleWidget(
                                      optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: 'iv)',
                                          options: [
                                            'Có tổn thương các cơ quan đích do THA',
                                            'Suy tim xung huyết,',
                                            'Dọa OAP, ...'
                                          ],
                                          id: "1009"),
                                      callback: (index, select, indexes) {},
                                    ),
                                  ],
                                ),
                              )
                            : Container();
                      },
//                      child: ,
                    ),
                    QuestionYNShowMultipleWidget(
                      title: 'Cần điều chỉnh tình trạng đông máu vì',
                      extraOptions: ['Liên quan thuốc kháng đông, KKTTC', 'CLS chứng minh RLĐM', 'Bệnh Nội khoa'],
                    ),
                    QuestionYNShowMultipleWidget(
                      title: 'Cần điều chỉnh Đường huyết vì',
                      extraOptions: [
                        'ĐH thấp (<60 -80mg%)',
                        'ĐH cao – duy trì 140 – 180mg%',
                      ],
                    ),
                    QuestionYNShowMultipleWidget(
                      title: 'Đánh giá tình trạng cân bằng nước',
                      extraOptions: ['Giảm V', 'Bình V', 'Tăng V'],
                    ),
                    QuestionYNShowTextFieldWidget(
                      title: 'Các tình trạng nội khoa đáng chú ý',
                      subs: ['', 'Mức độ', '', 'Mức độ', '', 'Mức độ'],
                    )
                  ],
                ),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
          ),
          Container(
            decoration: containerDecoration(),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Container(
                  decoration: containerDecoration(borderColor: Colors.blue),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    children: [
                      QuestionMultipleWidget(
                        questionItem: QuestionItem(
                          title: 'Diễn tiến',
                          options: ['Tốt hơn', 'Không thay đổi', 'Xấu hơn'],
                        ),
                        callback: (index, select, indexes) {
                          if (indexes.contains(2)) {
                            _dientienxauhon.value = true;
                          } else {
                            _dientienxauhon.value = false;
                          }
                        },
                      ),
                      Container(
                          child: Column(children: [
                        Container(
//                  decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
                          child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                            TableCellItem(title: 'Thời điểm'),
                            TableCellItem(title: 'NIHSS'),
                            TableCellItem(title: 'ICH score'),
                          ]),
                        ),
                        Container(
                          child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                            TableCellItem(title: '', height: 40),
                            TableCellItem(title: '', height: 40),
                            TableCellItem(title: '', height: 40),
                          ]),
                        ),
                      ]))
                    ],
                  ),
                ),
                Row(
                  children: [
                    Container(
                      child: Text('Thần kinh XHN '),
                      padding: EdgeInsets.all(6),
                    ),
                    Container(
                      child: Text('Nội khoa'),
                      padding: EdgeInsets.all(6),
                    ),
                  ],
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                ),
                Row(
                  children: [
                    Container(
                      width: (MediaQuery.of(context).size.width / 2 - 300),
                      child: Column(
                        children: [
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: 'Lí do diễn tiến không thuận lợi',
                                options: [
                                  'Lan rộng ổ XHN',
                                  'Chèn ép thân não',
                                  'Chèn ép mô não xung quanh',
                                  'XHN mới',
                                  'Não úng thủy tiến triển',
                                  'Phù não',
                                  'TALNS và giảm áp lực tưới máu não',
                                  'Thoát vị não',
                                ],
                                id: "1009"),
                            callback: (index, select, indexes) {},
                          ),
                          QuestionMultipleWidget(
                            optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: 'Các biến chứng thần kinh',
                                options: [
                                  'TALNS',
                                  'Phù não và thoát vị',
                                  'Não úng thủy',
                                  'Co giật và Động kinh',
                                  'Sảng hoặc lú lẫn',
                                  'Nấc cụt',
                                  'Khó nuốt và viêm phổi hít',
                                  'Rối loạn giấc ngũ',
                                  'Rối loạn vận động',
                                  'Co giật',
                                  'Rối loạn dinh dưỡng'
                                ],
                                id: "1009"),
                            callback: (index, select, indexes) {},
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: (MediaQuery.of(context).size.width / 2 - 300),
                      child: Column(
                        children: [
                          QuestionMultipleWidget(
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: 'Lí do diễn tiến không thuận lợi liên quan các NN nội khoa',
                                options: [
                                  'Mất nước',
                                  'Giảm HA',
                                  'Tăng HA quá mức',
                                  'Sốt cao',
                                  'Tăng ĐH',
                                  'Hạ ĐH',
                                  'Tăng thông khí, ứ CO2',
                                  'Hypoxia',
                                  'Hít sặc và viêm phổi',
                                  'Nhiễm trùng',
                                  'Thuyên tắc phổi',
                                  'Thiếu máu cơ tim, NMCT'
                                      'Loạn nhịp tim',
                                  'Suy tim',
                                  'Quá tải tuần hoàn',
                                  'Phù phổi do nguyên nhân thần kinh',
                                  'Co giật, Động kinh',
                                  'Hạ Natri máu',
                                  'Tăng Natri máu',
                                  'Các rối loạn cân băng điện giải khác',
                                  'Rối loạn toan kiềm',
                                  'Thiếu máu',
                                  'Thiếu Thiamine',
                                  'Suy gan',
                                  'Suy thận',
                                  'Sảng',
                                  'Độc chất',
                                  'Các yếu tố tâm lý'
                                ],
                                id: "1009"),
                            callback: (index, select, indexes) {},
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    color: Colors.blue,
                    onPressed: () {},
                    child: Text('Thêm'),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
