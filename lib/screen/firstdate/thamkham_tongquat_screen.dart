import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_check_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_yn_show_textfield_widget.dart';
import 'package:flutter_app/widget/save_button.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class ThamKhamTongQuatScreen extends StatefulWidget {
  VoidCallback callback;

  ThamKhamTongQuatScreen(this.callback);

  @override
  State createState() {
    return _ThamKhamTongQuatScreenState();
  }
}

class _ThamKhamTongQuatScreenState extends State<ThamKhamTongQuatScreen> {
  ValueNotifier<bool> _dauDuNuoc = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dauMatNuoc = ValueNotifier<bool>(false);
  ValueNotifier<bool> _da = ValueNotifier<bool>(false);
  ValueNotifier<bool> _timmach = ValueNotifier<bool>(false);
  ValueNotifier<bool> _hohap = ValueNotifier<bool>(false);
  ValueNotifier<bool> _thankinh = ValueNotifier<bool>(false);
  ValueNotifier<bool> _khac = ValueNotifier<bool>(false);
  ValueNotifier<bool> _phuNao = ValueNotifier<bool>(false);
  ValueNotifier<bool> _sungHuyetPhoit = ValueNotifier<bool>(false);

  ValueNotifier<bool> _trieuChung = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dauChung = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tumauDuoiDa = ValueNotifier<bool>(false);
  ValueNotifier<bool> _thieuMau = ValueNotifier<bool>(false);

  ValueNotifier<bool> _bienChung = ValueNotifier<bool>(false);
  ValueNotifier<bool> _loanNhip = ValueNotifier<bool>(false);
  ValueNotifier<bool> _viemPhoi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tieuchuanBoard = ValueNotifier<bool>(false);
  ValueNotifier<bool> _lastLongNguc = ValueNotifier<bool>(false);
  ValueNotifier<bool> _trungThat = ValueNotifier<bool>(false);
  ValueNotifier<bool> _trungThatKhac = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm ')),
              Container(
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SectionWidget(
                      title: AppString.titleToanThan,
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(title: 'Thể trạng ', options:  ['Trung bình ', 'Gầy', 'Suy kiệt', 'Dư cân', 'béo phì', 'Béo phì tạng'],),
                    ),

                    QuestionMultipleWidget(
                      questionItem: QuestionItem(title: 'Da niêm', options:  ['Hồng', 'Nhạt ', 'Xanh', 'Tím tái'],),
                    ),

                    QuestionSingleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.SINGLE_CHOICE,
                          title: 'Phù toàn thân   ',
                          options: ['Nhẹ ', 'Vừa  ', 'Nặng '],
                          id: "1005"),
                      callback: (index) {},
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.SINGLE_CHOICE,
                          title: 'Phù Khu trú',
                          options: [
                            'Mặt',
                            'Phù áo khoát',
                            'Báng bụng',
                            '1/2 Người dưới',
                            '1/2 Cùng bên',
                            'Tay trái',
                            'tay phái',
                            'Chân trái',
                            'Chân phải'
                          ],
                          id: "1005"),
                    ),
                    QuestionSingle(
                      title: 'Dấu dư nước',
                      callback: (index) {
                        if (index == 0) {
                          _dauDuNuoc.value = true;
                        } else {
                          _dauDuNuoc.value = false;
                        }
                      },
                    ),
                    _dauDuNuocUI(),
                    _dauMatNuocUI(),
                    _dauSaoMachUI()
                  ],
                ),
              ),
              _dauMatCoUI(),
              _longNgucUI(),
              _bungUI(),
              _khacUI(),
              SaveWidget(
                callback: () {},
              )
            ],
          ),
        ),
      ),
    );
  }

  _longNgucUI() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleLongNguc,
          ),
          SectionWidget(
            size: 13,
            title: AppString.titleTim,
          ),
          Wrap(
            children: [
              QuestionSingleWidget(
                questionItem: QuestionItem(
                    type: QuestionType.SINGLE_CHOICE,
                    title: 'Diện đập ',
                    options: ['Bình thường ', 'Rộng '],
                    id: "1005"),
                callback: (index) {},
              ),
              QuestionSingleWidget(
                questionItem: QuestionItem(
                    type: QuestionType.SINGLE_CHOICE,
                    title: 'Vị trí mỏm tim  ',
                    options: ['Bình thường ', 'Ngoài đường trung đòn '],
                    id: "1005"),
                callback: (index) {},
              ),
            ],
          ),
          Wrap(
            children: [
              Row(
                children: [
                  QuestionYNShowMultipleWidget(
                    title: 'Các biểu hiện suy tim/lâm sàng',
                  ),
                  QuestionYNShowMultipleWidget(
                    title: 'NMCT',
                  ),
                  QuestionYNShowTextFieldWidget(
                    title: 'Loạn nhịp',
                    subs: ['Tên loạn nhịp'],

                  ),
                  QuestionYNShowMultipleWidget(
                    title: 'Bệnh cơ tim choáng váng',
                  ),
                ],
              ),

              Container(
                margin: EdgeInsets.only(left: 250),
                child: ValueListenableBuilder<bool>(
                    valueListenable: _loanNhip,
                    builder: (_, value, child) {
                      return value
                          ? Container(
                              child: QuestionTextWidget(
                                questionItem: QuestionItem(title: ""),
                              ),
                            )
                          : Container();
                    }),
              ),
              QuestionYNShowMultipleWidget(
                title: 'Biến chứng tim của tình trạng XHN',
                extraOptions: ['Suy tim cấp', 'OAP '],
              ),
            ],
          ),
          SectionWidget(
            size: 13,
            title: AppString.titlePhoi,
          ),
          QuestionMultipleWidget(
            width: 300,
            optionWidth: 200,
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: ['COPD', 'Hen PQ', 'Viêm phổi hít', 'Viêm phổi liên quan đột quị (dựa tiêu chuẩn'],
                id: "1009"),
            callback: (index, select, indexes) {
              if (indexes.contains(3)) {
                _tieuchuanBoard.value = true;
              } else {
                _tieuchuanBoard.value = false;
              }
            },
          ),
          ValueListenableBuilder<bool>(
            valueListenable: _tieuchuanBoard,
            builder: (_, value, child) {
              return value
                  ? Container(
                      margin: EdgeInsets.all(8),
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Container(
                            child: Text('T/C viêm phổi liên quan đột quị'),
                            padding: EdgeInsets.all(6),
                          ),
                          Container(
                            height: 0.5,
                            color: Colors.blue,
                          ),
                          Row(
                            children: [
                              Container(
                                child: Text('07 ngày đầu sau đột quị và không thông khí cơ học'),
                                padding: EdgeInsets.all(6),
                              ),
                              Container(
                                child: Text('Sau 07 ngày đột quị và/hoặc thông khí cơ học'),
                                padding: EdgeInsets.all(6),
                              ),
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          ),
                          Container(
                            height: 0.5,
                            color: Colors.blue,
                          ),
                          Row(
                            children: [
                              Container(
                                width: (MediaQuery.of(context).size.width / 2 - 300),
                                child: Column(
                                  children: [
                                    QuestionMultipleWidget(
                                      optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'Ít nhất 1 trong các tiêu chuẩn sau',
                                          ],
                                          id: "1009"),
                                      callback: (index, select, indexes) {},
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: QuestionMultipleWidget(
                                        optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: [
                                              'Sốt (>38°C) mà không giải thích được bằng nguyên nhân khác.',
                                              'Leukopenia (<4000 WBC/mm3) hoặc leukocytosis (>12 000 WBC/mm3)',
                                              'Đối với người lớn ≥70 tuổi: thay đổi ý thức tri giác mà không giải thích được bằng nguyên nhân khác'
                                            ],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      ),
                                    ),


                                    QuestionMultipleWidget(
                                      optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'VÀ ít nhất 2 trong các tiêu chuẩn sau:',
                                          ],
                                          id: "1009"),
                                      callback: (index, select, indexes) {},
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: QuestionMultipleWidget(
                                        optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: [
                                              'Xuất hiện mới đàm mủ, hoặc thay đổi đặc tính đàm trên 24 giờ, hoặc tăng tiết đàm hoặc tăng yêu cầu hút đàm.',
                                              'Xuất hiện mới hoặc ho nặng hơn, hoặc khó thở hoặc tachypnea (>25/phút)',
                                              'Nghe phổi: Rales, crackles, hoặc bronchial breath sound'
                                                  'Nghe phổi: Rales, crackles, hoặc bronchial breath sound',
                                              'Trao đổi khí xấu (vd: bào hòa O2: PaO2/FiO2≤240], tăng nhu cầu O2)'
                                            ],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      ),
                                    ),
                                    QuestionMultipleWidget(
                                      optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'VÀ XQ phổi',
                                          ],
                                          id: "1009"),
                                      callback: (index, select, indexes) {},
                                    ),

                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: QuestionMultipleWidget(
                                        optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: [
                                              '≥ 2 hình ảnh có ít nhất 1 trong các biểu hiện Thâm nhiểm, đông đặc hoặc tạo hang mới hoặc tiến triển',
                                              '1 hình ảnh có 1 trong các biểu hiện Thâm nhiểm, đông đặc hoặc tạo hang nếu BN không có bệnh tim hoặc bệnh phổi trước đó'
                                            ],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      ),
                                    ),
                                  ],
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                ),
                              ),
                              Container(
                                width: (MediaQuery.of(context).size.width / 2 - 300),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    QuestionMultipleWidget(
                                      optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'Hình ảnh tổn thương trên XQ phổi: Thâm nhiểm, đông đặc hoặc tạo hang mới hoặc tiến triển.'
                                          ],
                                          id: "1009"),
                                      callback: (index, select, indexes) {},
                                    ),
                                    QuestionMultipleWidget(
                                      optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'VÀ ít nhất 2 trong các tiêu chuẩn sau:',
                                          ],
                                          id: "1009"),
                                      callback: (index, select, indexes) {},
                                    ),
                                    Container(
                                      margin: EdgeInsets.only(left: 20),
                                      child: QuestionMultipleWidget(
                                        optionWidth: MediaQuery.of(context).size.width / 2 - 400,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: [
                                              'Sốt (>38°C) mà không giải thích được bằng nguyên nhân khác',
                                              'Leukopenia (<4000 WBC/mm3) hoặc leukocytosis (>12 000 WBC/mm3',
                                              'Thay đổi ý thức tri giác (≥70 tuổi)mà không giải thích được bằng nguyên nhân khác'
                                                  'Xuất hiện mới đàm mủ, hoặc thay đổi đặc tính đàm trên 24 giờ, hoặc tăng tiết đàm hoặc tăng yêu cầu hút đàm.'
                                                  'Trao đổi khí xấu (vd: bào hòa O2: PaO2/FiO2≤240], tăng nhu cầu O2)',
                                              'Xuất hiện mới hoặc ho nặng hơn, hoặc khó thở hoặc tachypnea (>25/phút)',
                                              'Nghe phổi: Rales, crackles, hoặc bronchial breath sounds'
                                            ],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          ),
                          Container(
                            child: Text("Kết luận "),
                          ),
                          QuestionMultipleWidget(questionItem: QuestionItem(title: '', options: ['Xác định SAP:'])),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: QuestionMultipleWidget(
                              questionItem: QuestionItem(
                                  title: '', options: ['Tất cả thành phần tiêu chuẩn trên', 'XQ phổi ít nhất 01 lần']),
                              optionWidth: 300,
                            ),
                          ),
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: '', options: ['Nhiều khả năng SAP: '])),
                          Container(
                            margin: EdgeInsets.only(left: 20),
                            child: QuestionMultipleWidget(
                              questionItem: QuestionItem(title: '', options: [
                                'Tất cả thành phần tiêu chuẩn trên đều có '
                                    'nhưng thiếu sự thay đổi trên XQ phổi ban đầu',
                                'VÀ XQ phổi kiểm tra (hoặc không thể có XQ phổi), ',
                                'Và không thể giải thích hoặc chẩn đoán khác'
                              ]),
                              optionWidth: 300,
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container();
            },
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: ['TKMP', 'TDMP', 'Gợi ý thuyên tắc phổi', 'Yếu cơ hô hấp', 'Khác'],
                id: "1009"),
            callback: (index, select, indexes) {
              if (indexes.contains(4)) {
                _lastLongNguc.value = true;
              } else {
                _lastLongNguc.value = false;
              }
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 50),
            child: ValueListenableBuilder<bool>(
                valueListenable: _lastLongNguc,
                builder: (_, value, child) {
                  return value ? QuestionTextWidget(questionItem: QuestionItem(title: "Nhập")) : Container();
                }),
          ),
          _trungThatUI()
        ],
      ),
    );
  }

  _dauMatCoUI() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleDauMatCo,
          ),
          QuestionYNShowMultipleWidget(
            title: 'Tụ máu dưới da ',
            extraOptions: [
              'Đầu',
              'Mặt ',
              'Cổ ',
            ],
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: ' ',
                options: ['Sưng đau vùng cổ ', 'Vẻ mặt cushing  ', 'Vẻ mặt nhiễm trùng '],
                id: "1009"),

          ),
          QuestionYNShowMultipleWidget(
            title: 'Thiếu máu ',
            extraOptions: [
              'Da xanh hoặc vàng tái ',
              'Niêm nhạt  ',
            ],
          ),
          Row(
            children: [
              QuestionYNShowMultipleWidget(
                title: 'Xuất huyết/đáy mắt' ,
              ),
              QuestionYNShowMultipleWidget(
                title: 'Tắt nghẽn Airway ' ,
              ),
              QuestionYNShowMultipleWidget(
                title: 'Âm thổi' ,
              ),
            ],
          )

        ],
      ),
    );
  }

  _bungUI() {
    List<String> coquans = ['Gan', 'Lách', 'Thận', 'Khác', 'Khác', 'Khác'];
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleBung,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(coquans.length, (index) {
              return Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    QuestionTextWidget(
                      questionItem: QuestionItem(title: '', answer: coquans[index]),
                    ),
                    QuestionYNShowTextFieldWidget(title: '', yesno: ['Bình thường', 'Bất thường'], subs: ['Mô tả'], correctIndex: 1,),
                  ],
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  _khacUI() {
    List<String> coquans = ['Da', 'Lông ', 'Tóc ', 'Móng', 'Khớp ', 'Mạch máu ngoại biên', 'Khác', 'Khác', 'Khác'];
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleKhac,
          ),

          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(coquans.length, (index) {
              return Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    QuestionTextWidget(
                      questionItem: QuestionItem(title: '', answer: coquans[index]),
                    ),
                    QuestionYNShowTextFieldWidget(title: '', yesno: ['Bình thường', 'Bất thường'], subs: ['Mô tả'], correctIndex: 1,),
                  ],
                ),
              );
            }),
          ),
        ],
      ),
    );
  }

  _dauSaoMachUI() {
    return Column(
      children: [
        QuestionYNShowMultipleWidget(title: 'Dấu sao mạch ', ),
        Container(
          child: Column(
            children: [
              QuestionYNShowMultipleWidget(
                title: 'Trầy xướt do chấn thương ',
                extraOptions: [
                  'Đầu mặt ',
                  'Cổ ',
                  'Ngực bụng ',
                  'Tay chân ',
                ],
              ),
              QuestionYNShowMultipleWidget(
                title: 'Mảng, chấm xuất huyết ',
                extraOptions: [
                  'Đầu mặt ',
                  'Cổ ',
                  'Ngực bụng ',
                  'Tay chân ',
                ],
              )
            ],
          ),
        ),
      ],
    );
  }

  _dauDuNuocUI() {
    return Container(
      margin: EdgeInsets.only(left: 20),
      child: Column(
        children: [
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: [
                  'Da',
                ],
                id: "1006"),
            callback: (index, select, indexes) {},
          ),
          Container(
            decoration: containerDecoration(borderColor: Colors.blue),
            margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
            child: QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: '',
                  options: [
                    'Phù lạnh vùng trũng ',
                    'Phù mi mắt, mặt',
                    'Phù chi (phù mô kẻ',
                  ],
                  id: "1006"),
            ),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: [
                  'Tim mạch',
                ],
                id: "1006"),
            callback: (index, select, indexes) {},
          ),
          Container(
            decoration: containerDecoration(borderColor: Colors.blue),
            margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
            child: QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: '',
                  options: ['Mạch ngoại biên đầy, nảy mạnh', 'TM ngoại biên nổi ', 'TM cảnh nổi ', 'Tăng huyết áp'],
                  id: "1006"),
            ),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: [
                  'Hô hấp ',
                ],
                id: "1006"),
            callback: (index, select, indexes) {},
          ),
          Container(
            margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                QuestionMultipleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: '',
                      options: [
                        'Tăng nhịp thở ',
                        'Thở nông',
                        'Khó thở khi gắng sức hoặc nằm ngửa ',
                        'Sung huyết phổi và phù phổi '
                      ],
                      id: "1006"),
                  callback: (index, select, indexes) {
                    if (indexes.contains(3)) {
                      _sungHuyetPhoit.value = true;
                    } else {
                      _sungHuyetPhoit.value = false;
                    }
                  },
                ),
                ValueListenableBuilder<bool>(
                    valueListenable: _sungHuyetPhoit,
                    builder: (_, value, child) {
                      return value
                          ? Container(
                              margin: EdgeInsets.only(left: 40),
                              child: QuestionMultipleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: '',
                                    options: [
                                      'Ho',
                                      'Ran ngáy, rít ',
                                      'Ran ẩm ',
                                    ],
                                    id: "1006"),
                              ),
                            )
                          : Container();
                    }),
              ],
            ),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: [
                  'Thần kinh  ',
                ],
                id: "1006"),
            callback: (index, select, indexes) {},
          ),
          Container(
            decoration: containerDecoration(borderColor: Colors.blue),
            margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
            child: Column(
              children: [
                QuestionMultipleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: '',
                      options: ['Thay đổi mức độ ý thức', 'Rối loạn thị giác', 'Yếu cơ', 'Dị cảm ', 'Phù não '],
                      id: "1006"),
                  callback: (index, select, indexes) {
                    if (indexes.contains(index)) {
                      _phuNao.value = true;
                    } else {
                      _phuNao.value = false;
                    }
                  },
                ),
                ValueListenableBuilder<bool>(
                    valueListenable: _phuNao,
                    builder: (_, value, child) {
                      return value
                          ? Container(
                              margin: EdgeInsets.only(left: 40),
                              child: QuestionMultipleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: '',
                                    options: ['Đau đầu ', 'Lú lẫn ', 'Lơ mơ ', 'Giảm phản xạ ', 'Co giật  ', 'Hôn mê '],
                                    id: "1006"),
                              ),
                            )
                          : Container();
                    }),
              ],
            ),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(
                type: QuestionType.MULTIPLE_CHOICE,
                title: '',
                options: [
                  'Khác',
                ],
                id: "1006"),
            callback: (index, select, indexes) {},
          ),
          Container(
            decoration: containerDecoration(borderColor: Colors.blue),
            margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
            child: QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: '',
                  options: [
                    'Đa niệu',
                    'Đa nước tiểu về đêm ',
                    'Giảm HCT ',
                    'Giảm BUN ',
                    'Báng bụng ',
                    'Gan to',
                    'Tăng NĐR '
                  ],
                  id: "1006"),
            ),
          )
        ],
      ),
    );
  }

  _dauMatNuocUI() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        QuestionMultipleWidget(
          questionItem: QuestionItem(
              type: QuestionType.MULTIPLE_CHOICE,
              title: ' ',
              options: [
                'Dấu mất nước ',
              ],
              id: "1009"),
          callback: (index, select, indexes) {},
        ),
        Container(
          margin: EdgeInsets.only(left: 20),
          child: Column(
            children: [
              QuestionMultipleWidget(
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: '',
                    options: [
                      'Triệu chứng ',
                    ],
                    id: "1006"),
                callback: (index, select, indexes) {},
              ),
              Container(
                decoration: containerDecoration(borderColor: Colors.blue),
                margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
                child: QuestionMultipleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: '',
                      options: [
                        'Khát  ',
                        'Ít đi tiểu  ',
                        'Táo bón ',
                        'Mệt mỏi',
                        'Chóng mặt',
                        'Yếu chi',
                        'Chuột rút',
                        'Cứng khớp'
                      ],
                      id: "1006"),
                ),
              ),
              QuestionMultipleWidget(
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: '',
                    options: [
                      'Dấu chứng ',
                    ],
                    id: "1006"),
                callback: (index, select, indexes) {},
              ),
              Container(
                decoration: containerDecoration(borderColor: Colors.blue),
                margin: EdgeInsets.only(left: 40, right: 8, bottom: 8),
                child: QuestionMultipleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: '',
                      options: [
                        'Da khô ',
                        'Da và chi trở nên tái ',
                        'Mắt trũng',
                        'Môi, lưỡi khô',
                        'Mất tính đàn hồi',
                        'Run',
                        'Kích động',
                        'Ảo giác, hoang tưởng, sảng',
                        'Thay đổi màu nước tiểu',
                        'Giảm nhãn áp',
                        'Mạch nhanh ',
                        'Giảm huyết áp'
                      ],
                      id: "1006"),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }

  _trungThatUI() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 16),
//      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleTrungThat,
          ),
          QuestionYNShowMultipleWidget(
            title: 'Gợi ý bóc tách ĐM chủ ngực',
            extraOptions: ['Đau ngực', 'Khó thở ', 'Bứt rứt ', 'Tụt HA', 'Khác'],
            callBackChildOption: (indexes) {
              if (indexes.contains(4)) {
                _trungThatKhac.value = true;
              } else {
                _trungThatKhac.value = false;
              }
            },
          ),
          ValueListenableBuilder<bool>(
            valueListenable: _trungThat,
            builder: (_, value, child) {
              return value
                  ? Container(
                      margin: EdgeInsets.only(left: 40),
                      child: QuestionMultipleWidget(
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: ' ',
                            options: ['Đau ngự', 'Khó thở ', 'Bứt rứt ', 'Tụt HA', 'Khác'],
                            id: "1009"),
                        callback: (index, select, indexes) {
                          if (indexes.contains(4)) {
                            _trungThatKhac.value = true;
                          } else {
                            _trungThatKhac.value = false;
                          }
                        },
                      ),
                    )
                  : Container();
            },
          ),
          Container(
            alignment: Alignment.topRight,
//            margin: EdgeInsets.only(left: 400),
            child: ValueListenableBuilder<bool>(
                valueListenable: _trungThatKhac,
                builder: (_, value, child) {
                  return value ? QuestionTextWidget(questionItem: QuestionItem(title: "Nhập")) : Container();
                }),
          )
        ],
      ),
    );
  }
}
