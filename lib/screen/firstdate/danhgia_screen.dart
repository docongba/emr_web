import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/save_button.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class DanhGiaScreen extends StatefulWidget {

  DanhGiaScreen();

  @override
  State createState() {
    return _DanhGiaScreenState();
  }
}

class _DanhGiaScreenState extends State<DanhGiaScreen> {
  ValueNotifier<bool> _monitor = ValueNotifier<bool>(false);
  ValueNotifier<bool> _kieutho = ValueNotifier<bool>(false);
  ValueNotifier<bool> _cobieuhienxvdm = ValueNotifier<bool>(false);
  ValueNotifier<bool> _nkq = ValueNotifier<bool>(false);
  ValueNotifier<bool> _httk = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tim = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tuchi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _amthoi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _amthoivungmat = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tuongdong = ValueNotifier<bool>(false);
  ValueNotifier<bool> _hatb = ValueNotifier<bool>(false);
  ValueNotifier<bool> _canmonitor = ValueNotifier<bool>(false);
  ValueNotifier<bool> _naydeu = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm ')),
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    SectionWidget(
                      title: AppString.titleDauHieuSinhTon,
                    ),
                    Wrap(
                      children: List.generate(DataFirstDate().danhgiaQuestionSet().length, (index) {
                        var item = DataFirstDate().danhgiaQuestionSet()[index];
                        return QuestionTextWidget(
                          questionItem: item,
                          callback: (value) {},
                        );
                      }),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  children: [
                    QuestionYNShowMultipleWidget(title: 'Monitor', extraOptions: [
                      '1h',
                      '2h',
                      '4h',
                      '6h',
                      '8h',
                      '12h',
                      '24h',
                    ],)
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SectionWidget(
                      title: AppString.titleHoHp,
                    ),
                    Column(
                      children: [
                        Wrap(
                          children: [
                            QuestionTextWidget(
                              questionItem:
                                  QuestionItem(type: QuestionType.TEXT, title: 'SpO2', options: [], id: "1007"),
                            ),
                            QuestionYNShowMultipleWidget(hasDecor: true, showIndex: 1, title: 'Kiểu thở', mainOptions: ['Bình thường', 'Bất thường'],
                                extraOptions: [
                                  'Gắng sức',
                                  'Đảo ngược',
                                  'Nhiều đàm',
                                  'Giọng nói ứ đọng nước bọt',
                                  'Phập phồng mũi',
                                  'Tiếng rít thanh quản',
                                  'Tụt lưỡi',
                                  'Co kéo các cơ hô hấp khác',
                                  'Ho không hiệu quả',
                                  'Tiếng khò khè do co thắt PQ',
                                  'Lồng ngực hình thùng',
                                ],),

                          ],
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16, left: 10, right: 10),
                          width: MediaQuery.of(context).size.width,
                          decoration: containerDecoration(borderColor: Colors.blue),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              QuestionSingleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.SINGLE_CHOICE, title: AppString.titleHoNKQ, options: ['Có', 'Không'], id: "1009"),
                                callback: (index) {
                                  print("SHOW SELECTED INDEX $index");
                                  if (index == 0) {
                                    _nkq.value = true;
                                  } else {
                                    _nkq.value = false;
                                  }
                                },
                              ),
                              _nkqChilds(),
                            ],
                          ),
                        ),
                        Container(
                          margin: EdgeInsets.only(top: 16, left: 10, right: 10, bottom: 10),
                          width: MediaQuery.of(context).size.width,
                          decoration: containerDecoration(borderColor: Colors.blue),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [

                              QuestionSingleWidget(
                                questionItem: QuestionItem(
                                    type: QuestionType.SINGLE_CHOICE, title: AppString.titleHoHTTK, options: ['Có', 'Không'], id: "1005"),
                                callback: (index) {
                                  print("SHOW SELECTED INDEX $index");
                                  if (index == 0) {
                                    _httk.value = true;
                                  } else {
                                    _httk.value = false;
                                  }
                                },
                              ),
                              _httkChilds(),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SectionWidget(
                      title: AppString.titleTuanHoan,
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SectionWidget(
                            title: AppString.titleTim,
                          ),
                          QuestionSingleWidget(
                            questionItem: QuestionItem(
                                type: QuestionType.SINGLE_CHOICE,
                                title: 'Tim có loạn nhịp?',
                                options: ['Có', 'Không'],
                                id: "1009"),
                            callback: (index) {
                              print("SHOW SELECTED INDEX $index");
                              if (index == 0) {
                                _tim.value = true;
                              } else {
                                _tim.value = false;
                              }
                            },
                          ),
                          _timChilds(),
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SectionWidget(
                            title: AppString.titleHATB,
                          ),
                          QuestionTextWidget(questionItem: QuestionItem(title: 'mmHg')),


                          Container(
                            margin: EdgeInsets.only(left: 40),
                            child: ValueListenableBuilder<bool>(
                                valueListenable: _tuongdong,
                                builder: (_, value, child) {
                                  return value
                                      ? Wrap(
                                          children: [
                                            Container(
                                              child: Row(
                                                children: [
                                                  QuestionTextWidget(questionItem: QuestionItem(title: 'HATB max')),
                                                  QuestionTextWidget(questionItem: QuestionItem(title: 'Chi')),
                                                ],
                                              ),
                                              decoration: containerDecoration(),
                                            ),
                                            Container(
                                              child: Row(children: [
                                                QuestionTextWidget(questionItem: QuestionItem(title: 'HATB min')),
                                                QuestionTextWidget(questionItem: QuestionItem(title: 'Chi'))
                                              ]),
                                              decoration: containerDecoration(),
                                            ),
                                          ],
                                        )
                                      : Container();
                                }),
                          ),

                          QuestionSingleWidget(
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: ' ',
                                options: [
                                  'Cao',
                                  'Thấp ',
                                  'Đạt ',
                                ],
                                id: "1009"),
                            callback: (index) {
                                _hatb.value = true;
                            },
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 40),
                            child: ValueListenableBuilder<bool>(
                              valueListenable: _hatb,
                              builder: (_, value, child) {
                                return value
                                    ? QuestionMultipleWidget(
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: 'Đánh giá dựa vào',
                                            options: [
                                              'Dạng đột quỵ ',
                                              'Tổn thương não  ',
                                              'Giai đoạn đột quỵ  ',
                                              'Trạng thái bệnh lý nội khoa ',
                                              'Tiền căn'
                                            ],
                                            id: "1009"),
                                        callback: (index, select, indexes) {},
                                      )
                                    : Container();
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      width: MediaQuery.of(context).size.width,
                      decoration: containerDecoration(borderColor: Colors.blue),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SectionWidget(
                            title: AppString.titleMach,
                          ),
                          Row(
                            children: [
//                              uestionSingleWidget(
//                                questionItem: QuestionItem(
//                                    type: QuestionType.SINGLE_CHOICE,
//                                    title: 'Mạch?',
//                                    options: ['Đều', 'Không đều'],
//                                    id: "1009"),
//                                callback: (index) {
//                                  print("SHOW SELECTED INDEX $index");
//                                },
//                              ),
//                              SizedBox(
//                                width: 50,
//                              ),
//                              QuestionSingleWidget(
//                                questionItem: QuestionItem(
//                                    type: QuestionType.SINGLE_CHOICE,
//                                    title: 'Mạch có?',
//                                    options: ['Nhanh', 'Chậm'],
//                                    id: "1009"),
//                                callback: (index) {
//                                  print("SHOW SELECTED INDEX $index");
//                                },
//                              ),


                              Container(
                                decoration: containerDecoration(),
                                child: QuestionMultipleWidget(
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'Mạch nhanh',
                                        'Mạch chậm',
                                        'Mạch đều ',
                                        'Mạch không đều',
                                        'Đồng bộ nhịp tim ',
                                      ],
                                      id: "1009"),

                                ),
                              ),

                            ],
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 40),
                            child: Column(
                              children: [
                                QuestionSingleWidget(
                                  questionItem: QuestionItem(
                                      type: QuestionType.SINGLE_CHOICE,
                                      title: 'Ảnh hưởng huyết động',
                                      options: ['Có ', 'Không '],
                                      id: "1009"),
                                  callback: (index) {
                                    print("SHOW SELECTED INDEX $index");
                                  },
                                ),
                                Container(
                                  decoration: containerDecoration(),
                                  child: Row(
                                    children: [
                                      QuestionMultipleWidget(
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: 'Bị ảnh hưởng bởi',
                                            options: [
                                              'Bị ảnh hưởng thân nhiệt',
                                              'Bị ảnh hưởng hô hấp',

                                            ],
                                            id: "1009"),

                                      ), QuestionTextWidget(questionItem: QuestionItem(title: 'Khác '))
                                    ],
                                  ),
                                ),

                              ],
                            ),
                          ),
                          _machChilds(),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SectionWidget(
                      title: AppString.titleKhac,
                    ),
                    _bodyInfo()
                  ],
                ),
              ),
              SaveWidget(
                callback: () {},
              )
            ],
          ),
        ),
      ),
    );
  }

  _machChilds() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: 'Mạch',
                  options: [
                    'Vết tiêm chích bất thường',
                  ],
                  id: "1009"),
              callback: (index, select, indexes) {
                if (index == 5 && select) {
                  _tuchi.value = true;
                } else {
                  _tuchi.value = false;
                }
                print("SHOW SELECTED INDEX $index");
              },
            ),
            QuestionSingleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: 'Có biểu hiện của XVĐM? ',
                  options: [
                    'Có  ',
                    'Không',
                  ],
                  id: "1009"),
              callback: (index) {
                if (index == 0) {
                  _cobieuhienxvdm.value = true;
                } else {
                  _cobieuhienxvdm.value = false;
                }
              },
            ),

            QuestionSingleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: 'Nảy đều? ',
                  options: [
                    'Có  ',
                    'Không',
                  ],
                  id: "1009"),
              callback: (index) {
                if (index == 0) {
                  _naydeu.value = true;
                } else {
                  _naydeu.value = false;
                }
              },
            ),


            Container(
              margin: EdgeInsets.only(left: 40),
              child: ValueListenableBuilder<bool>(
                valueListenable: _naydeu,
                builder: (_, value, child) {
                  return value
                      ? Container(): QuestionTextWidget(questionItem: QuestionItem(title: 'Chi giảm hoặc mất mạch  '));
                },
              ),
            ),
          ],
        ),
        Container(
          margin: EdgeInsets.only(left: 50),
          child: ValueListenableBuilder<bool>(
              valueListenable: _tuchi,
              builder: (_, value, child) {
                return value
                    ? QuestionTextWidget(questionItem: QuestionItem(title: 'Chi giảm hoặc mất mạch'))
                    : Container();
              }),
        ),

        Container(
          margin: EdgeInsets.only(left: 40),
          child: ValueListenableBuilder<bool>(
            valueListenable: _cobieuhienxvdm,
            builder: (_, value, child) {
              return value
                  ? QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'Cứng  ',
                            'ngoằn ngoèo ',
                            'Đáy mắt ',
                          ],
                          id: "1009"),
                      callback: (index, select, indexes) {},
                    )
                  : Container();
            },
          ),
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: 'ĐM Cảnh ',
                  options: [
                    'Nảy đều 2 bên  ',
                    'Âm thổi ',
                  ],
                  id: "1009"),
              callback: (index, select, indexes) {
                if (indexes.contains(1)) {
                  _amthoi.value = true;
                } else {
                  _amthoi.value = false;
                }
              },
            ),

            Container(
              margin: EdgeInsets.only(left: 40),
              child: ValueListenableBuilder(
                  valueListenable: _amthoi,
                  builder: (_, value, child) {
                    return value
                        ? QuestionMultipleWidget(questionItem: QuestionItem(title: 'bên ', options: ['1:trái', '2:phải']),)
                        : Container();
                  }),
            ),
            QuestionMultipleWidget(
              questionItem: QuestionItem(
                  type: QuestionType.MULTIPLE_CHOICE,
                  title: '',
                  options: [
                    'Âm thổi vùng mắt ',
                  ],
                  id: "1009"),
              callback: (index, select, indexes) {
                if (indexes.contains(0)) {
                  _amthoivungmat.value = true;
                } else {
                  _amthoivungmat.value = false;
                }
              },
            ),
            Container(
              margin: EdgeInsets.only(left: 40),
              child: ValueListenableBuilder(
                  valueListenable: _amthoivungmat,
                  builder: (_, value, child) {
                    return value
                        ? QuestionMultipleWidget(questionItem: QuestionItem(title: 'bên ', options: ['1:trái', '2:phải']),)
                        : Container();
                  }),
            ),
          ],
        ),

      ],
    );
  }

  _monitorChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _monitor,
      builder: (_, value, child) {
        return _monitor.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: QuestionSingleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.SINGLE_CHOICE,
                      title: '',
                      options: [
                        '1h',
                        '2h',
                        '4h',
                        '6h',
                        '8h',
                        '12h',
                        '24h',
                      ],
                      id: "1006"),
                  callback: (index) {
                    print("SHOW SELECTED INDEX $index");
                  },
                ),
              )
            : Container();
      },
    );
  }

  _nkqChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _nkq,
      builder: (_, value, child) {
        return _nkq.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionDateWidget(
                      questionItem: QuestionItem(title: 'Thời điểm', type: QuestionType.DATE),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'TANLS',
                            'Tổn thương não',
                            'Bảo vệ đường thở',
                            'Giảm chức năng tim ',
                            'Giảm chức năng phổi',
                            'Yếu cơ hô hấp',
                            'Sử dụng thuốc(an thần, giảm đau, chống thần kinh...)',
                            'Thở máy ',
                          ],
                          id: "1006"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  _httkChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _httk,
      builder: (_, value, child) {
        return _httk.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    QuestionDateWidget(
                      questionItem: QuestionItem(title: 'Thời gian', type: QuestionType.DATE),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'TANLS',
                            'Tổn thương não',
                            'Giảm chức năng tim',
                            'Yếu cơ hô hấp ',
                            'TKMP',
                            'TDMP',
                            'Co thắt phế quản',
                            'Tổn thương phế nang',
                            'Thuyên tắc phổi',
                            'Sử dụng thuốc (an thần, giảm đau, thần kinh...',
                          ],
                          id: "1006"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  _timChilds() {
    return ValueListenableBuilder<bool>(
      valueListenable: _tim,
      builder: (_, value, child) {
        return _tim.value
            ? Container(
                margin: EdgeInsets.only(left: 20),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        QuestionTextWidget(
                          questionItem: QuestionItem(title: 'Tên loạn nhịp', type: QuestionType.TEXT),
                        ),
                        QuestionMultipleWidget(
                          questionItem: QuestionItem(
                              type: QuestionType.MULTIPLE_CHOICE,
                              title: '',
                              options: ['Cần HC tim mạch', 'Ảnh hưởng đến huyết động'],
                              id: "1006"),
                          callback: (index, select, indexes) {
                            print("SHOW SELECTED INDEX $index");
                          },
                        ),
                      ],
                    ),
                    QuestionYNShowMultipleWidget(title: 'Cần Monitor', subTitle: 'Dự trù', extraOptions: [ '1d',
                      '2d',
                      '3d',
                      '>3d',],),
//                    QuestionYNSingleWidget(title: 'Cần monitor? ', subTitle: 'Dự trù', extraOptions: [
//                      '1d',
//                      '2d',
//                      '3d',
//                      '>3d',
//                    ],),

                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: 'Ảnh hưởng bởi',
                          options: [
                            'Bệnh nền',
                            'RL nước-dg',
                            'Thân nhiệt',
                            'KR',
                          ],
                          id: "1009"),
                      callback: (index, select, indexes) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                  ],
                ),
              )
            : Container();
      },
    );
  }

  _bodyInfo() {
    var list = ['Cân nặng (kg)', 'Chiều cao(cm)', 'BMI'];
    return Container(
      child: Wrap(
        children: List.generate(list.length, (index) {
          return QuestionTextWidget(questionItem: QuestionItem(title: list[index]));
        }),
      ),
    );
  }
}
