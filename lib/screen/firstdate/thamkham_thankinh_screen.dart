import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/screen/general/gcs_screen.dart';
import 'package:flutter_app/screen/general/nihss_screen.dart';
import 'package:flutter_app/widget/question_button_widget.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/save_button.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class ThamKhamThanKinhScreen extends StatefulWidget {
  VoidCallback callback;

  ThamKhamThanKinhScreen(this.callback);

  @override
  State createState() {
    return _ThamKhamThanKinhScreenState();
  }
}

class _ThamKhamThanKinhScreenState extends State<ThamKhamThanKinhScreen> {
  ValueNotifier<bool> _monitor = ValueNotifier<bool>(false);
  ValueNotifier<bool> _kieutho = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dongtu = ValueNotifier<bool>(false);
  ValueNotifier<bool> _nystagmus = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tayOption = ValueNotifier<bool>(false);
  ValueNotifier<bool> _chanOption = ValueNotifier<bool>(false);
  ValueNotifier<bool> _yeuLietChi = ValueNotifier<bool>(false);

  ValueNotifier<bool> _chanLCOption = ValueNotifier<bool>(false);
  ValueNotifier<bool> _rlCamGiac = ValueNotifier<bool>(false);
  ValueNotifier<bool> _rlPXGC = ValueNotifier<bool>(false);
  ValueNotifier<bool> _rlTraiPXGC = ValueNotifier<bool>(false);
  ValueNotifier<bool> _rlPhaiPXGC = ValueNotifier<bool>(false);
  ValueNotifier<bool> _vitritonthuong = ValueNotifier<bool>(false);
  ValueNotifier<bool> _bancauDaiNao = ValueNotifier<bool>(false);
  ValueNotifier<bool> _banCauTieuNao = ValueNotifier<bool>(false);
  ValueNotifier<bool> _aphasia = ValueNotifier<bool>(false);
  ValueNotifier<bool> _bienchung = ValueNotifier<bool>(false);
  ValueNotifier<bool> _lietdaythankinhso = ValueNotifier<bool>(false);

  double widthLoop = 90;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm ')),
              Container(
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Wrap(children: [
                      QuestionButtonWidget(
                        questionItem: QuestionItem(title: 'GCS'),
                        callback: () {
                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("GCS"),
                                      FlatButton(
                                        color: primaryColor,
                                        child: Text('Lưu', style: TextStyle(color: Colors.white),),
                                        onPressed: (){
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                  content: Container(
                                    width: 300,
                                    height: 150,
                                      child: GCSScreen()),
                                );
                              });
                        },
                      ),
                      QuestionButtonWidget(
                        questionItem: QuestionItem(title: 'NIHSS'),
                        callback: () {
                          print('NIHSS');

                          showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                  title: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text("NIHSS"),
                                      FlatButton(
                                        color: primaryColor,
                                        child: Text('Lưu', style: TextStyle(color: Colors.white),),
                                        onPressed: (){
                                          Navigator.pop(context);
                                        },
                                      )
                                    ],
                                  ),
                                  content: Container(

                                    width: MediaQuery.of(context).size.width*2/3,
                                      child: NIHSSScreen()),
                                );
                              });
                        },
                      )
                    ]),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Container(
                  margin: EdgeInsets.only(left: 20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      QuestionSingleWidget(
                        questionItem: QuestionItem(
                            type: QuestionType.SINGLE_CHOICE,
                            title: 'Thuận tay',
                            options: ['Trái ', 'Phải ', 'KR'],
                            id: "1005"),
                      ),
                      QuestionMultipleWidget(
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: '',
                            options: [
                              'Hợp tác',
                              'Kích động',
                              'Thờ ơ ',
                              'Không tiếp xúc',
                              'Yên lặng ',
                              'Mất trí nhớ ',
                              'Thờ ơ 1/2 người ',
                              'Gồng mất võ ',
                              'Gồng mất não ',
                              'Gồng 1 bên ',
                              'Xoay mắt đầu về bên liệt ',
                              'Xoay mắt đầu đối bên liệt ',
                              'Dysarthria',
                              'Aphasia'
                            ],
                            id: "1006"),
                        callback: (index, select, indexes) {
                          print("SHOW SELECTED INDEX $index");
                          if (indexes.contains(13)) {
                            _aphasia.value = true;
                          } else {
                            _aphasia.value = false;
                          }
                        },
                      ),
                      ValueListenableBuilder<bool>(
                          valueListenable: _aphasia,
                          builder: (_, value, child) {
                            return value
                                ? Container(
                                    margin: EdgeInsets.only(left: 50, bottom: 8, right: 8),
                                    decoration: containerDecoration(borderColor: Colors.blue),
                                    child: QuestionMultipleWidget(
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'Broca',
                                            'Wernick',
                                            'Global',
                                          ],
                                          id: "1006"),
                                    ),
                                  )
                                : Container();
                          }),
                      Container(
                        decoration: containerDecoration(),
                        child: Row(
                          children: [
                            QuestionYNShowMultipleWidget(
                              title: 'Hẹp khe mi?',
                              extraOptions: [
                                'Trái ',
                                'Phải ',
                              ],
                            ),
                            QuestionYNShowMultipleWidget(
                              title: 'Giảm tiết mồ hôi?',
                              extraOptions: [
                                'Trái ',
                                'Phải ',
                              ],
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SectionWidget(
                      title: AppString.titleDontTu,
                    ),
                    QuestionSingleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.SINGLE_CHOICE,
                          title: '',
                          options: ['Đều 2 bên ', 'Không đều 2 bên '],
                          id: "1009"),
                      callback: (index) {
                        print("SHOW SELECTED INDEX $index");
                        _dongtu.value = true;
                      },
                    ),
                    _dongtuChilds(),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SectionWidget(
                      title: AppString.titleVanNhan,
                    ),
                    QuestionSingleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.SINGLE_CHOICE,
                          title: '',
                          options: ['Theo y lệnh ', 'Tự động'],
                          id: "1005"),
                      callback: (index) {
                        print("SHOW SELECTED INDEX $index");
                      },
                    ),
                    Wrap(
                      children: [
                        QuestionMultipleWidget(
                          questionItem: QuestionItem(
                              type: QuestionType.MULTIPLE_CHOICE,
                              title: 'Khuynh hướng nhìn ?',
                              options: [
                                'Lên',
                                'xuống  ',
                                '1 bên  ',
                              ],
                              id: "1006"),
                        ),
                      ],
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: 'Đồng trục',
                          options: [
                            'Ngang   ',
                            'Dọc   ',
                          ],
                          id: "1006"),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: 'Phạn xạ búp bê ',
                          options: [
                            'Ngang   ',
                            'Dọc   ',
                          ],
                          id: "1006"),
                    ),
                    QuestionMultipleWidget(
                      questionItem: QuestionItem(
                          type: QuestionType.MULTIPLE_CHOICE,
                          title: '',
                          options: [
                            'Nystagmus   ',
                          ],
                          id: "1006"),
                      callback: (idx, select, selectedIndex) {
                        if (selectedIndex.contains(0)) {
                          _nystagmus.value = true;

                          ///
                        } else {
                          _nystagmus.value = false;
                        }
                      },
                    ),
                    ValueListenableBuilder<bool>(
                      valueListenable: _nystagmus,
                      builder: (_, value, child) {
                        return value
                            ? Container(
                                decoration: containerDecoration(borderColor: Colors.blue),
                                margin: EdgeInsets.only(left: 40, bottom: 8, right: 8),
                                child: QuestionMultipleWidget(
                                    width: widthLoop,
                                    questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: ['Ngang   ', 'Dọc ', 'Xoay ', 'Trái ', 'Phải '],
                                    )),
                              )
                            : Container();
                      },
                    ),
                    QuestionMultipleWidget(
                        width: widthLoop,
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: '',
                            options: ['Liệt CN nhìn ngang', 'Liệt CN nhìn dọc', 'HC 1 và 1/2'],
                            id: "1006")),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 16),
                width: MediaQuery.of(context).size.width,
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [

                    QuestionYNShowMultipleWidget(
                      title: AppString.titleDauMangNao,
                      extraOptions: [
                        'Cổ gượng ',
                        'Kerning ',
                      ],
                    ),
                    QuestionSingle(
                        title: 'Liệt dây thần kinh sọ',
                        mainOptions: ['Có', 'Không'],
                        callback: (index) {
                          if (index == 0) {
                            _lietdaythankinhso.value = true;
                          } else {
                            _lietdaythankinhso.value = false;
                          }
                        }),
                    Container(
                      margin: EdgeInsets.only(left: 40),
                      child: ValueListenableBuilder(
                        valueListenable: _lietdaythankinhso,
                        builder: (_, value, child) {
                          return value
                              ? Container(
                                  child: Column(
                                    children: [
                                      QuestionMultipleWidget(
//                    width: 100,
                                          hasDecor: true,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'I',
                                                'Bên T',
                                                'Bên P ',
                                                'Bán manh đồng danh ',
                                                'Bán manh thái dương ',
                                                'Mù 1 mắt',
                                                'Mù võ não '
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'II',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'III ',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'IV',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'V',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'VI',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'VII ',
                                                'Bên T',
                                                'Bên P ',
                                                'KR ',
                                                'Ngoại biên ',
                                                'TW ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'VIII  ',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'IX   ',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'X  ',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'XI  ',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006")),
                                      QuestionMultipleWidget(
                                          hasDecor: true,
                                          width: widthLoop,
                                          questionItem: QuestionItem(
                                              type: QuestionType.MULTIPLE_CHOICE,
                                              title: '',
                                              options: [
                                                'XII  ',
                                                'Bên T',
                                                'Bên P ',
                                              ],
                                              id: "1006"))
                                    ],
                                  ),
                                )
                              : Container();
                        },
                      ),
                    )
                  ],
                ),
              ),
              _yeuLietChiUI(),
              _rlCamGiacUI(),
              _pxgcUI(),
              _pxThapUI(),
              _thatDieuUI(),
              _vitritonthuongUI(),
              _bienChungUI(),
              SaveWidget(
                callback: () {},
              )
            ],
          ),
        ),
      ),
    );
  }

  _yeuLietChiUI() {
    bool tayTrai = false;
    bool tayPhai = false;
    bool chanTrai = false;
    bool chanPhai = false;
    ValueNotifier<bool> yeulietchi = ValueNotifier<bool>(false);

    ValueNotifier<bool> tayTraiOption = ValueNotifier<bool>(false);
    ValueNotifier<bool> tayPhaiOption = ValueNotifier<bool>(false);

    ValueNotifier<bool> chanTraiOption = ValueNotifier<bool>(false);
    ValueNotifier<bool> chanPhaiOption = ValueNotifier<bool>(false);

    ValueNotifier<bool> tuchiOption = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          QuestionSingleWidget(
            questionItem:
                QuestionItem(type: QuestionType.SINGLE_CHOICE, title: AppString.titleYeuLietChi, options: ['Có', 'Không'], id: "1009"),
            callback: (index) {
              if (index == 0) {
                yeulietchi.value = true;
              } else {
                yeulietchi.value = false;
              }
              print("SHOW SELECTED INDEX $index");
            },
          ),
          ValueListenableBuilder(
              valueListenable: yeulietchi,
              builder: (_, value, child) {
                return value
                    ? Container(
                        margin: EdgeInsets.only(left: 40),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: 'Tay',
                                    options: [
                                      'Trái   ',
                                      'Phải ',
                                    ],
                                    id: "1006"),
                                callback: (idx, select, indexes) {
                                  print("Nemo $idx ${indexes}");

                                  if (indexes.contains(0)) {
                                    tayTrai = true;
                                  } else {
                                    tayTrai = false;
                                  }

                                  if (indexes.contains(1)) {
                                    tayPhai = true;
                                  } else {
                                    tayPhai = false;
                                  }

                                  tayTraiOption.value = tayTrai;
                                  tayPhaiOption.value = tayPhai;
                                }),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 350,
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: tayTraiOption,
                                      builder: (_, value, child) {
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Gốc /5 ')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Ngọn /5')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 350,
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: tayPhaiOption,
                                      builder: (_, value, child) {
                                        print("TAY PHAI $value");
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Gốc /5 ')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Ngọn /5')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: 'Chân ',
                                    options: [
                                      'Trái   ',
                                      'Phải ',
                                    ],
                                    id: "1006"),
                                callback: (idx, select, indexes) {
                                  if (indexes.contains(0)) {
                                    chanTrai = true;
                                  } else {
                                    chanTrai = false;
                                  }

                                  if (indexes.contains(1)) {
                                    chanPhai = true;
                                  } else {
                                    chanPhai = false;
                                  }

                                  chanTraiOption.value = chanTrai;
                                  chanPhaiOption.value = chanPhai;
                                }),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 350,
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: chanTraiOption,
                                      builder: (_, value, child) {
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Gốc /5 ')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Ngọn /5')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 350,
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: chanPhaiOption,
                                      builder: (_, value, child) {
                                        print("TAY PHAI $value");
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Gốc /5 ')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Ngọn /5')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            QuestionMultipleWidget(
                              width: widthLoop,
                              questionItem: QuestionItem(
                                  type: QuestionType.MULTIPLE_CHOICE,
                                  title: ' ',
                                  options: [
                                    'Tứ chi   ',
                                  ],
                                  id: "1006"),
                              callback: (idx, select, indexes) {
                                if (indexes.contains(idx)) {
                                  tuchiOption.value = true;
                                  tayTraiOption.value = true;
                                  tayPhaiOption.value = true;
                                  chanTraiOption.value = true;
                                  chanTraiOption.value = true;
                                } else {
                                  tuchiOption.value = false;
                                }
                              },
                            ),
                          ],
                        ),
                      )
                    : Container();
              }),
        ],
      ),
    );
  }

  _rlCamGiacUI() {
    bool tayTrai = false;
    bool tayPhai = false;
    bool chanTrai = false;
    bool chanPhai = false;
    ValueNotifier<bool> tayTraiOption = ValueNotifier<bool>(false);
    ValueNotifier<bool> tayPhaiOption = ValueNotifier<bool>(false);

    ValueNotifier<bool> chanTraiOption = ValueNotifier<bool>(false);
    ValueNotifier<bool> chanPhaiOption = ValueNotifier<bool>(false);

    ValueNotifier<bool> tuchiOption = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          QuestionSingleWidget(
            questionItem:
                QuestionItem(type: QuestionType.SINGLE_CHOICE, title: AppString.titleTeRLCamGiac, options: ['Có', 'Không'], id: "1009"),
            callback: (index) {
              if (index == 0) {
                _rlCamGiac.value = true;
              } else {
                _rlCamGiac.value = false;
              }
              print("SHOW SELECTED INDEX $index");
            },
          ),
          ValueListenableBuilder(
              valueListenable: _rlCamGiac,
              builder: (_, value, child) {
                return value
                    ? Container(
                        margin: EdgeInsets.only(left: 40),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: 'Tay',
                                    options: [
                                      'Trái   ',
                                      'Phải ',
                                    ],
                                    id: "1006"),
                                callback: (idx, select, indexes) {
                                  print("Nemo $idx ${indexes}");

                                  if (indexes.contains(0)) {
                                    tayTrai = true;
                                  } else {
                                    tayTrai = false;
                                  }

                                  if (indexes.contains(1)) {
                                    tayPhai = true;
                                  } else {
                                    tayPhai = false;
                                  }

                                  tayTraiOption.value = tayTrai;
                                  tayPhaiOption.value = tayPhai;
                                }),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 350,
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: tayTraiOption,
                                      builder: (_, value, child) {
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nông')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Sâu')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 350,
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: tayPhaiOption,
                                      builder: (_, value, child) {
                                        print("TAY PHAI $value");
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nông')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Sâu')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: 'Chân ',
                                    options: [
                                      'Trái   ',
                                      'Phải ',
                                    ],
                                    id: "1006"),
                                callback: (idx, select, indexes) {
                                  if (indexes.contains(0)) {
                                    chanTrai = true;
                                  } else {
                                    chanTrai = false;
                                  }

                                  if (indexes.contains(1)) {
                                    chanPhai = true;
                                  } else {
                                    chanPhai = false;
                                  }

                                  chanTraiOption.value = chanTrai;
                                  chanPhaiOption.value = chanPhai;
                                }),
                            Container(
                              width: MediaQuery.of(context).size.width,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Container(
                                    width: 350,
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: chanTraiOption,
                                      builder: (_, value, child) {
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nông')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Sâu')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                  Container(
                                    width: 350,
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: chanPhaiOption,
                                      builder: (_, value, child) {
                                        print("TAY PHAI $value");
                                        return Visibility(
                                          visible: value,
                                          child: Row(
                                            children: [
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Nông')),
                                              QuestionTextWidget(questionItem: QuestionItem(title: 'Sâu')),
                                            ],
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            QuestionMultipleWidget(
                              width: widthLoop,
                              questionItem: QuestionItem(
                                  type: QuestionType.MULTIPLE_CHOICE,
                                  title: ' ',
                                  options: [
                                    'Tứ chi   ',
                                  ],
                                  id: "1006"),
                              callback: (idx, select, indexes) {
                                if (indexes.contains(idx)) {
                                  tuchiOption.value = true;
                                  tayTraiOption.value = true;
                                  tayPhaiOption.value = true;
                                  chanTraiOption.value = true;
                                  chanTraiOption.value = true;
                                } else {
                                  tuchiOption.value = false;
                                }
                              },
                            ),
                            Container(
//                              decoration: containerDecoration(),
                              child: QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: 'Mặt ',
                                    options: [
                                      'Trái   ',
                                      'Phải ',
                                    ],
                                    id: "1006"),
                              ),
                            ),
                          ],
                        ),
                      )
                    : Container();
              }),
        ],
      ),
    );
  }

  _thatDieuUI() {
    ValueNotifier<bool> thatdieu = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          QuestionSingleWidget(
            questionItem:
                QuestionItem(type: QuestionType.SINGLE_CHOICE, title: AppString.titleThatDieu, options: ['Có', 'Không'], id: "1009"),
            callback: (index) {
              if (index == 0) {
                thatdieu.value = true;
              } else {
                thatdieu.value = false;
              }
              print("SHOW SELECTED INDEX $index");
            },
          ),
          ValueListenableBuilder<bool>(
            valueListenable: thatdieu,
            builder: (_, value, child) {
              return value
                  ? Container(
                      margin: EdgeInsets.only(left: 40),
                      child: Column(
                        children: [
                          Row(
                            children: [
                              QuestionMultipleWidget(
                                  width: widthLoop,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: 'Tay',
                                      options: [
                                        'Trái   ',
                                        'Phải ',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {}),
                              QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: 'Chân',
                                    options: [
                                      'Trái   ',
                                      'Phải ',
                                    ],
                                    id: "1006"),
                              ),
                              QuestionMultipleWidget(
                                width: widthLoop,
                                questionItem: QuestionItem(
                                    type: QuestionType.MULTIPLE_CHOICE,
                                    title: '',
                                    options: [
                                      'Trục thân ',
                                    ],
                                    id: "1006"),
                              ),
                            ],
                          ),
                          Container(
                            child: Row(
                              children: [
                                QuestionMultipleWidget(
                                  width: widthLoop,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'Mất thăng bằng ',
                                      ],
                                      id: "1006"),
                                ),
                                QuestionMultipleWidget(
                                  width: widthLoop,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: 'Khuynh hướng ngã sang ',
                                      options: [
                                        'Trái ',
                                        'Phải',
                                        'Mọi hướng ',
                                      ],
                                      id: "1006"),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    )
                  : Container();
            },
          )
        ],
      ),
    );
  }

  _pxgcUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          QuestionMultipleWidget(
            questionItem: QuestionItem(type: QuestionType.SINGLE_CHOICE, title: AppString.titlePXGC, options: ['Trái'], id: "1009"),
            callback: (index, select, indexes) {
              if (indexes.contains(index)) {
                _rlTraiPXGC.value = true;
              } else {
                _rlTraiPXGC.value = false;
              }
              print("SHOW SELECTED INDEX $index");
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder(
                valueListenable: _rlTraiPXGC,
                builder: (_, value, child) {
                  return value
                      ? Wrap(
                          children: List.generate(DataFirstDate().pxgcQuestionSet().length, (index) {
                            var item = DataFirstDate().pxgcQuestionSet()[index];
                            return QuestionTextWidget(questionItem: item);
                          }),
                        )
                      : Container();
                }),
          ),
          QuestionMultipleWidget(
            questionItem: QuestionItem(type: QuestionType.SINGLE_CHOICE, title: '', options: ['Phải'], id: "1009"),
            callback: (index, select, indexes) {
              if (indexes.contains(index)) {
                _rlPhaiPXGC.value = true;
              } else {
                _rlPhaiPXGC.value = false;
              }
              print("SHOW SELECTED INDEX $index");
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder(
                valueListenable: _rlPhaiPXGC,
                builder: (_, value, child) {
                  return value
                      ? Wrap(
                          children: List.generate(DataFirstDate().pxgcQuestionSet().length, (index) {
                            var item = DataFirstDate().pxgcQuestionSet()[index];
                            return QuestionTextWidget(questionItem: item);
                          }),
                        )
                      : Container();
                }),
          ),
        ],
      ),
    );
  }

  _pxThapUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titlePXThap,
          ),
          Wrap(
            children: [
              QuestionMultipleWidget(
                width: 60,
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: 'Hoffman',
                    options: [
                      'Trái  ',
                      'Phải',
                    ],
                    id: "1006"),
              ),
              QuestionMultipleWidget(
                width: 60,
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: 'Babinski',
                    options: [
                      'Trái  ',
                      'Phải',
                    ],
                    id: "1006"),
              ),
              QuestionMultipleWidget(
                width: 60,
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: 'Lê Văn Thành',
                    options: [
                      'Trái  ',
                      'Phải',
                    ],
                    id: "1006"),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _vitritonthuongUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleVitritonthuong,
          ),

          Container(
            margin: EdgeInsets.only(left: 40, right: 8),
            child: Column(
              children: [
                QuestionYNShowMultipleWidget(
                  title: 'Bán cầu đại não ',
                  extraOptions: ['Trái ', 'Phải ', 'Vỏ', 'Dưới vỏ'],
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Bán cầu tiểu não ',
                  extraOptions: ['Trái ', 'Phải ', 'Thuỳ giun'],
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Thân não',
                  extraOptions: ['Trái', 'Phải', 'Hai bên', 'Cuống não', 'Cầu não', 'Hành não'],
                ),
                QuestionMultipleWidget(
                    questionItem: QuestionItem(
                        title: '',
                        options: ['Não thất ', 'Màng não', 'Hai bán cầu ', 'Toàn bộ não ', 'Không xác định'])),
              ],
            ),
          )
        ],
      ),
    );
  }

  _bienChungUI() {
    return Container(
      margin: EdgeInsets.only(top: 16),
      width: MediaQuery.of(context).size.width,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: AppString.titleBienChung,
          ),
          QuestionMultipleWidget(
//            width: 60,
            questionItem:
                QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: '', options: ['TALNS', 'Tụt não '], id: "1006"),
            callback: (idx, select, indexes) {
//              var checkTutNao = _bienchung.value;
              if (indexes.contains(1)) {
                _bienchung.value = true;
              } else {
                _bienchung.value = false;
              }
            },
          ),
          ValueListenableBuilder<bool>(
            valueListenable: _bienchung,
            builder: (_, value, child) {
              return _bienchung.value
                  ? Container(
                      margin: EdgeInsets.only(left: 40, right: 8),
                      child: Column(
                        children: [
                          QuestionMultipleWidget(
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: 'Dạng tụt não ',
                                options: ['Thái dương  ', 'Trung tâm ', 'Lều tiểu não lên trên ', 'Lỗ chẩm '],
                                id: "1006"),
                            callback: (index, select, indexes) {},
                          ),
                        ],
                      ),
                    )
                  : Container();
            },
          ),
        ],
      ),
    );
  }

  _dongtuChilds() {
    ValueNotifier<bool> trai = ValueNotifier<bool>(false);
    ValueNotifier<bool> phai = ValueNotifier<bool>(false);
    return ValueListenableBuilder<bool>(
      valueListenable: _dongtu,
      builder: (_, value, child) {
        return _dongtu.value
            ? Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    decoration: containerDecoration(),
                    margin: EdgeInsets.only(left: 20),
                    child: Wrap(
                      children: [
                        QuestionTextWidget(
                          questionItem: QuestionItem(title: "Trái (mm)"),
                          callback: (value) {},
                        ),
                        QuestionMultipleWidget(
                          questionItem: QuestionItem(
                              type: QuestionType.MULTIPLE_CHOICE,
                              title: '',
                              options: [
                                'Tròn ',
                                'Méo',
                                'PXAS',
                              ],
                              id: "1006"),
                          callback: (idx, select, indexes) {
                            if (indexes.contains(2)) {
                              trai.value = true;
                            } else {
                              trai.value = false;
                            }
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 200),
                          child: ValueListenableBuilder<bool>(
                            valueListenable: trai,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionMultipleWidget(
                                      questionItem: QuestionItem(title: '', options: ['Có', 'Có, chậm', 'Không']))
                                  : Container();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    decoration: containerDecoration(),
                    child: Wrap(
                      children: [
                        QuestionTextWidget(
                          questionItem: QuestionItem(title: "Phải  (mm)"),
                          callback: (value) {},
                        ),
                        QuestionMultipleWidget(
                          questionItem: QuestionItem(
                              type: QuestionType.MULTIPLE_CHOICE,
                              title: '',
                              options: [
                                'Tròn ',
                                'Méo',
                                'PXAS',
                              ],
                              id: "1006"),
                          callback: (idx, select, indexes) {
                            if (indexes.contains(2)) {
                              phai.value = true;
                            } else {
                              phai.value = false;
                            }
                          },
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 200),
                          child: ValueListenableBuilder<bool>(
                            valueListenable: phai,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionMultipleWidget(
                                      questionItem: QuestionItem(title: '', options: ['Có', 'Có, chậm', 'Không']))
                                  : Container();
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                  Container(
                    margin: EdgeInsets.only(left: 20),
                    child: Wrap(
                      children: [
                        QuestionMultipleWidget(
                          questionItem: QuestionItem(
                              type: QuestionType.MULTIPLE_CHOICE,
                              title: 'Phản xạ đồng cảm',
                              options: [
                                'Trái  ',
                                'Phải ',
                              ],
                              id: "1006"),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            : Container();
      },
    );
  }
}
