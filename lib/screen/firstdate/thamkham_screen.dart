import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/screen/firstdate/thamkham_thankinh_screen.dart';
import 'package:flutter_app/screen/firstdate/thamkham_tongquat_screen.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class ThamKhamScreen extends StatefulWidget {

  ThamKhamScreen();

  @override
  State createState() {
    return _ThamKhamScreenState();
  }
}

class _ThamKhamScreenState extends State<ThamKhamScreen> with SingleTickerProviderStateMixin {
  ValueNotifier<int> _tab = ValueNotifier<int>(0);

  ValueNotifier<bool> _monitor = ValueNotifier<bool>(false);
  ValueNotifier<bool> _kieutho = ValueNotifier<bool>(false);
  ValueNotifier<bool> _nkq = ValueNotifier<bool>(false);
  ValueNotifier<bool> _httk = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tim = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tuchi = ValueNotifier<bool>(false);

  TabController _tabController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _tabController = new TabController(length: 2, vsync: this);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: PreferredSize(
          preferredSize: Size(MediaQuery.of(context).size.width, 60),
          child: Column(
            children: [
              TabBar(
                indicatorColor: Colors.white,
                controller: _tabController,
                tabs: [
                  Container(),
                  Container(),
                ],
              ),
              ValueListenableBuilder<int>(
                valueListenable: _tab,
                builder: (_, value, child) {
                  print("RUN AGAIN $value");
                  return Container(
                    margin: EdgeInsets.all(10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FlatButton(
                        child: Text("Thăm khám thần kinh", style: TextStyle(color: value == 0? Colors.white: Colors.black87),),
                        onPressed: () {
                          _tab.value = 0;
                          _tabController.index = 0;
                        },
                        color: value == 0? Colors.blue: Colors.grey[200],
                        height: 40,

                      ),

                      FlatButton(
                        child: Text("Thăm khám tổng quát", style: TextStyle(color: value == 1? Colors.white: Colors.black87),),
                        onPressed: () {
                          _tab.value = 1;
                          _tabController.index = 1;
                        },
                        color: value == 1? Colors.blue: Colors.grey[200],
                        height: 40,
                      )
                    ],
                  ));
                },
              )
            ],
          ),
        ),
        body: TabBarView(
            controller: _tabController,
            children: <Widget>[ThamKhamThanKinhScreen(() {}), ThamKhamTongQuatScreen(() {})]));
  }
}
