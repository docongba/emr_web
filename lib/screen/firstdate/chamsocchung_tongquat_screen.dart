import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_check_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class ChamSocChungTongQuat extends StatefulWidget {

  ChamSocChungTongQuat();

  @override
  State createState() {
    return _ChamSocChungTongQuatState();
  }
}

class _ChamSocChungTongQuatState extends State<ChamSocChungTongQuat> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm (Hiện tại - Lúc nhận bệnh)')),
                      QuestionDateWidget(questionItem: QuestionItem(title: 'Đến (+24h)')),
                    ],
                  ),
                  Container(
                    height: 50,
                    alignment: Alignment.center,
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    color: Colors.blue,
                    child: Text('TRENDS (Hiển thị các lần chăm sóc trước)', style: TextStyle(color: Colors.white)),
                  )
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                color: Colors.grey,
                child: SectionWidget(
                  title: "Bắt đầu thăm khám mới",
                ),
              ),
              Row(
                children: [
                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm (Hiện tại)')),
                  Container(
                    color: Colors.blue,
                    child: IconButton(
                      icon: Icon(Icons.add),
                      onPressed: () {},
                    ),
                  )
                ],
              ),
              _oxy(),
              _tuthe(),
              _monitor(),
              _dichtruyen(),
              _hasot(),
              _thuockhac(),
              _ppchamsockhac(),
              _hoichan(),
            ],
          ),
        ),
      ),
    );
  }

  _monitor() {
    ValueNotifier<int> hypoxia = ValueNotifier<int>(-1);

    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Mornitor',
          ),
          _headerYLenh(),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                flex: 2,
                child: QuestionSingleWidget(
                  horizontal: false,
                  questionItem: QuestionItem(title: '', options: [
                    'Cần Monitor',
                    'Không Monitor',
                  ]),
                  callback: (index) {
                    if (index == 0) {
                      hypoxia.value = 0;
                    } else {
                      hypoxia.value = 1;
                    }
                  },
                ),
              ),
              Expanded(
                flex: 5,
                child: ValueListenableBuilder<int>(
                    valueListenable: hypoxia,
                    builder: (_, value, child) {
                      if (value == 0) {
                        return Column(
                          children: [
                            QuestionTextWidget(questionItem: QuestionItem(title: 'Theo dõi định kì mỗi (phút)')),
                            Text('Báo BS khi'),
                            Wrap(
                              children: [
                                QuestionTextWidget(questionItem: QuestionItem(title: ' < thân nhiệt')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'thân nhiệt < ')),
                                QuestionTextWidget(questionItem: QuestionItem(title: ' < HA tthu (mmHg)')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'HA tthu (mmHg) < ')),
                                QuestionTextWidget(questionItem: QuestionItem(title: ' < HA ttrg (mmHg)')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'HA ttrg (mmHg) < ')),
                                QuestionTextWidget(questionItem: QuestionItem(title: '< HA TB (mmHg)')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'HA TB (mmHg) <')),
                              ],
                            ),
                          ],
                        );
                      } else if (value == 1) {
                        return Column(
                          children: [
                            QuestionTextWidget(questionItem: QuestionItem(title: 'Theo dõi định kì mỗi (phút)')),
                            Text('Báo BS khi'),
                            Wrap(
                              children: [
                                QuestionTextWidget(questionItem: QuestionItem(title: ' < thân nhiệt')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'thân nhiệt < ')),
                                QuestionTextWidget(questionItem: QuestionItem(title: ' < HA tthu (mmHg)')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'HA tthu (mmHg) < ')),
                                QuestionTextWidget(questionItem: QuestionItem(title: ' < HA ttrg (mmHg)')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'HA ttrg (mmHg) < ')),
                                QuestionTextWidget(questionItem: QuestionItem(title: '< HA TB (mmHg)')),
                                QuestionTextWidget(questionItem: QuestionItem(title: 'HA TB (mmHg) <')),
                              ],
                            ),
                          ],
                        );
                      }

                      return Container();
                    }),
              ),
            ],
          ),
        ],
      ),
    );
  }

  _hasot() {
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Hạ sốt',
          ),
          Column(
            children: [
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Tên thuốc')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Liều lượng')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              _addButotn()
            ],
          )
        ],
      ),
    );
  }

  _thuockhac() {
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Thuốc khác',
          ),
          Column(
            children: [
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Tên thuốc')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Liều lượng')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Tên thuốc')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Liều lượng')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Tên thuốc')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Liều lượng')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              _addButotn()
            ],
          )
        ],
      ),
    );
  }

  _ppchamsockhac() {
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Các phương pháp chăm sóc khác',
          ),
          Column(
            children: [
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: '')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: '')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: '')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                ],
              ),
              _addButotn()
            ],
          )
        ],
      ),
    );
  }

  _hoichan() {
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Hội chẩn ',
          ),
          Column(
            children: [
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: '')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Kết quả')),
                ],
              ),
              Row(
                children: [
                  QuestionTextWidget(questionItem: QuestionItem(title: '')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  QuestionTextWidget(questionItem: QuestionItem(title: 'Kết quả')),
                ],
              ),
              _addButotn()
            ],
          )
        ],
      ),
    );
  }

  _dichtruyen() {
    ValueNotifier<bool> hypoxia = ValueNotifier<bool>(false);
    ValueNotifier<bool> tacnghen = ValueNotifier<bool>(false);

    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Dịch truyền',
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                margin: EdgeInsets.all(10),
                decoration: containerDecoration(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    QuestionTextWidget(questionItem: QuestionItem(title: 'NaCl 0,9% ml')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM giọt/phút')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                decoration: containerDecoration(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    QuestionTextWidget(questionItem: QuestionItem(title: 'NaCl 3% ml')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM giọt/phút')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                alignment: Alignment.topLeft,
                decoration: containerDecoration(),
                child: Wrap(
                  alignment: WrapAlignment.start,
                  children: [
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Maninitol 20% ml')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM giọt/phút,')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'lập lại ml')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM giọt/phút')),
                    QuestionTextWidget(questionItem: QuestionItem(title: ' mỗi (hr)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                decoration: containerDecoration(),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Khác (ml, mg)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM (giọt/phút, mg, đơn vị/hr)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                decoration: containerDecoration(),
                child: Row(
                  children: [
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Khác (ml, mg)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM (giọt/phút, mg, đơn vị/hr)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.all(10),
                decoration: containerDecoration(),
                child: Row(
                  children: [
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Khác (ml, mg)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'TTM (giọt/phút, mg, đơn vị/hr)')),
                    QuestionTextWidget(questionItem: QuestionItem(title: 'Giải thích')),
                  ],
                ),
              ),
              _addButotn()
            ],
          )
        ],
      ),
    );
  }

  _addButotn() {
    return FlatButton(
      onPressed: () {},
      child: Text("Thêm"),
    );
  }

  _headerYLenh() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50),
      decoration: containerDecoration(radius: 0, color: Colors.grey[200]),

      height: 40,
      alignment: Alignment.topCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.center,
              child: Text('Đánh giá'),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              alignment: Alignment.center,
              child: Text('Y lệnh'),
            ),
          )
        ],
      ),
    );
  }

  _oxy() {
    ValueNotifier<bool> tacnghen = ValueNotifier<bool>(false);
    ValueNotifier<bool> maskApluc = ValueNotifier<bool>(false);
    ValueNotifier<bool> maskApluc1 = ValueNotifier<bool>(false);

    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Oxy',
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: QuestionYNShowMultipleWidget(
                        title: 'Hypoxia spO2<94%',
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          QuestionYNShowMultipleWidget(
                            horizontal: false,
                            title: 'Bổ sung Oxy qua',
                            mainOptions: [
                              'mũi 2 nhánh',
                              'mũi 1 nhánh',
                              'Mask Venturi (40%)',
                              'Mask có túi dự trữ',
                              'NKQ',
                              'Mask áp lực dương 2 thì',
                              'Thở máy'
                            ],
                            callbackYN: (indexes) {
                              if (indexes == 0 || indexes == 1 || indexes == 2 || indexes == 3 || indexes == 4) {
                                maskApluc.value = true;
                              } else {
                                maskApluc.value = false;
                              }
                            },
                          ),
                          ValueListenableBuilder<bool>(
                            valueListenable: maskApluc,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionSingleWidget(
                                      questionItem: QuestionItem(title: '', options: ['2l/p', '3l/p', '>3l/p']))
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          QuestionYNShowMultipleWidget(
                            title: 'Hypoxia (spO2<96% kéo dài > 5 phút)',
                            subTitle: 'Nguyên nhân',
                            horizontal: false,
                            extraOptions: [
                              'Không rõ',
                              'Kiểu thở bất thường',
                              'Giảm thông khí',
                              'Hít sặc',
                              'Xẹp phổi ',
                              'Viêm phổi',
                              'Bệnh tim',
                              'Tắc nghẽn một phần đường thở',
                            ],
                            callBackChildOption: (indexes) {
                              if (indexes.contains(7)) {
                                tacnghen.value = true;
                              } else {
                                tacnghen.value = false;
                              }
                            },
                          ),
                          ValueListenableBuilder<bool>(
                            valueListenable: tacnghen,
                            builder: (_, value, child) {
                              return value
                                  ? Container(
                                      padding: EdgeInsets.all(10),
                                      margin: EdgeInsets.only(left: 60, right: 10, top: 10, bottom: 10),
                                      decoration: containerDecoration(),
                                      child: QuestionMultipleWidget(
                                          questionItem:
                                              QuestionItem(title: '', options: ['Liệt hầu họng', 'Đàm nhớt'])))
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          QuestionYNShowMultipleWidget(
                            horizontal: false,
                            title: 'Bổ sung Oxy qua',
                            mainOptions: [
                              'mũi 2 nhánh',
                              'mũi 1 nhánh',
                              'Mask Venturi (40%)',
                              'Mask có túi dự trữ',
                              'NKQ',
                              'Mask áp lực dương 2 thì',
                              'Thở máy'
                            ],
                            callbackYN: (indexes) {
                              if (indexes == 0 || indexes == 1 || indexes == 2 || indexes == 3 || indexes == 4) {
                                maskApluc1.value = true;
                              } else {
                                maskApluc1.value = false;
                              }
                            },
                          ),
                          ValueListenableBuilder<bool>(
                            valueListenable: maskApluc1,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionSingleWidget(
                                      questionItem: QuestionItem(title: '', options: ['2l/p', '3l/p', '>3l/p']))
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _tuthe() {
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      margin: EdgeInsets.all(10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Tư thế',
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: QuestionYNShowMultipleWidget(
                        title: 'Hypoxia',
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: '', options: [
                            'Nằm ngữa',
                            'nằm nghiêng',
                            'Đầu bằng',
                            'Đầu cao 15o- 30',
                            'Có thể đi lai'
                          ])),
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: 'Xoay trở mỗi', options: ['1hr', '2hr', '3hr', '4hr'])),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: QuestionYNShowMultipleWidget(
                        title: 'Hypoxia khi thay đổi tư thế',
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(
                                  title: '', options: ['Nằm ngữa', 'nằm nghiêng', 'Đầu bằng', 'Đầu cao 15o- 30'])),
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: 'Xoay trở mỗi', options: ['1hr', '2hr', '3hr', '4hr'])),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: QuestionYNShowMultipleWidget(
                        title: 'Bệnh phổi nặng',
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: '', options: [
                            'Nằm ngữa',
                            'nằm nghiêng',
                            'Đầu bằng',
                            'Đầu cao 15o- 30',
                          ])),
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: 'Xoay trở mỗi', options: ['1hr', '2hr', '3hr', '4hr'])),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 2,
                      child: QuestionYNShowMultipleWidget(
                        title: 'Nguy cơ tắc nghẽn đường thở, hít sặc hoặc TALNS ',
                        subTitle: 'Ảnh hưởng',
                        extraOptions: ['Sp02', 'tắc nghẽn đường thở', 'tri giác'],
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(
                                  title: '', options: ['Nằm ngữa', 'nằm nghiêng', 'Đầu bằng', 'Đầu cao 15o- 30'])),
                          QuestionMultipleWidget(
                              questionItem: QuestionItem(title: 'Xoay trở mỗi', options: ['1hr', '2hr', '3hr', '4hr'])),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
