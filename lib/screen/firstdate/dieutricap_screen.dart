import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/widget/dropdown_cell.dart';
import 'package:flutter_app/widget/question_button_widget.dart';
import 'package:flutter_app/widget/question_check_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_check_show_text_widget.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/question_yn_show_textfield_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';
import 'package:flutter_app/widget/sub_section_widget.dart';
import 'package:flutter_app/widget/table_cell.dart';

class DieuTriCapScreen extends StatefulWidget {
  DieuTriCapScreen();

  @override
  State createState() {
    return _DieuTriCapScreenState();
  }
}

class _DieuTriCapScreenState extends State<DieuTriCapScreen> {
  ValueNotifier<bool> _datSp02 = ValueNotifier<bool>(false);

  ValueNotifier<bool> _ylenh = ValueNotifier<bool>(false);
  ValueNotifier<bool> _ketquaSp02 = ValueNotifier<bool>(false);
  ValueNotifier<bool> _datnkq = ValueNotifier<bool>(false);
  ValueNotifier<bool> _koduytrisp02 = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tangCO2 = ValueNotifier<bool>(false);
  ValueNotifier<bool> _anhhuongCO2SPO2 = ValueNotifier<bool>(false);
  ValueNotifier<bool> _chidinhnkq = ValueNotifier<bool>(false);
  ValueNotifier<bool> _suyhohap = ValueNotifier<bool>(false);
  ValueNotifier<bool> _phanxabaove = ValueNotifier<bool>(false);
  ValueNotifier<bool> _caidatbandau = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dieuchinhcaidatbandau = ValueNotifier<bool>(false);
  ValueNotifier<bool> _bncotha = ValueNotifier<bool>(false);
  ValueNotifier<int> _cotha = ValueNotifier<int>(-1);
  ValueNotifier<bool> _tanls = ValueNotifier<bool>(false);
  ValueNotifier<bool> _tangtrophin = ValueNotifier<bool>(false);
  ValueNotifier<bool> _ecg = ValueNotifier<bool>(false);
  ValueNotifier<bool> _thoatvi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _buoc1 = ValueNotifier<bool>(false);
  ValueNotifier<bool> _buoc2 = ValueNotifier<bool>(false);
  ValueNotifier<bool> _pptalns = ValueNotifier<bool>(false);
  ValueNotifier<bool> _cppchuan = ValueNotifier<bool>(false);
  ValueNotifier<bool> _anthan = ValueNotifier<bool>(false);
  ValueNotifier<bool> _pttk = ValueNotifier<bool>(false);
  ValueNotifier<bool> _kodatmuctieu = ValueNotifier<bool>(false);
  ValueNotifier<bool> _datcppchuan = ValueNotifier<bool>(false);
  ValueNotifier<bool> _dieutrithamthau = ValueNotifier<bool>(false);
  ValueNotifier<bool> _thongkhi = ValueNotifier<bool>(false);
  ValueNotifier<bool> _thannhiet = ValueNotifier<bool>(false);
  ValueNotifier<bool> _daonguockhangdong = ValueNotifier<bool>(false);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            children: [
              _chamsochohapUI(),
              _chamsoctimmachUI(),
              _cammauvadaonguocrldm(),
              _dieuchinhduonghuyet(),
              _dieuchinhthannhiet(),
              _phongnguavakiemsoatcogiat(),
              _canbangnuoc(),
              _dichtruyen(),
              _phauthuatxhn()
            ],
          ),
        ),
      ),
    );
  }

  _headerYLenh() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 50),
      decoration: containerDecoration(radius: 0, color: Colors.grey[200]),

      height: 40,
      alignment: Alignment.topCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            flex: 2,
            child: Container(
              alignment: Alignment.center,
              child: Text('Đánh giá'),
            ),
          ),
          Expanded(
            flex: 5,
            child: Container(
              alignment: Alignment.center,
              child: Text('Y lệnh'),
            ),
          )
        ],
      ),
    );
  }

  _chamsochohapUI() {
    ValueNotifier<bool> httk = ValueNotifier<bool>(false);

    ValueNotifier<bool> tacnghen = ValueNotifier<bool>(false);
    ValueNotifier<bool> maskApluc = ValueNotifier<bool>(false);
    ValueNotifier<bool> maskApluc1 = ValueNotifier<bool>(false);

    ValueNotifier<int> spO2 = ValueNotifier<int>(-1);
    return Container(
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        children: [
          SectionWidget(
            title: 'Chăm sóc hô hấp',
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm đánh giá')),
                          QuestionYNShowMultipleWidget(
                            optionWidth: 250,
                            title: 'Đạt Sp02 > 93%',
                            mainOptions: ['Có với pp can thiệp','Không với tất cả các pp can thiệp'],
                            callbackYN: (index){
                              if(index == 0){
                                spO2.value = 0;
                              }else{
                                spO2.value = 1;
                              }
                            },

                          ),

                        ],
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          ValueListenableBuilder<int>(
                            valueListenable: spO2,
                            builder: (_, value, child){
                              if(value == 0){
                                return  Column(
                                  children: [
                                    QuestionYNShowMultipleWidget(
                                      horizontal: false,
                                      title: 'pp can thiệp',
                                      mainOptions: [
                                        'mũi 2 nhánh',
                                        'mũi 1 nhánh',
                                        'Mask Venturi (40%)',
                                        'Mask có túi dự trữ',
                                        'NKQ',
                                        'Mask áp lực dương 2 thì',
                                        'Thở máy'
                                      ],
                                      callbackYN: (indexes) {
                                        if (indexes == 0 || indexes == 1 || indexes == 2 || indexes == 3 || indexes == 4) {
                                          maskApluc.value = true;
                                        } else {
                                          maskApluc.value = false;
                                        }
                                      },
                                    ),
                                    QuestionTextWidget(questionItem: QuestionItem(title: 'Kết quả SpO2')),
                                    Text('Báo BS khi SpO2 < 94%'),
                                  ],
                                );
                              }else if(value == 1){
                                return Container();
                              }
                              return Container();

                            },
                          )
                          ,
                          ValueListenableBuilder<bool>(
                            valueListenable: maskApluc,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionSingleWidget(
                                  questionItem: QuestionItem(title: '', options: ['2l/p', '3l/p', '>3l/p']))
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm đánh giá')),
                          SubSectionWidget(title: 'Nhu cầu đặt NKQ',),
                          QuestionYNShowMultipleWidget(
                            title: 'Không duy trì đủ SpO2 < 94%',
                            extraOptions: ['GCS < 8 – 10', 'PaO2 < 60 -70'],
                          ),
                          QuestionYNShowMultipleWidget(
                            title: 'Tăng CO2',
                            extraOptions: [
                              'PaCO2 > 35 và TALNS',
                              'PaCO2 > 45',
                              'obstructive sleep apnea, PaCO2 > 45'
                            ],
                          ),
                          QuestionYNShowMultipleWidget(
                            title: 'Ảnh hưởng cả hai SpO2 và CO2',
                            extraOptions: ['Liệt hầu họng', 'Mất phản xạ ho, nôn', 'Tiếng rít thanh quản nặng'],
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                        QuestionYNShowMultipleWidget(
                            optionWidth: 250,
                            title: 'Đặt NKQ',
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                _headerYLenh(),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm đánh giá')),
                          SubSectionWidget(title: 'Nhu cầu thở máy',),
                          Container(
                            decoration: containerDecoration(borderColor: Colors.blue),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Container(
                                  width: (MediaQuery.of(context).size.width / 2 - 350),
                                  child: QuestionMultipleWidget(
                                    questionItem: QuestionItem(
                                        title: '',
                                        options: [
                                          'Po2 < 70 mmHg dù có hổ trợ O2 qua mũi, mask, NK',
                                          'Pco2 > 50 – 60 mmHg (trừ COPD hoặc các bệnh gây tăng CO2 mạn tính)',
                                          'Vital capacity < 500-600 mL'
                                        ],
                                        id: "1006"),
                                    horizontal: false,
                                  ),
                                ),
                                Container(
                                  width: (MediaQuery.of(context).size.width / 2 - 350),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      QuestionYNShowMultipleWidget(
                                        title: 'Có các dấu hiệu suy hô hấp',
                                        extraOptions: [
                                          'thở nhanh > 35l/',
                                          'co kéo các cơ hô hấp phụ,',
                                          'hô hấp đảo nghịch'
                                        ],
                                      ),
                                      QuestionYNShowMultipleWidget(
                                        title: 'Toan hô hấp nặng',
                                      ),
                                      QuestionYNShowMultipleWidget(
                                        title: 'Mất các phản xạ bảo vệ đường thở',
                                        extraOptions: ['Ho', 'Nuốt', 'giảm mức độ ý thức'],
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 5,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          QuestionYNShowMultipleWidget(
                            optionWidth: 250,
                            title: 'Thở máy',
                          ),
                          _caidatUI()
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),

//          Container(
//            margin: EdgeInsets.all(10),
//            decoration: containerDecoration(borderColor: Colors.blue),
//            child: Column(
//              children: [
//                QuestionYNShowMultipleWidget(
//                  title: 'Hỗ trợ thông khí',
//                  callbackYN: (value) {
//                    if (value == 0) {
//                      httk.value = true;
//                    } else {
//                      httk.value = false;
//                    }
//                  },
//                ),
////                Container(
////                  margin: EdgeInsets.only(left: 40),
////                  child: ValueListenableBuilder<bool>(
////                    valueListenable: httk,
////                    builder: (_, value, child) {
////                      return value
////                          ? Column(
////                              crossAxisAlignment: CrossAxisAlignment.start,
////                              children: [
//////                                ,
////
////                              ],
////                            )
////                          : Container();
////                    },
////                  ),
////                ),
//              ],
//            ),
//          ),
        ],
      ),
    );
  }

  _caidatUI() {
    ValueNotifier<bool> thaydoimuctieu = ValueNotifier<bool>(false);
    return Container(
      alignment: Alignment.topCenter,
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Cài đặt',
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              children: [
                SectionWidget(
                  title: 'Mục tiêu',
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      width: (MediaQuery.of(context).size.width / 2 - 350),
                      child: Column(
                        children: [
                          QuestionTextWidget(questionItem: QuestionItem(title: 'Pao2: (ex: 80 – 100 mmHg)')),
                          QuestionTextWidget(
                              questionItem:
                                  QuestionItem(title: 'Paco2: (ex: 38 – 42 mmHg (28-32 mmHg ở gđ đầu TALNS))')),
                          QuestionTextWidget(questionItem: QuestionItem(title: 'pH: (ex:7.35 – 7.45)')),
                        ],
                      ),
                    ),
                    Container(
                      width: (MediaQuery.of(context).size.width / 2 - 350),
                      child: Column(
                        children: [
                          QuestionMultipleWidget(
                            questionItem: QuestionItem(
                                type: QuestionType.MULTIPLE_CHOICE,
                                title: '',
                                options: ['Cài đặt ban đầu'],
                                id: "1006"),
                            callback: (idx, select, indexes) {
                              if (indexes.contains(idx)) {
                                _caidatbandau.value = true;
                              } else {
                                _caidatbandau.value = false;
                              }
                            },
                          ),
                          ValueListenableBuilder<bool>(
                            valueListenable: _caidatbandau,
                            builder: (_, value, child) {
                              return value
                                  ? QuestionMultipleWidget(
                                      optionWidth: 280,
                                      isChild: true,
                                      questionItem: QuestionItem(
                                          type: QuestionType.MULTIPLE_CHOICE,
                                          title: '',
                                          options: [
                                            'I:E 1:2, và PEEP 5 – 8 cm H2O.',
                                            'Volume-controlled ventilation: tidal volume 6 – 8 mL/kg, inspiratory flow: 30 L/p, và frequency: 10 – 15 l/p',
                                            'Pressure-controlled ventilation: inspiratory pressures 12 – 14 cm H2O trên PEEP. End-inspiratory pre'
                                          ],
                                          id: "1006"),
                                      callback: (idx, select, indexes) {},
                                    )
                                  : Container();
                            },
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          QuestionYNShowMultipleWidget(
            title: 'Điều chỉnh cài đặt ban đầu',
            extraOptions: [
              'không dung nạp',
              'không đạt mục tiêu',
              'thay đổi mục tiêu',
//              'Có bệnh phổi trước (COPD)',
//              'Viêm phổi',
//              'ARDS',
//              'OAP'
            ],
            callBackChildOption: (indexes) {
              if (indexes.contains(2)) {
                thaydoimuctieu.value = true;
              } else {
                thaydoimuctieu.value = false;
              }
            },
          ),
          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
              valueListenable: thaydoimuctieu,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: 'Ghi rõ mong muón')) : Container();
              },
            ),
          ),
          Container(
//                  decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
            child: Column(
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: 'Thời điểm thay đổi'),
                  TableCellItem(title: ''),
                  TableCellItem(title: 'Pao2'),
                  TableCellItem(title: 'Paco2'),
                  TableCellItem(title: 'pH'),
                  TableCellItem(title: '(A-a)∆O2'),
                  TableCellItem(title: ''),
                  TableCellItem(title: 'Pao2'),
                  TableCellItem(title: 'Paco2'),
                  TableCellItem(title: 'pH'),
                  TableCellItem(title: '(A-a)∆O2'),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: ''),
                  TableCellItem(title: 'KMĐM trước thay đổi'),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: 'KMĐM sau thay đổi (30 – 60p)'),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: ''),
                  TableCellItem(title: 'KMĐM trước thay đổi'),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: 'KMĐM sau thay đổi (30 – 60p)'),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                ]),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    color: Colors.blue,
                    onPressed: () {},
                    child: Text('Thêm'),
                  ),
                )
              ],
            ),
          ),
          Container(
            decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SectionWidget(
                  title: 'An thần',
                ),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: 'Thời điểm'),
                  TableCellItem(title: 'Fentanyl'),
                  TableCellItem(title: 'Midazolam'),
                  TableCellItem(title: 'Propofol'),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                ]),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    color: Colors.blue,
                    onPressed: () {},
                    child: Text('Thêm'),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  _chamsoctimmachUI() {
    ValueNotifier<bool> vanmach = ValueNotifier<bool>(false);
    ValueNotifier<bool> haHa = ValueNotifier<bool>(false);
    ValueNotifier<bool> khac = ValueNotifier<bool>(false);
    ValueNotifier<bool> dieutri = ValueNotifier<bool>(false);
    ValueNotifier<bool> theodoi = ValueNotifier<bool>(false);
    return Container(
      margin: EdgeInsets.only(top: 16),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title:
                'Chăm sóc tim mạch: cân bằng giữa tưới máu não tối đa, giảm nguy cơ lan rộng ổ XHN VÀ bảo vệ não, cơ quan khác (đặc biệt:Tim, Thận',
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SectionWidget(
                  title: 'HA',
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Dựa vào: CPP = MAP - ICP',
                  extraOptions: [
                    'MAP < 130 mmHg',
                    'MAP > 130 – Hạ HA PIV',
                    'Giảm MAP < 30% trong 24 h đầu',
                    'CPP > 60 mmHg nếu BN tỉnh',
                    'CPP > 60 mmHg nếu BN tỉnh',
                    'Thuốc vận mạch'
                  ],
                  callBackChildOption: (indexes) {
                    if (indexes.contains(1)) {
                      vanmach.value = true;
                    } else {
                      vanmach.value = false;
                    }

                    if (indexes.contains(5)) {
                      haHa.value = true;
                    } else {
                      haHa.value = false;
                    }
                  },
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: vanmach,
                    builder: (_, value, child) {
                      return value
                          ? QuestionTextWidget(
                              questionItem: QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: '', id: "1006"),
                            )
                          : Container();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: haHa,
                    builder: (_, value, child) {
                      return value
                          ? QuestionTextWidget(
                              questionItem: QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: '', id: "1006"),
                            )
                          : Container();
                    },
                  ),
                ),
                SectionWidget(
                  title: 'Một số căn cứ chọn lựa mức HA điều chỉnh',
                  color: Colors.transparent,
                ),
                QuestionMultipleWidget(
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: 'Nêu ở phần tình trạng Nội khoa:',
                      options: ['I', 'II', 'III', 'IV', 'V', 'VI'],
                      id: "1006"),
                  callback: (idx, select, indexes) {},
                ),
                QuestionYNShowMultipleWidget(
                  title: 'BN có TC THA',
                  callbackYN: (index) {
                    if (index == 0) {
                      _cotha.value = 0;
                    } else {
                      _cotha.value = 1;
                    }
                  },
                ),
                ValueListenableBuilder<int>(
                  valueListenable: _cotha,
                  builder: (_, value, child) {
                    if (value == 0) {
                      return QuestionMultipleWidget(
                        isChild: true,
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: '',
                            options: [
                              'CPP > 70 mmHg',
                            ],
                            id: "1006"),
                        callback: (idx, select, indexes) {},
                      );
                    } else if (value == 1) {
                      return QuestionMultipleWidget(
                        isChild: true,
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: '',
                            options: [
                              'CPP > 60 mmHg',
                            ],
                            id: "1006"),
                        callback: (idx, select, indexes) {},
                      );
                    } else {
                      return Container();
                    }
                  },
                ),
                QuestionYNShowMultipleWidget(
                  title: 'TALNS',
                  subTitle: 'Dựa vào',
                  extraOptions: ['đo ICP', 'lâm sàng + GCS < 8', 'Giảm HATB < 30%'],
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SectionWidget(
                  title: 'Loạn nhịp tim',
                ),
                QuestionMultipleWidget(
                  optionWidth: 200,
                  questionItem: QuestionItem(
                      type: QuestionType.MULTIPLE_CHOICE,
                      title: 'Loạn nhịp tim',
                      options: [
                        'Rung nhĩ',
                        'Nhịp nhanh kịch phát trên thất',
                        'Chậm xoang',
                        'Khác',
                        'Ảnh hưởng huyết động',
                        'Điều trị',
                        'Theo dỏi'
                      ],
                      id: "1006"),
                  callback: (idx, select, indexes) {
                    if (indexes.contains(3)) {
                      khac.value = true;
                    } else {
                      khac.value = false;
                    }

                    if (indexes.contains(5)) {
                      dieutri.value = true;
                    } else {
                      dieutri.value = false;
                    }

                    if (indexes.contains(6)) {
                      theodoi.value = true;
                    } else {
                      theodoi.value = false;
                    }
                  },
                ),
                Row(
                  children: [
                    ValueListenableBuilder<bool>(
                      valueListenable: khac,
                      builder: (_, value, child) {
                        return value
                            ? QuestionTextWidget(
                                questionItem:
                                    QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: 'Khác', id: "1006"))
                            : Container();
                      },
                    ),
                    ValueListenableBuilder<bool>(
                      valueListenable: dieutri,
                      builder: (_, value, child) {
                        return value
                            ? QuestionTextWidget(
                                questionItem:
                                    QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: 'Điều trị ', id: "1006"))
                            : Container();
                      },
                    ),
                    ValueListenableBuilder<bool>(
                      valueListenable: theodoi,
                      builder: (_, value, child) {
                        return value
                            ? QuestionTextWidget(
                                questionItem:
                                    QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: 'Theo dõi ', id: "1006"))
                            : Container();
                      },
                    ),
                  ],
                )
              ],
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SectionWidget(
                  title: 'NMCT',
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Tăng Troponin ban đầu',
                  extraOptions: [
                    'Các bệnh kèm',
                    'Suy thận',
                    'Thuyên tắc phổi',
                    'Nhiễm trùng',
                    ' THA quá mức',
                    'Bệnh cơ tim choáng váng'
                  ],
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Troponin, ECG kiểm tra sau 3 giờ',
                  extraOptions: [
                    'Troponin thay đổi < 30%: ít nghĩ NMCT',
                    'Troponin thay đổi > 30%: có thể NMCT, Hội chẩn'
                  ],
                  optionWidth: 250,
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
            child: Column(
              children: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: 'Thời điểm'),
                  TableCellItem(title: 'Mạch l/p'),
                  TableCellItem(title: 'HATthu mmHg'),
                  TableCellItem(title: 'HATtrương mmHg'),
                  TableCellItem(title: 'HATB mmHg'),
                  TableCellItem(title: 'Cần điều chỉnh'),
                  TableCellItem(title: 'Phương pháp chọn lựa'),
                  TableCellItem(title: 'HA mục tiêu'),
                  TableCellItem(title: 'HA Lí do'),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  DropDownCell(options: ['Xuống', 'Nâng lên']),
                  TableCellItem(title: ''),
                  DropDownCell(options: ['Đạt', 'Không đạt']),
                  TableCellItem(title: ''),
                ]),
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: [
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  TableCellItem(title: ''),
                  DropDownCell(options: ['Xuống', 'Nâng lên']),
                  TableCellItem(title: ''),
                  DropDownCell(options: ['Đạt', 'Không đạt']),
                  TableCellItem(title: ''),
                ]),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    color: Colors.blue,
                    onPressed: () {},
                    child: Text('Thêm'),
                  ),
                )
              ],
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue, radius: 0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SectionWidget(
                  title: 'Kiểm soát tình trạng TALNS',
                ),
                SectionWidget(
                  title: 'Tình trạng BN',
                  color: Colors.transparent,
                ),
                Container(

                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        child: Column(
                          children: [
                            QuestionDateWidget(
                              questionItem: QuestionItem(title: 'Thời điểm'),
                            ),
                            Column(
                              children: [
                                QuestionButtonWidget(questionItem: QuestionItem(title: 'GCS')),
                                QuestionButtonWidget(questionItem: QuestionItem(title: 'NIHSS')),
                                QuestionMultipleWidget(
                                  optionWidth: 100,
                                  isChild: true,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'Giảm ý thức tri giác',
                                        'Hôn mê',
                                        'Các dấu hiệu gợi ý thoát vị',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(2)) {
                                      _thoatvi.value = true;
                                    } else {
                                      _thoatvi.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  alignment: Alignment.topRight,
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder<bool>(
                                    valueListenable: _thoatvi,
                                    builder: (_, value, child) {
                                      return value
                                          ? QuestionMultipleWidget(
                                        optionWidth: 100,
                                        isChild: true,
                                        questionItem: QuestionItem(
                                            type: QuestionType.MULTIPLE_CHOICE,
                                            title: '',
                                            options: [
                                              'đồng tử',
                                              'nhịp thở',
                                              'gồng',
                                            ],
                                            id: "1006"),
                                        callback: (idx, select, indexes) {},
                                      )
                                          : Container();
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),

                      QuestionMultipleWidget(
                        optionWidth: 100,
                        isChild: true,
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: '',
                            options: [
                              'Bước 1',
                            ],
                            id: "1006"),
                        callback: (idx, select, indexes) {
                          if (indexes.contains(0)) {
                            _buoc1.value = true;
                          } else {
                            _buoc1.value = false;
                          }
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 40),
                        child: ValueListenableBuilder<bool>(
                          valueListenable: _buoc1,
                          builder: (_, value, child) {
                            return value
                                ? QuestionMultipleWidget(
                                    optionWidth: 100,
                                    isChild: true,
                                    questionItem: QuestionItem(
                                        type: QuestionType.MULTIPLE_CHOICE,
                                        title: '',
                                        options: [
                                          'Tư thế đầu 30(o)',
                                          'Mannitol 20% 1 – 1,5g/kg',
                                          'Tăng thông khí đạt PaCO2 26 – 32 mmHg',
                                        ],
                                        id: "1006"),
                                    callback: (idx, select, indexes) {},
                                  )
                                : Container();
                          },
                        ),
                      ),
                      QuestionMultipleWidget(
                        optionWidth: 600,
                        isChild: true,
                        questionItem: QuestionItem(
                            type: QuestionType.MULTIPLE_CHOICE,
                            title: '',
                            options: [
                              'Bước 2 hoặc HA thấp (Truyền DD NaCl ưu trương qua đường TM lớn (giúp giảm nhanh và hiệu quả ICP, trong thời gian chờ đợi các biện pháp PTTK))',
                            ],
                            id: "1006"),
                        callback: (idx, select, indexes) {
                          if (indexes.contains(0)) {
                            _buoc2.value = true;
                          } else {
                            _buoc2.value = false;
                          }
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 40),
                        child: ValueListenableBuilder<bool>(
                          valueListenable: _buoc2,
                          builder: (_, value, child) {
                            return value
                                ? QuestionMultipleWidget(
                                    optionWidth: 180,
                                    isChild: true,
                                    questionItem: QuestionItem(
                                        type: QuestionType.MULTIPLE_CHOICE,
                                        title: '',
                                        options: [
                                          'PTTK',
                                          'Mở sọ giải ép',
                                          'EVD',
                                          'Đặt dụng cụ theo dỏi ICP',
                                          'Lấy máu tụ'
                                        ],
                                        id: "1006"),
                                    callback: (idx, select, indexes) {},
                                  )
                                : Container();
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: FlatButton(
                    color: Colors.blue,
                    onPressed: () {},
                    child: Text('Thêm'),
                  ),
                ),
                QuestionYNShowMultipleWidget(
                  title: 'Các PP kiểm soát TALNS',
                  callbackYN: (index) {
                    if (index == 0) {
                      _pptalns.value = true;
                    } else {
                      _pptalns.value = false;
                    }
                  },
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _pptalns,
                    builder: (_, value, child) {
                      return value
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                QuestionMultipleWidget(
                                  optionWidth: 180,
                                  isChild: true,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'PTTK',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      _pttk.value = true;
                                    } else {
                                      _pttk.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder<bool>(
                                      valueListenable: _pttk,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    optionWidth: 180,
                                                    isChild: true,
                                                    questionItem: QuestionItem(
                                                        type: QuestionType.MULTIPLE_CHOICE,
                                                        title: '',
                                                        options: [
                                                          'KQ CT scan kiểm tra trước chỉ định',
                                                          'EVD',
                                                          'Đặt dụng cụ theo dỏi ICP',
                                                          'GCS ≤ 8 đ',
                                                          'Đạt mục tiêu',
                                                          'ICP < 20 mmH2O ( 15 mmHg)',
                                                          'CPP > 70 mmHg',
                                                          'Không đạt mục tiêu, lí do'
                                                        ],
                                                        id: "1006"),
                                                    callback: (idx, select, indexes) {
                                                      if (indexes.contains(7)) {
                                                        _kodatmuctieu.value = true;
                                                      } else {
                                                        _kodatmuctieu.value = false;
                                                      }
                                                    },
                                                  ),
                                                ],
                                              )
                                            : Container();
                                      }),
                                ),
                                ValueListenableBuilder<bool>(
                                  valueListenable: _kodatmuctieu,
                                  builder: (_, value, child) {
                                    return value
                                        ? QuestionTextWidget(
                                            questionItem:
                                                QuestionItem(type: QuestionType.MULTIPLE_CHOICE, title: '', id: "1006"),
                                          )
                                        : Container();
                                  },
                                )
                              ],
                            )
                          : Container();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _pptalns,
                    builder: (_, value, child) {
                      return value
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                QuestionMultipleWidget(
                                  optionWidth: 180,
                                  isChild: true,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'An thần TM',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      _anthan.value = true;
                                    } else {
                                      _anthan.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder<bool>(
                                      valueListenable: _anthan,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    optionWidth: 180,
                                                    isChild: true,
                                                    questionItem: QuestionItem(
                                                        type: QuestionType.MULTIPLE_CHOICE,
                                                        title: '',
                                                        options: [
                                                          'Đạt trạng thái nằm yên, bất động',
                                                          'Không đạt',
                                                          'fentanyl (1 - 3 μg/kg/h) – giảm đau',
                                                          'propofol (0.3 - 3 mg/kg/h)',
                                                        ],
                                                        id: "1006"),
                                                  ),
                                                ],
                                              )
                                            : Container();
                                      }),
                                ),
                              ],
                            )
                          : Container();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _pptalns,
                    builder: (_, value, child) {
                      return value
                          ? Container(
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  QuestionMultipleWidget(
                                    optionWidth: 180,
                                    isChild: true,
                                    questionItem: QuestionItem(
                                        type: QuestionType.MULTIPLE_CHOICE,
                                        title: '',
                                        options: [
                                          'CPP chuẩn ',
                                        ],
                                        id: "1006"),
                                    callback: (idx, select, indexes) {
                                      if (indexes.contains(idx)) {
                                        _cppchuan.value = true;
                                      } else {
                                        _cppchuan.value = false;
                                      }
                                    },
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(left: 40),
                                    child: ValueListenableBuilder<bool>(
                                      valueListenable: _cppchuan,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    optionWidth: 180,
                                                    isChild: true,
                                                    questionItem: QuestionItem(
                                                        type: QuestionType.MULTIPLE_CHOICE,
                                                        title: '',
                                                        options: [
                                                          'Thuốc vận mạch nếu CPP < 70 mmHg',
                                                          'Hạ huyết áp nếu CPP > 110 mmHg',
                                                          'Đạt',
                                                        ],
                                                        id: "1006"),
                                                    callback: (idx, select, indexes) {
                                                      if (indexes.contains(2)) {
                                                        _datcppchuan.value = true;
                                                      } else {
                                                        _datcppchuan.value = false;
                                                      }
                                                    },
                                                  )
                                                ],
                                              )
                                            : Container();
                                      },
                                    ),
                                  ),
                                  ValueListenableBuilder<bool>(
                                    valueListenable: _datcppchuan,
                                    builder: (_, value, child) {
                                      return value
                                          ? Column(
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                QuestionMultipleWidget(
                                                  optionWidth: 180,
                                                  isChild: true,
                                                  questionItem: QuestionItem(
                                                      type: QuestionType.MULTIPLE_CHOICE,
                                                      title: '',
                                                      options: [
                                                        'Bình thể tích',
                                                        'Hb bình thường',
                                                        'Hct bình thường',
                                                        'Protein/máu bình thường',
                                                        'PaCO2 bình thường',
                                                        'PaO2 bình thường'
                                                      ],
                                                      id: "1006"),
                                                ),
                                                QuestionTextWidget(questionItem: QuestionItem(title: 'Can thiệp'))
                                              ],
                                            )
                                          : Container();
                                    },
                                  )
                                ],
                              ),
                            )
                          : Container();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _pptalns,
                    builder: (_, value, child) {
                      return value
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                QuestionMultipleWidget(
                                  optionWidth: 180,
                                  isChild: true,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'Điều trị thẩm thấu',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      _dieutrithamthau.value = true;
                                    } else {
                                      _dieutrithamthau.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder<bool>(
                                      valueListenable: _dieutrithamthau,
                                      builder: (_, value, child) {
                                        return value
                                            ? Column(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionMultipleWidget(
                                                    optionWidth: 180,
                                                    isChild: true,
                                                    questionItem: QuestionItem(
                                                        type: QuestionType.MULTIPLE_CHOICE,
                                                        title: '',
                                                        options: [
                                                          'Mannitol 20% 1 – 1,5 g/kg Sau đó 0,25 – 1g/kg khi cần đề đạt ALTT 300 -320 mOsm/l',
                                                          'NaCl ưu trương ;',
                                                          'lập lại (1-6h) khi cần đề đạt ALTT 300 -320 mOsm',
                                                        ],
                                                        id: "1006"),
                                                  ),
                                                ],
                                              )
                                            : Container();
                                      }),
                                ),
                              ],
                            )
                          : Container();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _pptalns,
                    builder: (_, value, child) {
                      return value
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                QuestionMultipleWidget(
                                  optionWidth: 180,
                                  isChild: true,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'KS thông khí',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      _thongkhi.value = true;
                                    } else {
                                      _thongkhi.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder<bool>(
                                      valueListenable: _thongkhi,
                                      builder: (_, value, child) {
                                        return value
                                            ? Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionYNShowTextFieldWidget(
                                                    title: 'PaCO2 đạt 26 32 mmHg',
                                                    yesno: ['Đạt', 'Không đạt'],
                                                    correctIndex: 1,
                                                    subs: ['Lí do'],
                                                  ),
                                                ],
                                              )
                                            : Container();
                                      }),
                                ),
                              ],
                            )
                          : Container();
                    },
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(left: 40),
                  child: ValueListenableBuilder<bool>(
                    valueListenable: _pptalns,
                    builder: (_, value, child) {
                      return value
                          ? Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                QuestionMultipleWidget(
                                  optionWidth: 180,
                                  isChild: true,
                                  questionItem: QuestionItem(
                                      type: QuestionType.MULTIPLE_CHOICE,
                                      title: '',
                                      options: [
                                        'Hạ Thân nhiệt',
                                      ],
                                      id: "1006"),
                                  callback: (idx, select, indexes) {
                                    if (indexes.contains(idx)) {
                                      _thannhiet.value = true;
                                    } else {
                                      _thannhiet.value = false;
                                    }
                                  },
                                ),
                                Container(
                                  margin: EdgeInsets.only(left: 40),
                                  child: ValueListenableBuilder<bool>(
                                      valueListenable: _thannhiet,
                                      builder: (_, value, child) {
                                        return value
                                            ? Row(
                                                mainAxisAlignment: MainAxisAlignment.start,
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                                  QuestionYNShowTextFieldWidget(
                                                    title: 'Thân nhiệt 32 - 330C',
                                                    yesno: ['Đạt', 'Không đạt'],
                                                    correctIndex: 1,
                                                    subs: ['Lí do'],
                                                  ),
                                                ],
                                              )
                                            : Container();
                                      }),
                                ),
                              ],
                            )
                          : Container();
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  _cammauvadaonguocrldm() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.all(10),
      decoration: containerDecoration(borderColor: Colors.blue),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SectionWidget(
            title: 'Cầm máu và đảo ngược tình trạng RLĐM',
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: QuestionMultipleWidget(
              optionWidth: 800,
              questionItem: QuestionItem(title: '', options: [
                'Recombinant factor VII (rFVIIa, Novoseven®, Novo Nordisk) – tăng cầm máu khi hệ thống đông máu bình thường: Kết quả không khác biệt về tỉ lệ tử vong và mất chức năng nặng (mRS 5-6) với liều 20, 80 µg/kg sử dụng trong vòng 4h sau khởi phát XHN so với placebo (FAST 2008) – Class III, B'
              ]),
            ),
          ),
          Container(
            margin: EdgeInsets.all(10),
            decoration: containerDecoration(borderColor: Colors.blue),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                QuestionYNShowMultipleWidget(
                  title: 'Đảo ngược kháng đông',
                  callbackYN: (index) {
                    if (index == 0) {
                      _daonguockhangdong.value = true;
                    } else {
                      _daonguockhangdong.value = false;
                    }
                  },
                ),
                _daonguockhangdongUI()
              ],
            ),
          ),
        ],
      ),
    );
  }

  _canbangnuoc(){
    ValueNotifier<bool> nacl = ValueNotifier<bool>(false);
    ValueNotifier<bool> show1 = ValueNotifier<bool>(false);
    ValueNotifier<bool> show2 = ValueNotifier<bool>(false);

    return Container(
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionWidget(
              title: 'Cân bằng nước – điện giải',
            ),
            QuestionYNShowMultipleWidget(
              title: 'Bình thể tích ',
              extraOptions: [
                'Cân nặng ổn định',
                'Cân bằng xuất nhập ',
                'CVP 5 – 8 mmHg',
                'NaCl 0.9% (1ml/kg/h) ',
                'Đạt VNT > 0,5 ml/kg/h',
                'Tránh dùng NaCl 0,45%',
                'Tránh dùng Glucose 5%'
              ],
              callBackChildOption: (indexes){
                if(indexes.contains(3)){
                  nacl.value = true;
                }else{
                  nacl.value = false;
                }
              },
            ),

            ValueListenableBuilder<bool>(
              valueListenable: nacl,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: 'NaCl 0.9% (1ml/kg/h) ')) : Container();
              },
            ),
            QuestionYNShowTextFieldWidget(
              title: 'Giảm thể tích ',
              subs: ['NaCl 0.9% '],
            ),
            QuestionYNShowTextFieldWidget(
              title: 'Tăng thể tích ',
              subs: ['NaCl 0.9% '],
            ),
            QuestionYNShowMultipleWidget(
              title: 'HyperNatremia ',
              extraOptions: ['Kiểm soát chậm <12mEq/p/24 h', 'Kiểm soát không phù hợp '],
            )
          ],
        ));
  }

  _phongnguavakiemsoatcogiat() {
    ValueNotifier<bool> show = ValueNotifier<bool>(false);
    ValueNotifier<bool> show1 = ValueNotifier<bool>(false);
    ValueNotifier<bool> show2 = ValueNotifier<bool>(false);

    return Container(
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionWidget(
              title: 'Phòng ngừa và Kiểm soát co giật',
            ),
            QuestionMultipleWidget(
              questionItem: QuestionItem(
                title: '',
                  options: [
                    'Co giật',
                    'Hôn mê',
                    'Phù hợp giữa lâm sàng và hình ảnh học',
                    'EEG',
                    'EEG mornitoring',
                    'Chông co giật',
                    'Dự phòng',
                    'Điều trị',
                    'Tên thuốc',
                    'Loại co giật',
                    'Từng cơn',
                    'Chùm cơn',
                    'TTĐK',
                    'Thời điểm chẩn đoán'
                  ]
              ),
              callback: (idx, select, indexes) {
                if (indexes.contains(13)) {
                  show.value = true;
                } else {
                  show.value = false;
                }
              },
            ),
            ValueListenableBuilder<bool>(
              valueListenable: show,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
              },
            ),
            Column(
              children: [
                QuestionYNShowTextFieldWidget(
                  title: 'Kết quả kiểm soát co giật',
                  correctIndex: 1,
                  yesno: ['Đạt', 'Không đạt'],
                  subs: ['Lí do', 'Thời điểm đánh giá'],
                ),
                QuestionYNShowTextFieldWidget(
                  title: 'Kết quả kiểm soát co giật',
                  correctIndex: 1,
                  yesno: ['Đạt', 'Không đạt'],
                  subs: ['Lí do', 'Thời điểm đánh giá'],
                ),
              ],
            ),
          ],
        ));
  }

  _dichtruyen() {
    ValueNotifier<bool> show = ValueNotifier<bool>(false);
    ValueNotifier<bool> show1 = ValueNotifier<bool>(false);
    ValueNotifier<bool> show2 = ValueNotifier<bool>(false);
    return Container(
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionWidget(
              title: 'Dịch truyền',
            ),
            QuestionCheckShowMultipleWidget(
              title: 'Bình thể tích',
              optionWidth: 200,
              options: [
                'Cân nặng ổn định',
                'Cân bằng xuất nhập',
                'VP 5 – 8 mm',
                'NaCl 0.9% (1ml/kg/h)',
                'Đạt VNT > 0,5 ml/kg/h',
                'Tránh dùng NaCl 0,45%',
                'Tránh dùng Glucose 5%'
              ],
            ),
            QuestionCheckShowMultipleWidget(
              title: 'Na/máu - mục tiêu: > 140mEq/l',
              optionWidth: 200,
              options: ['Đạt', 'Không đạt, nêu lí do'],
              callback: (idx, indexes) {
                if (indexes.contains(1)) {
                  show.value = true;
                } else {
                  show.value = false;
                }
              },
            ),
            Container(
              width: 180,
              child: ValueListenableBuilder<bool>(
                valueListenable: show,
                builder: (_, value, child) {
                  return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
                },
              ),
            ),
            QuestionCheckShowTextWidget(
              title: "Giảm thể tích",
              inputTitle: 'NaCl 0.9%',
            ),
            QuestionCheckShowTextWidget(
              title: "Tăng thể tích",
              inputTitle: 'NaCl 0.9%',
            ),
            QuestionCheckShowMultipleWidget(
              title: 'ALTT máu',
              optionWidth: 200,
              showParentCheck: false,
              options: [
                'Hypoosmolality < 280 mOsm/l',
                'Mannitol 20%',
                'NaCl 3% (0,5 – 1 ml/kg/h)',
                'Đạt mục tiêu 300 – 320 mOsm/l và Na/máu 145 -155 mEq/l – nếu cần giảm ICP'
              ],
              callback: (idx, indexes) {
                if (indexes.contains(3)) {
                  show1.value = true;
                } else {
                  show1.value = false;
                }
              },
            ),
            Container(
              width: 180,
              child: ValueListenableBuilder<bool>(
                valueListenable: show1,
                builder: (_, value, child) {
                  return value ? QuestionTextWidget(questionItem: QuestionItem(title: 'Thời điểm')) : Container();
                },
              ),
            ),
            QuestionCheckShowMultipleWidget(
              title: 'HyperNatremia',
              optionWidth: 200,
              options: ['Kiểm soát chậm <12mEq/p/24 h', 'Kiểm soát không phù hợp'],
              callback: (idx, indexes) {
                if (indexes.contains(3)) {
                  show1.value = true;
                } else {
                  show1.value = false;
                }
              },
            ),
          ],
        ));
  }

  _phauthuatxhn() {

    List<String> titles = ['PT lấy máu tụ', 'PT Dẫn lưu DNT và Ventriculostomy lấy máu tụ ', 'PT Mở sọ giải ép', 'PT xâm lấn tối thiểu '];

    return Container(
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionWidget(
              title: 'Phẩu thuật XHN',
            ),
//            QuestionCheckShowMultipleWidget(
//              title: 'Phẩu thuật XHN',
//              optionWidth: 200,
//              options: [
//                'Ngăn và kiểm soát tởn thương não thứ phát d0 Ổ XHN hoặc TALNS',
//                'Nhận diện nguồn gốc XHN và kiểm soát'
//              ],
//            ),
            Column(
              children: List.generate(titles.length, (index) {
                ValueNotifier<bool> laymautu = ValueNotifier<bool>(false);
                return Container(
                  decoration: containerDecoration(),
                  child: Column(
                    children: [
                      QuestionYNShowMultipleWidget(

                        title: titles[index],
                        callbackYN: (index){
                          if(index == 0){
                            laymautu.value = true;
                          }else{
                            laymautu.value = false;
                          }
                        },
                      ),

                      ValueListenableBuilder<bool>(
                        valueListenable: laymautu,
                        builder: (_, value, child){
                          return value? Container(
                            margin: EdgeInsets.only(left: 40),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm')),
                                    SubSectionWidget(title: 'Chỉ định '),

                                    QuestionYNShowMultipleWidget(
                                      horizontal: false,
                                      title: 'BN < 75 tuổi',
                                      optionWidth: 200,
                                      extraOptions: ['VXHN > 3 cm3, chèn ép não thất 4','Sừng thái dương não thất bên lớn (não úng thủy không thông thương)',
                                        'Chèp ép thân não','Giảm ý thức ( không chờ đến Hôn mê nếu kết hợp các yếu tố trên) < 13'
                                      ],
                                    ),
                                    QuestionYNShowMultipleWidget(
                                      horizontal: false,
                                      optionWidth: 200,
                                      title: 'XHN trên lều',
                                      extraOptions: ['XHN thùy hoặc nông – gần bề mặt não',
                                        'VXHN > 2 cm3 và có hiệu ứng choán chổ','Đẩy lệch đường giữa > 5mm',
                                        'Xóa bể não cùng bên',
                                        'Giảm ý thức – GCS 9 – 12'
                                      ],
                                    ),
                                    index == 3? QuestionYNShowMultipleWidget(
                                      horizontal: false,
                                      optionWidth: 200,
                                      title: 'XH não thất ',
                                      extraOptions: ['Dẫn lưu não thất',
                                        '+-rtPA',
                                        'Xóa bể não cùng bên',
                                        'Giảm ý thức – GCS 9 – 12'
                                      ],
                                    ): Container(),
                                  ],
                                ),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [

                                    QuestionYNShowMultipleWidget(
                                      horizontal: false,
                                      optionWidth: 200,
                                      title: 'HC Ngoại thần kinh',
                                      extraOptions: ['Đồng ý phẩu thuật',
                                        'Không đồng ý PT',
                                        'Giải thích cho than nhân',
                                        'Đồng thuận PT',
                                        'Không đồng thuận '
                                      ],
                                    ),
                                    QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm HC')),
                                    QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm phẫu thuật')),
                                  ],
                                ),
                              ],
                            ),

                          ): Container();
                        },
                      ),

                    ],
                  ),
                );
              }),
            ),
            QuestionYNShowMultipleWidget(
              optionWidth: 200,
              title: 'PT các Nguyên nhân XHN',
              extraOptions: ['Phình mạch',
                'DD ĐM-TM',
                'Carvernous angioma',
                'Dural Fistulas',
                'U não'
              ],
            ),
          ],
        ));
  }

  _dieuchinhthannhiet() {
    ValueNotifier<bool> show = ValueNotifier<bool>(false);
    return Container(
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionWidget(
              title: 'Điều chỉnh thân nhiệt',
            ),
            QuestionTextWidget(questionItem: QuestionItem(title: "Nhiệt độ")),
            QuestionCheckShowMultipleWidget(
              title: 'Cần điều chỉnh',
              optionWidth: 200,
              options: ['Acetaminophene mg', 'uống', 'TTM'],
            ),
            QuestionCheckShowMultipleWidget(
              title: 'Thân nhiệt mục tiêu: 36 – 37,50',
              optionWidth: 200,
              options: ['Đạt', 'Không đạt, nêu lí do'],
              callback: (idx, indexes) {
                if (indexes.contains(idx)) {
                  show.value = true;
                } else {
                  show.value = false;
                }
              },
            ),
            ValueListenableBuilder<bool>(
              valueListenable: show,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
              },
            ),
          ],
        ));
  }

  _dieuchinhduonghuyet() {
    ValueNotifier<bool> khac = ValueNotifier<bool>(false);
    return Container(
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SectionWidget(
              title: 'Điều chỉnh đường huyết',
            ),
            Container(
                padding: EdgeInsets.all(10),
                margin: EdgeInsets.all(10),
                decoration: containerDecoration(borderColor: Colors.blue),
                child: Column(
                  children: [
                    QuestionYNShowMultipleWidget(
                      title: 'ĐH < 60mg%',
                      extraOptions: [
                        'Glucose 30% TTM', 'Nước đường, sữa' //, kiểm tra giờ
                      ],
                      callBackChildOption: (index) {
                        if (index.contains(0)) {
                          khac.value = true;
                        } else {
                          khac.value = false;
                        }
                      },
                    ),
                    ValueListenableBuilder<bool>(
                      valueListenable: khac,
                      builder: (_, value, child) {
                        return value
                            ? QuestionTextWidget(questionItem: QuestionItem(title: 'kiểm tra  giờ'))
                            : Container();
                      },
                    ),
                  ],
                )),
            _duytriUI(),
            _dhmuctieu(),
          ],
        ));
  }

  _duytriUI() {
    ValueNotifier<bool> tenlieu = ValueNotifier<bool>(false);
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          children: [
            QuestionCheckShowMultipleWidget(
              title: 'Duy trì',
              showParentCheck: false,
              optionWidth: 200,
              options: [
                '> 180 mg%',
                '140 – 180 mg%',
                '80 – 140 mg%',
                'Ổn định',
                'không ổn định',
                'Không thuốc',
                'Insulin TDD',
                'Insulin bơm tiêm điện',
                'thuốc uống, tên-liều'
              ],
              callback: (idx, indexes){
                if(indexes.contains(8)){
                  tenlieu.value = true;
                }else{
                  tenlieu.value = false;
                }
              },
            ),
            ValueListenableBuilder<bool>(
              valueListenable: tenlieu,
              builder: (_, value, child) {
                return value
                    ? QuestionTextWidget(questionItem: QuestionItem(title: 'Tên liều '))
                    : Container();
              },
            ),
          ],
        ));
  }

  _dhmuctieu() {
    ValueNotifier<bool> show = ValueNotifier<bool>(false);
    return Container(
        padding: EdgeInsets.all(10),
        margin: EdgeInsets.all(10),
        decoration: containerDecoration(borderColor: Colors.blue),
        child: Column(
          children: [
            QuestionCheckShowMultipleWidget(
              title: 'ĐH mục tiêu: 100 – 180 mg%',
              showParentCheck: false,
              optionWidth: 200,
              options: ['Đạt', 'Không đạt, nêu lí do'],
              callback: (idx, indexes) {
                if (indexes.contains(idx)) {
                  show.value = true;
                } else {
                  show.value = false;
                }
              },
            ),
            ValueListenableBuilder<bool>(
              valueListenable: show,
              builder: (_, value, child) {
                return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
              },
            )
          ],
        ));
  }

  _daonguockhangdongUI() {
    ValueNotifier<bool> prothrombin = ValueNotifier<bool>(false);
    ValueNotifier<bool> pcc = ValueNotifier<bool>(false);
    ValueNotifier<bool> ffp = ValueNotifier<bool>(false);

    ValueNotifier<bool> unf = ValueNotifier<bool>(false);
    ValueNotifier<bool> rlcn = ValueNotifier<bool>(false);

    return Container(
      child: ValueListenableBuilder<bool>(
        valueListenable: _daonguockhangdong,
        builder: (_, value, child) {
          return value
              ? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      QuestionMultipleWidget(
                        width: 500,
                        optionWidth: 800,
                        questionItem: QuestionItem(
                            title: 'Warfarin (Mục đích: Đưa INR về mức bình thường bằng PCC hoặc Plasma tươi Và Vit K)',
                            options: [
                              'Vit K 10 mg IV, lập lại 3 lần/ngày  (khởi phát chậm 2 giờ và cần 24h để đạt đỉnh nếu CN gan giảm)',
                              'Plasma tươi đông lạnh 15ml/kg',
                              'Prothrombin complex concentrate 15-50 U/kg ((phụ thuộc cân nặng, INR và loại sản phẩm) (PCC – 3 YT: II,IX, X; 4 YT: II, VII, IX, X; Activated PCC: 4 YT – nhanh: 25 phút so với FFP 30-120 phút, không cần xét nghiệm, thử phản ứng chéo, ít nguy cơ nhiểm trùng, V nho – quan trọng ở BN nguy cơ phù phổi, suy tim; nguy cơ DIC))',
                              'Cần PTTK',
                              'Vit K    +    ',
                              'PCC',
                              'FFP',
                              'Và rFVIIa'
                            ]),
                        callback: (idx, select, indexes) {
                          if (indexes.contains(2)) {
                            prothrombin.value = true;
                          } else {
                            prothrombin.value = false;
                          }

                          if (indexes.contains(5)) {
                            pcc.value = true;
                          } else {
                            pcc.value = false;
                          }

                          if (indexes.contains(6)) {
                            ffp.value = true;
                          } else {
                            ffp.value = false;
                          }
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 40),
                        child: Row(
                          children: [
                            ValueListenableBuilder<bool>(
                              valueListenable: prothrombin,
                              builder: (_, value, child) {
                                return value
                                    ? QuestionTextWidget(questionItem: QuestionItem(title: 'Prothrombin'))
                                    : Container();
                              },
                            ),
                            ValueListenableBuilder<bool>(
                              valueListenable: pcc,
                              builder: (_, value, child) {
                                return value
                                    ? QuestionTextWidget(questionItem: QuestionItem(title: 'PCC'))
                                    : Container();
                              },
                            ),
                            ValueListenableBuilder<bool>(
                              valueListenable: ffp,
                              builder: (_, value, child) {
                                return value
                                    ? QuestionTextWidget(questionItem: QuestionItem(title: 'FFP'))
                                    : Container();
                              },
                            )
                          ],
                        ),
                      ),
                      QuestionMultipleWidget(
                        optionWidth: 600,
                        questionItem: QuestionItem(
                            title: 'Unfractionated hoặc LMWH',
                            options: ['Protamine sulfate (1mg/100 units Heparin hoặc 1 mg Enoxaparin)']),
                        callback: (idx, select, indexes) {
                          if (indexes.contains(idx)) {
                            unf.value = true;
                          } else {
                            unf.value = false;
                          }
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 40),
                        child: ValueListenableBuilder<bool>(
                          valueListenable: unf,
                          builder: (_, value, child) {
                            return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
                          },
                        ),
                      ),
                      QuestionMultipleWidget(
                        optionWidth: 600,
                        questionItem: QuestionItem(
                            title: 'RL chức năng TC', options: ['Truyền TC (4 – 8 Units, đạt TC > 100.000)']),
                        callback: (idx, select, indexes) {
                          if (indexes.contains(idx)) {
                            rlcn.value = true;
                          } else {
                            rlcn.value = false;
                          }
                        },
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 40),
                        child: ValueListenableBuilder<bool>(
                          valueListenable: rlcn,
                          builder: (_, value, child) {
                            return value ? QuestionTextWidget(questionItem: QuestionItem(title: '')) : Container();
                          },
                        ),
                      ),
                      Container(
                        margin: EdgeInsets.only(left: 10),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('DOAC Direct oral anticoagulants – apixaban, edoxaban, dabigatran, rivaroxaban'),
                            QuestionCheckShowMultipleWidget(optionWidth: 600, title: 'Specific antidotes', options: [
                              'Dabigatran – Direct thrombin inhibitor:  Idarucizumab (2,5g/50ml) 5g IV (vài phút)',
                              'Apixaban, edoxaban, rivaroxaban – Factor Xa inhibitors:  Andexanet alpha 400-800 mg IV bolus, sau đó 4-8 mg/p PIV đến 120 phút (hiệu quả trong 2 phút)'
                            ]),
                            QuestionCheckShowMultipleWidget(
                              optionWidth: 600,
                              title: 'Đảo ngược không chuyên biệt Direct thrombin inhibitor',
                              options: [
                                'Activated charcoal (thay thế Idarucizumab, trong vòng 3-4h sau Dabigatran -ngăn hấp thu)',
                                'Hemodialysis (loại bỏ phần không gắn Protein – chiếm 2/3)',
                                'Factor eight inhibitor bypassing agent (FEIBA)'
                              ],
                            ),
                            QuestionCheckShowMultipleWidget(
                              optionWidth: 600,
                              title: 'Đảo ngược không chuyên biệt Factor Xa inhibitors',
                              options: [
                                'PCC (đảo ngược không hoàn toàn)',
                                'Factor eight inhibitor bypassing agent (FEIBA)',
                              ],
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                )
              : Container();
        },
      ),
    );
  }
}
