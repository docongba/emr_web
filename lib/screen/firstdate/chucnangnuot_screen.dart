import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class ChucNangNuotScreen extends StatefulWidget {

  VoidCallback callback;
  ChucNangNuotScreen({this.callback});
  @override
  State createState() {
    return _ChucNangNuotScreenState();
  }
}

class _ChucNangNuotScreenState extends State<ChucNangNuotScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(16),
          width: MediaQuery.of(context).size.width,
          decoration: containerDecoration(borderColor: Colors.blue),
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              QuestionYNShowMultipleWidget(
                title: 'Test nuốt',
                mainOptions: ['Thực hiện',
                'Không thực hiện']
              ),

              QuestionYNShowMultipleWidget(
                  title: 'Kết quả:',
                  mainOptions: ['Đạt',
                    'Không đạt',]
              ),

              QuestionDateWidget(questionItem: QuestionItem(title: 'Thời điểm'))
            ],
          ),

        ),
      ),
    );
  }

}
