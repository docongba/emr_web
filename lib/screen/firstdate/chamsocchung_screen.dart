import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class ChamSocChungScreen extends StatefulWidget {

  VoidCallback callback;
  ChamSocChungScreen({this.callback});
  @override
  State createState() {
    return _ChamSocChungScreenState();
  }
}

class _ChamSocChungScreenState extends State<ChamSocChungScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.all(16),
          width: MediaQuery.of(context).size.width,
          decoration: containerDecoration(borderColor: Colors.blue),
          padding: EdgeInsets.symmetric(horizontal: 12, vertical: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              QuestionMultipleWidget(
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: 'Test nuốt',
                    options: [
                      'Thực hiện',
                      'Không thực hiện',
                      'Giảm chức năng tim',
                    ],
                    id: "1006"),
                callback: (index, select, indexes) {
                  print("SHOW SELECTED INDEX $index");
                },
              ),

              QuestionMultipleWidget(
                questionItem: QuestionItem(
                    type: QuestionType.MULTIPLE_CHOICE,
                    title: 'Kết quả:',
                    options: [
                      'Đạt',
                      'Không đạt',
                    ],
                    id: "1006"),
                callback: (index, select, indexes) {
                  print("SHOW SELECTED INDEX $index");
                },
              ),
            ],
          ),

        ),
      ),
    );
  }

}
