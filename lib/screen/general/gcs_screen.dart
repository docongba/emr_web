import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class GCSScreen extends StatefulWidget {
  GCSScreen();

  @override
  State createState() {
    return _GCSScreenState();
  }
}

class _GCSScreenState extends State<GCSScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        margin: EdgeInsets.all(10),
        padding: EdgeInsets.all(10),
        decoration: containerDecoration(),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [

            QuestionMultipleWidget(
              horizontal: false,
              optionWidth: 250,
              questionItem: QuestionItem(title: '', options: [
                '0- GCS:13–15',
                '1- GCS:5–12',
                '2- GCS:3–4',
              ]),
            )
          ],
        ),
      ),
    );
  }
}
