import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_date_widget.dart';
import 'package:flutter_app/widget/question_multiple_widget.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/question_yn_show_multiple_widget.dart';
import 'package:flutter_app/widget/section_widget.dart';
import 'package:flutter_app/widget/style.dart';

class NIHSSScreen extends StatefulWidget {
  NIHSSScreen();

  @override
  State createState() {
    return _NIHSSScreenState();
  }
}

class _NIHSSScreenState extends State<NIHSSScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: containerDecoration(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        SectionWidget(
                          title: 'LOC',
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Tỉnh táo và đáp ứng tốt',
                                  '1 – Không tỉnh, cần kích thích nhẹ',
                                  '2 – Không tỉnh, cần kích thích đau',
                                  '3 – Chỉ đáp ứng phản xạ',
                                ]),
                              ),
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Trả lời đúng cả hai câu',
                                  '1 – Trả lời đúng một câu',
                                  '2 – Không đúng câu nào',
                                ]),
                              ),
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Thực hiện đúng hai y lệnh',
                                  '1 – Thực hiện đúng 1 y lệnh',
                                  '2 – Không thực hiện cả hai y lệnh',
                                ]),
                              ),
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: containerDecoration(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Vẫn nhãn',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Bình thường',
                                  '1 – Liệt vận nhãn một phần',
                                  '2 – Nhìn về 1 bên hoặc liệt hoàn toàn (PX mắt búp bê -)',
                                ]),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Thị trường',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Không mất thị trường',
                                  '1 – Bán manh một phần',
                                  '2 – Bán manh hoàn toàn',
                                  '3 – Bán manh hai bên hoặc mù'
                                ]),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Liệt mặt',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – không liệt',
                                  '1 – Yếu nhẹ',
                                  '2 – Liệt một phần ( phần dưới của mặt )',
                                  '3 – Liệt hoàn toàn'
                                ]),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: containerDecoration(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Thất điều chi',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Không',
                                  '1 – Thất điều ở chi trên hoặc chi dưới',
                                  '2 – Thất điều ở chi trên và chi dướ',
                                ]),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Cảm giác ',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Bình thường',
                                  '1 – Mất cảm giác từ nhẹ/trung bình ở một bên',
                                  '2 – Mất cảm giác hoàn toàn',
                                ]),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Ngôn ngữ',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Bình thường',
                                  '1 – Mất ngôn ngữ nhẹ / trung bình',
                                  '2 – Mất ngôn ngữ nghiêm trọng',
                                  '3 – Mất ngôn ngữ toàn bộ / Hôn mê'
                                ]),
                              ),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              padding: EdgeInsets.all(10),
              decoration: containerDecoration(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Dysarthria',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Bình thường',
                                  '1 – Nhẹ / Trung bình',
                                  '2 – Nặng (không hiểu được)',
                                  '3 – Nội khí quản'
                                ]),
                              ),
                            ),
                          ],
                        ),
                        Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            SectionWidget(
                              title: 'Thờ ơ, mất chú ý',
                            ),
                            Container(
                              width: 250,
                              child: QuestionMultipleWidget(
                                horizontal: false,
                                optionWidth: 250,
                                questionItem: QuestionItem(title: '', options: [
                                  '0 – Bình thường',
                                  '1 – 1 phần (bên ảnh hưởng có nhận biết nhưng không hoàn toàn so đối bên)',
                                  '2 – nặng',
                                ]),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.all(10),
              decoration: containerDecoration(),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.all(10),
                    decoration: containerDecoration(),
                    child: Row(
                      children: [
                        Container(
                          child: Text('Gốc chi'),
                          width: 80,
                          padding: EdgeInsets.all(8),
                        ),
                        Column(
                          children: [
                            QuestionMultipleWidget(
                                optionWidth: 130,
                                questionItem: QuestionItem(title: 'Tay trái', options: [
                                  '0 – Bình thường',
                                  '1 – Rơi chậm',
                                  '2 – Rơi nhanh',
                                  '3 – 1–2/5 (sức cơ)',
                                  '4 – liệt hoàn toàn'
                                ])),
                            QuestionMultipleWidget(
                                optionWidth: 130,
                                questionItem: QuestionItem(title: 'Chân trái', options: [
                                  '0 – Bình thường',
                                  '1 – Rơi chậm',
                                  '2 – Rơi nhanh',
                                  '3 – 1–2/5 (sức cơ)',
                                  '4 – liệt hoàn toàn'
                                ])),
                            QuestionMultipleWidget(
                                optionWidth: 130,
                                questionItem: QuestionItem(title: 'Tay phải', options: [
                                  '0 – Bình thường',
                                  '1 – Rơi chậm',
                                  '2 – Rơi nhanh',
                                  '3 – 1–2/5 (sức cơ)',
                                  '4 – liệt hoàn toàn'
                                ])),
                            QuestionMultipleWidget(
                                optionWidth: 130,
                                questionItem: QuestionItem(title: 'Chân trái', options: [
                                  '0 – Bình thường',
                                  '1 – Rơi chậm',
                                  '2 – Rơi nhanh',
                                  '3 – 1–2/5 (sức cơ)',
                                  '4 – liệt hoàn toàn'
                                ])),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Container(
                    decoration: containerDecoration(),
                    padding: EdgeInsets.all(10),
                    child: Row(
                      children: [
                        Container(
                          child: Text('Ngọn chi'),
                          width: 80,
                          padding: EdgeInsets.all(8),
                        ),
                        Column(
                          children: [
                            QuestionMultipleWidget(
                                optionWidth: 220,
                                questionItem: QuestionItem(title: 'Tay trái', options: [
                                  '0 – Bình thường (không gập > 5”)',
                                  '1 – còn duỗi 1 ít sau 5”',
                                  '2 – không còn duỗi sau 5”',
                                ])),
                            QuestionMultipleWidget(
                                optionWidth: 220,
                                questionItem: QuestionItem(title: 'Chân trái', options: [
                                  '0 – Bình thường (không gập > 5”)',
                                  '1 – còn duỗi 1 ít sau 5”',
                                  '2 – không còn duỗi sau 5”',
                                ])),
                            QuestionMultipleWidget(
                                optionWidth: 220,
                                questionItem: QuestionItem(title: 'Tay phải', options: [
                                  '0 – Bình thường (không gập > 5”)',
                                  '1 – còn duỗi 1 ít sau 5”',
                                  '2 – không còn duỗi sau 5”',
                                ])),
                            QuestionMultipleWidget(
                                optionWidth: 220,
                                questionItem: QuestionItem(title: 'Chân trái', options: [
                                  '0 – Bình thường (không gập > 5”)',
                                  '1 – còn duỗi 1 ít sau 5”',
                                  '2 – không còn duỗi sau 5”',
                                ])),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
