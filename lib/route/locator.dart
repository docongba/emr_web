import 'dart:io';

import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

void setupLocator(){
  locator.registerSingleton<GlobalKey<NavigatorState>>(GlobalKey(debugLabel: "Main Navigator"));
//  locator.registerFactory(() => DioHttpProvider());
//
//   AUTH
//  locator.registerFactory(() => AuthProvider());
//  locator.registerFactory(() => AuthRepo());
//  locator.registerFactory(() => LoginBloc());
//  locator.registerFactory(() => ForgotPasswordBloc());
//  locator.registerFactory(() => HomeBloc());
//  locator.registerFactory(() => HomeProvider());
//  locator.registerFactory(() => InspectionsBloc());
//  locator.registerFactory(() => InspectionDetailBloc());
//  locator.registerFactory(() => MyDayBloc());
//  locator.registerFactory(() => InspectionProvider());
//  locator.registerFactory(() => BookResultBloc());
//  locator.registerFactory(() => UpdateUserBloc());
//  locator.registerFactory(() => BookResultSuccessBloc());
//  locator.registerFactory(() => BookResultFailBloc());
//  locator.registerFactory(() => BookResultRemainsBloc());
//  locator.registerFactory(() => ConversationsBloc());
//  locator.registerFactory(() => ConversationsProvider());
//  locator.registerFactory(() => ChatsBloc());
//  locator.registerFactory(() => PickDateSlotBloc());
//  locator.registerFactory(() => InspectionsFilterBloc());
//  locator.registerFactory(() => TaggingBloc());
//  locator.registerFactory(() => TaggingProvider());
//  locator.registerFactory(() => OverviewTaggingBloc());
//  locator.registerFactory(() => MapsBloc());
//  locator.registerFactory(() => InspectionCancelBloc());
//  locator.registerFactory(() => RegisterBloc());
//  locator.registerFactory(() => CaseManagerOptionBloc());
//  locator.registerFactory(() => RelationshipBloc());
//  locator.registerFactory(() => RelationshipRoleDetailBloc());
//  locator.registerFactory(() => RelationshipProvider());
//  locator.registerFactory(() => RelationshipApplicantBloc());
//  locator.registerFactory(() => RelationshipLandlordDetailBloc());
//  locator.registerFactory(() => RelationshipHomeBloc());
//  locator.registerFactory(() => RelationshipUsersBloc());
//  locator.registerFactory(() => RelationshipBusinessBloc());
}