
import 'package:flutter/material.dart';
import 'package:flutter_app/screen/home_screen.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {

      //BOB APP <S>
      case RoutePaths.homeScreen:
        return MaterialPageRoute(
            settings: settings,
            builder: (_) => HomeScreen()
        );
        break;

      default:
        return null;
    }
  }
}

class RoutePaths {
  static const String login = '/login';

  static const String homeScreen = '/homeScreen';


  static const String forgotPasswordScreen = '/forgotPasswordScreen';

  static const String inspectionsScreen = '/inspectionsScreen';

  static const String inspectionDetailScreen = '/inspectionDetailScreen';

  static const String myDayScreen = '/myDayScreen';

  static const String bookResultScreen = '/bookResultScreen';

  static const String bookResultSuccessScreen = '/bookResultSuccessScreen';

  static const String bookResultFailScreen = '/bookResultFailScreen';

  static const String bookInspectionFailScreen = '/bookInspectionFailScreen';

  static const String bookResultRemainScreen = '/bookResultRemainScreen';

  static const String bookResultRemainDoneScreen = '/bookResultRemainDoneScreen';

  static const String bookResultFailDoneScreen = '/bookResultFailDoneScreen';

  static const String conversationsScreen = '/conversationsScreen';

  static const String chatScreen = '/chatScreen';

  static const String filterScreen = '/filterScreen';

  static const String taggingOverviewScreen = '/taggingOverviewScreen';

  static const String cancelInspection = '/cancelInspectionScreen';

  static const String cancelInspectionDone = '/cancelInspectionDoneScreen';


  //Bob.AI
  static const String caseManagerOptionScreen = '/caseManagerOptionScreen';

  static const String registerScreen = '/register';

  static const String homeBobAIScreen = '/home_bobai';

  static const String chatListScreen = '/chat_list';

  static const String affordabilityScreen = '/affordability';

  static const String preferencesScreen = '/preferences';

  static const String rentalScreen = '/rental';

  static const String virtualInspectionScreen = '/virtual_inspections';

  static const String homeBobScreen = '/homeBobScreen';

  static const String showingScreen = '/showing';

  static const String applicationDashboard = '/applicant_dashboard';

  static const String fagScreen = '/faq';

  static const String relationShipRoleScreen = '/relationshipRoleScreen';

  static const String relationshipRoleDetailScreen = '/relationshipRoleDetailScreen';

  static const String relationshipApplicantScreen = '/relationshipApplicantScreen';

  static const String relationshipLandlordScreen = '/relationshipLandlordDetailScreen';

  static const String relationshipHomeScreen = '/relationshipHomeScreen';

  static const String relationshipUsersScreen = '/RelationshipUsersScreen';

  static const String relationshipPartnerUserDetailScreen = '/relationshipPartnerUserDetailScreen';

  static const String relationshipAddPartnerUserScreen = '/relationshipAddPartnerUserScreen';

  static const String relationshipBusinessScreen = '/relationshipBusinessScreen';

  static const String relationshipEditPartnerUser = '/RelationshipPartnerUserDetailScreen';
}

