import 'package:flutter/material.dart';
import 'package:flutter_app/route/locator.dart';
import 'package:flutter_app/route/route.dart';

class AppNavigator {
  GlobalKey<NavigatorState> _globalKey = locator<GlobalKey<NavigatorState>>();

  void pop() {
    _globalKey.currentState.pop();
  }

  void moveToHome() {
    _globalKey.currentState.pushNamedAndRemoveUntil(RoutePaths.homeScreen, (route) => false);
  }
}

