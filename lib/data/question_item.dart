class QuestionType{
  static const String SINGLE_CHOICE = 'singleChoice';
  static const String MULTIPLE_CHOICE = 'multipleChoice';
  static const String TEXT = 'singleChoice';
  static const String DATE = 'date';
  static const String BUTTON = 'button';
}

class QuestionItem{
  final String answer;
  final String title;
  final String type;
  final List<String> options;
  final String id;
  final bool isShowing;

  List<QuestionItem> childs = [];

  QuestionItem({this.type, this.title, this.id, this.options, this.answer = '', this.isShowing = true, this.childs});
}