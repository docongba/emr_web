class
AppString {
  // BUTTOn
  static final String btnRest = 'Reset';
  static final String btnCancel = 'Cancel';
  static final String btnNo = 'No';
  static final String btnClose = 'Close';
  static final String btnOk = 'Ok';
  static final String btnVerifyNow = 'Set Up Now';
  static final String btnYes = 'Yes';
  static final String titleDanhGia = 'Đánh giá';
  static final String titleDauHieuSinhTon = 'Dấu hiệu sinh tồn';
  static final String titleNhap = 'Nhập';
  static final String titleHoHp = 'Hô hấp';
  static final String titleTongQuat  = 'Tổng quát ';
  static final String titleHoHTTK = 'Hỗ trợ thông khí';
  static final String titleHoNKQ = 'Nội khí quản';
  static final String titleTuanHoan = 'Tuần hoàn';
  static final String titleHATB = 'HATB';
  static final String titleKhac = 'Khác';
  static final String titleMach= 'Mạch';
  static final String titleDontTu= 'Đồng tử ';
  static final String titleVanNhan= 'Vận nhãn  ';
  static final String titleDauMangNao= 'Dấu màng não  ';
  static final String titleYeuLietChi= 'Yếu liệt chi   ';
  static final String titleTeRLCamGiac = 'Tê, RL Cảm giác   ';
  static final String titlePXGC = 'PXGC ';
  static final String titlePXThap = 'PX tháp';
  static final String titleThatDieu = 'Thất điều';
  static final String titleVitritonthuong = 'Vị trí tổn thương ';
  static final String titleBienChung = 'Biến chứng ';
  static final String titleToanThan = 'Toàn thân  ';
  static final String titleDauMatCo = 'Đầu mặt cổ   ';
  static final String titleLongNguc = 'Lồng ngực ';
  static final String titleTim = 'Tim ';
  static final String titlePhoi = 'Phổi ';
  static final String titleTrungThat = 'Trung thất ';
  static final String titleBung = 'Bụng ';
//  static final String titleKhac = 'Khác ';
}

