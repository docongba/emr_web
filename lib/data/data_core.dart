import 'package:flutter_app/model/menu_item.dart';

class DataCore{

  static DataCore _instance = new DataCore.internal();

  DataCore.internal();

  factory DataCore() => _instance;


  List<MenuItem> get1MenuItems(){
    List<MenuItem> menuDatas = [];
    var item = MenuItem();
    item.title = 'Chăm sóc đầu tiên tại đơn vị đột quỵ';
    item.isSection = true;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Đánh giá ';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Thăm khám';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Cận lâm sàng';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Đánh giá và chăm sóc chức năng nuốt';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Chăm sóc chung và tổng quát';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Chăm sóc chuyên biệt';
    item.isSpecial = true;
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Điều trị tối cấp/cấp';
    item.isSpecial = true;
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Chăm sóc ngày 2 - 7 tại đơn vị đột quỵ';
    item.isSection = true;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Tình trạng thần kinh ';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Tình trạng nội ';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Cân bằng xuất ';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'HA được chăm sóc xuất nhập';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'RLĐM được kiểm soát';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Có rối loạn chức năng nuốt';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Bệnh nhân có cảm thấy thoải mái';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Cơ chế bệnh sinh và nguyên nhân XHN';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Biến chứng nội khoa, chăm sóc';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Thúc đẩy sự hồi phục';
    item.isSection = false;
    menuDatas.add(item);

    item = MenuItem();
    item.title = 'Kế hoạch chăm sóc, xuất viện';
    item.isSection = false;
    menuDatas.add(item);

    return menuDatas;
  }
}