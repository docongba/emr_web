import 'package:flutter_app/data/question_item.dart';

class DataFirstDate {

  static DataFirstDate _instance = new DataFirstDate.internal();

  DataFirstDate.internal();

  factory DataFirstDate() => _instance;

  List<QuestionItem> pxgcQuestionSet(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Tay: nhị đầu +/-', options: [], id: "1001"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Tay: tam đầu +/-', options: [], id: "1002"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Tay: Cánh tay quay +/-', options: [], id: "1002"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Chân: Gối +/-', options: [], id: "1002"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Chân: gót +/-', options: [], id: "1002"));

    return questions;
  }

  List<String> ctmStore = ['', '', '', ''];

  List<QuestionItem> thamKhamPointsQuestionSet(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'GCS (SUẢ LẠI) - ĐIỂM TỔNG VÀ FORMAT EVM', options: [], id: "1001"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'NIHSS', options: [], id: "1002"));

    return questions;
  }

  List<QuestionItem> danhgiaQuestionSet(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Mạch', options: [], id: "1001"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'HATTh', options: [], id: "1002"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'HATT', options: [], id: "1003"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Nhịp thở', options: [], id: "1004"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Nhiệt độ', options: [], id: "1005"));

    return questions;
  }

  List<QuestionItem> hoHap1QuestionSet(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'SpO2', options: [], id: "1007"));
//    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Kiểu thở', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> canlamsan(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'HC M', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Hb g/', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'BC (N % L %)', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'TC', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> vss(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'H1', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'H2', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> kmdb(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'pH', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'paO2', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'paO2', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'HCO3', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: '∆(A-a)O2', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> sgot(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'SGOT', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'SGOT', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'GGT', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'NH3', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Bilirubin', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'TT', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'GT', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> bun(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'BUN mg%', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Creatinine mg%', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'eGFR', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> ckmb(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'CKMB', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Troponin', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> btDmtb(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'INR', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'aPTT', options: [], id: "1008"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'PT', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> mentim(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'CKMB', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Troponin', options: [], id: "1008"));

    return questions;
  }

  List<QuestionItem> glucose(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'mg%', options: [], id: "1007"));

    return questions;
  }

  List<QuestionItem> diengiaido(){

    List<QuestionItem> questions = [];

    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Na', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'K', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Ca', options: [], id: "1007"));
    questions.add(QuestionItem(type: QuestionType.TEXT, title: 'Mg', options: [], id: "1007"));

    return questions;
  }
}

