import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/colors.dart';
import 'package:flutter_app/route/locator.dart';
import 'package:flutter_app/screen/home_screen.dart';
import 'route/route.dart' as router;
void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      navigatorKey: locator<GlobalKey<NavigatorState>>(),
      title: 'EMR',
      onGenerateRoute: router.Router.generateRoute,
      home: HomeScreen(),
      theme: ThemeData(
        primaryColor: primaryColor,
      )
    );
  }
}
