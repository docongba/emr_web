import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionYNShowMultipleWidget extends StatefulWidget {
  final Function(int) callbackYN;
  final Function(List<int>) callBackChildOption;
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final String subTitle;
  final List<String> mainOptions; // = [AppString.btnYes, AppString.btnNo];
  final List<String> extraOptions;
  final int showIndex;
  final bool horizontal;

  QuestionYNShowMultipleWidget(
      {this.showIndex = 0,
      this.mainOptions,
      this.horizontal = true,
      this.subTitle = '',
      this.extraOptions,
      this.title = '',
      this.callbackYN,
      this.width = 120,
      this.optionWidth = 100,
      this.callBackChildOption,
      this.hasDecor = false});

  @override
  State createState() {
    return QuestionYNShowMultipleWidgetState();
  }
}

class QuestionYNShowMultipleWidgetState extends State<QuestionYNShowMultipleWidget> {
  final List<int> selectedIndexs = [];

  ValueNotifier<int> _yesno = ValueNotifier<int>(-1);

  List<String> mainOptions = [];
  List<String> extraOptions = [];
  int showIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.mainOptions == null || widget.mainOptions.length == 0) {
      mainOptions = ['Có', 'Không'];
    } else {
      mainOptions = widget.mainOptions;
    }

    if(widget.extraOptions == null || widget.extraOptions.length == 0){
      extraOptions = [];
    }

    showIndex = widget.showIndex;
  }

  int selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: widget.hasDecor ? containerDecoration() : BoxDecoration(),
      constraints: BoxConstraints(
        minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SelectableText(widget.title),
          ),
          widget.horizontal
              ? Wrap(
                  children: List.generate(mainOptions.length, (index) {
                    return GestureDetector(
                      onTap: () {
                        setState(() {
                          selectedIndex = index;
                          if (widget.callbackYN != null) {
                            widget.callbackYN(index);
                          }
                          _yesno.value = index;
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 20),
                        child: Row(
                          children: [
                            selectedIndex == index
                                ? Icon(
                                    Icons.radio_button_checked,
                                    size: checkboxSize,
                                  )
                                : Icon(
                                    Icons.radio_button_unchecked,
                                    size: checkboxSize,
                                  ),
                            SizedBox(
                              width: 4,
                            ),
                            SelectableText(
                              mainOptions[index],
                              style: TextStyle(fontSize: 11),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                )
              : Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: List.generate(mainOptions.length, (index) {
                    return FlatButton(
                      onPressed: () {
                        setState(() {
                          selectedIndex = index;
                          if (widget.callbackYN != null) {
                            widget.callbackYN(index);
                          }
                          _yesno.value = index;
                        });
                      },
                      child: Container(
                        margin: EdgeInsets.only(right: 20),
                        child: Row(
                          children: [
                            selectedIndex == index
                                ? Icon(
                                    Icons.radio_button_checked,
                                    size: checkboxSize,
                                  )
                                : Icon(
                                    Icons.radio_button_unchecked,
                                    size: checkboxSize,
                                  ),
                            SizedBox(
                              width: 4,
                            ),
                            SelectableText(
                              mainOptions[index],
                              style: TextStyle(fontSize: 11),
                            ),
                          ],
                        ),
                      ),
                    );
                  }),
                ),
          ValueListenableBuilder<int>(
            valueListenable: _yesno,
            builder: (_, value, child) {
              return value == showIndex
                  ? Container(
                      margin: widget.extraOptions != null
                          ? EdgeInsets.only(left: 40, top: 8, bottom: 8)
                          : EdgeInsets.all(0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          widget.subTitle.isNotEmpty
                              ? Container(
                                  child: SelectableText(widget.subTitle),
                                  padding: EdgeInsets.symmetric(vertical: 6),
                                )
                              : Container(),
                          widget.extraOptions != null
                              ? widget.horizontal
                                  ? Wrap(
                                      children: List.generate(widget.extraOptions.length, (index) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (selectedIndexs.contains(index)) {
                                                selectedIndexs.remove(index);
                                              } else {
                                                selectedIndexs.add(index);
                                              }

                                              if (widget.callBackChildOption != null) {
                                                widget.callBackChildOption(selectedIndexs);
                                              }
                                            });
                                          },
                                          child: Container(
                                            width: widget.optionWidth,
                                            margin: EdgeInsets.only(right: 20),
                                            child: Row(
                                              children: [
                                                selectedIndexs.contains(index)
                                                    ? Icon(
                                                        Icons.check_box,
                                                        size: checkboxSize,
                                                      )
                                                    : Icon(
                                                        Icons.check_box_outline_blank,
                                                        size: checkboxSize,
                                                      ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Expanded(
                                                    child: SelectableText(
                                                  widget.extraOptions[index],
                                                  style: TextStyle(fontSize: 11),
                                                )),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                    )
                                  : Column(
                                      children: List.generate(widget.extraOptions.length, (index) {
                                        return GestureDetector(
                                          onTap: () {
                                            setState(() {
                                              if (selectedIndexs.contains(index)) {
                                                selectedIndexs.remove(index);
                                              } else {
                                                selectedIndexs.add(index);
                                              }

                                              if (widget.callBackChildOption != null) {
                                                widget.callBackChildOption(selectedIndexs);
                                              }
                                            });
                                          },
                                          child: Container(
                                            width: widget.optionWidth,
                                            margin: EdgeInsets.only(right: 20),
                                            child: Row(
                                              children: [
                                                selectedIndexs.contains(index)
                                                    ? Icon(
                                                        Icons.check_box,
                                                        size: checkboxSize,
                                                      )
                                                    : Icon(
                                                        Icons.check_box_outline_blank,
                                                        size: checkboxSize,
                                                      ),
                                                SizedBox(
                                                  width: 4,
                                                ),
                                                Expanded(
                                                    child: SelectableText(
                                                  widget.extraOptions[index],
                                                  style: TextStyle(fontSize: 11),
                                                )),
                                              ],
                                            ),
                                          ),
                                        );
                                      }),
                                    )
                              : Container(),
                        ],
                      ),
                    )
                  : Container();
            },
          )
        ],
      ),
    );
  }
}
