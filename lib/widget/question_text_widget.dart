import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionTextWidget extends StatelessWidget {
  final Function(String) callback;
  final QuestionItem questionItem;
  final String answer;

  QuestionTextWidget({@required this.questionItem, this.answer, this.callback});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      constraints: BoxConstraints(minWidth: 120, maxWidth: 150),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          questionItem.title.isNotEmpty? SelectableText(
            questionItem.title,
            style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),
          ):Container(),
          TextFormField(

            style: TextStyle(fontSize: 12, color: Colors.black87),
            initialValue: questionItem.answer,
            decoration: customInputDecoration(error: '', hint: AppString.titleNhap),
            onChanged: (value) {
              callback(value);
            },
          )
        ],
      ),
    );
  }
}
