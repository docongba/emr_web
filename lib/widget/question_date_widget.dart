import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';
import 'package:intl/intl.dart';

class QuestionDateWidget extends StatelessWidget{

  final Function(String) callback;
  final QuestionItem questionItem;
  QuestionDateWidget({@required this.questionItem, this.callback});

  @override
  Widget build(BuildContext context) {

    final DateTime now = DateTime.now();
    final DateFormat formatter = DateFormat('dd-MM-yyyy HH:mm');
    final String formatted = formatter.format(now);

    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      constraints: BoxConstraints(
        minWidth: 120,
          maxWidth: 150
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SelectableText(questionItem.title, style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),),

          TextFormField(
            style: TextStyle(fontSize: 11, color: Colors.black87),
            initialValue: formatted,
            decoration: customInputDecoration(error: '', hint: AppString.titleNhap),
            onChanged: (value){
              callback(value);
            },
          )
        ],
      ),

    );
  }
}