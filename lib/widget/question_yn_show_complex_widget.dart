import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionYNShowComplexWidget extends StatefulWidget{

  final Function(int, List<int>) callback;
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final String subTitle;
  final List<String> questionComplexTypes;


  QuestionYNShowComplexWidget({this.subTitle = '', this.questionComplexTypes, this.title, this.callback, this.width = 120, this.optionWidth = 100, this.hasDecor = false});

  @override
  State createState() {
    return _QuestionYNShowComplexWidgetState();
  }
}

class _QuestionYNShowComplexWidgetState extends State<QuestionYNShowComplexWidget>{

  final List<int> selectedIndexs = [];

  ValueNotifier<bool> _parentCheck = ValueNotifier<bool>(false);
  ValueNotifier<int> _yesno = ValueNotifier<int>(-1);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

  }

  int selectedIndex = -1;

  List<String> yesno = ['Có', 'Không'];

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: widget.hasDecor? containerDecoration(): BoxDecoration(),
      constraints: BoxConstraints(
          minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
//          QuestionSingleWidget(questionItem: null),

        Container(
          child: Text(widget.title),
        ),
          Row(
            children: List.generate(yesno.length, (index) {
              return GestureDetector(
                onTap: (){
                  setState(() {
                    selectedIndex = index;
                    _yesno.value = index;
                  });
                },
                child: Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Row(
                    children: [
                      selectedIndex == index? Icon(Icons.check_box, size: checkboxSize,): Icon(Icons.check_box_outline_blank, size: checkboxSize,),
                      SizedBox(width: 4,),
                      SelectableText(yesno[index], style: TextStyle(fontSize: 11),),
                    ],
                  ),
                ),
              );
            }),
          ),

          ValueListenableBuilder<int>(
            valueListenable: _yesno,
            builder: (_, value, child){
              return value == 0? Container(
                margin: EdgeInsets.only(left: 40, top: 8, bottom: 8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    widget.subTitle.isNotEmpty? Container(

                      child: Text(widget.subTitle),
                      padding: EdgeInsets.symmetric(vertical: 6),
                    ): Container(),
                    Wrap(
                      children: List.generate(widget.questionComplexTypes.length, (index) {
                        var questionType = widget.questionComplexTypes[index];
                        if(questionType == QuestionType.MULTIPLE_CHOICE){
                          return  GestureDetector(
                            onTap: (){

                              setState(() {
                                if(selectedIndexs.contains(index)){
                                  selectedIndexs.remove(index);
                                }else{
                                  selectedIndexs.add(index);
                                }
                              });

                              widget.callback(index, selectedIndexs);
                            },
                            child: Container(
                              width: widget.optionWidth,
                              margin: EdgeInsets.only(right: 20),
                              child: Row(
                                children: [
                                  selectedIndexs.contains(index)? Icon(Icons.check_box, size: checkboxSize,): Icon(Icons.check_box_outline_blank, size: checkboxSize,),
                                  SizedBox(width: 4,),
                                  Expanded(child: SelectableText(widget.questionComplexTypes[index], style: TextStyle(fontSize: 11),)),
                                ],
                              ),
                            ),
                          );
                        }else if(questionType == QuestionType.TEXT){
                          return QuestionTextWidget(questionItem: QuestionItem(title: ''));
                        }else {
                          return Container();
                        }
                        
                      }),
                    ),
                  ],
                ),
              ):Container();
            },
          )
        ],
      ),

    );
  }
}