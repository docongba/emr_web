import 'package:flutter/material.dart';

InputDecoration customInputDecoration({String error, String hint, Widget suffixIcon}) => InputDecoration(
  labelStyle: TextStyle(fontSize: 14, color: Colors.black87),
  border: borderOutlineUnFocus(),
  enabledBorder: borderOutlineUnFocus(),
  focusedBorder: borderOutlineFocus(),
  filled: true,
  fillColor: Colors.white,
  hintStyle: TextStyle(
    color: Colors.grey,
  ),
//  errorText: error,
  hintText: hint,
  suffixIcon: suffixIcon,
  isDense: true,
);

OutlineInputBorder borderOutlineError({Color color = Colors.black87}) {
  return OutlineInputBorder(borderRadius: BorderRadius.circular(4), borderSide: BorderSide(color: color, width: 1.0));
}

OutlineInputBorder borderOutlineUnFocus({Color color = Colors.grey}) {
  return OutlineInputBorder(borderRadius: BorderRadius.circular(4), borderSide: BorderSide(color: color, width: 1.0));
}

OutlineInputBorder borderOutlineFocus({Color color = Colors.grey}) {
  return OutlineInputBorder(borderRadius: BorderRadius.circular(4), borderSide: BorderSide(color: color, width: 1.0));
}

unFocusTextField(BuildContext context) {
  return FocusScope.of(context).unfocus();
}

Decoration containerDecoration({double radius = 4.0, Color color = Colors.white, double borderSize = 1, Color borderColor = Colors.blue}) =>
    BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(radius)),
      border: Border.all(color: borderColor, width: borderSize),
      color: color,
      boxShadow: [
        new BoxShadow(
            spreadRadius: 2.1, offset: Offset(0.1, 0.1), color: Colors.black.withOpacity(0.08), blurRadius: 8)
      ],
    );

