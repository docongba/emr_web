import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/widget/style.dart';

class TableCellItem extends StatelessWidget{

  final String title;
  final double height;
  TableCellItem({this.title, this.height = 30});
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: containerDecoration(radius: 0, borderColor: Colors.blue),
        height: height,
        alignment: Alignment.centerLeft,
        child: Text(
          title,
          style: TextStyle(
            fontSize: 10,
          ),
          textAlign: TextAlign.center,
        ),
      ),
    );
  }
}