import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SaveWidget extends StatelessWidget{

  VoidCallback callback;
  SaveWidget({this.callback});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.only(top: 50),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          FlatButton(
            onPressed: () {
              callback();
            },
            child: SelectableText("Lưu"),
            color: Colors.blue,
          )
        ],
      ),
    );
  }
}