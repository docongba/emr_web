import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionMultipleWidget extends StatefulWidget{

  final Function(int, bool, List<int>) callback;
  final QuestionItem questionItem;
  final double width, optionWidth;
  final bool hasDecor;
  final bool isChild;
  final bool horizontal;

  QuestionMultipleWidget({this.horizontal = true, this.isChild = false, this.callback, @required this.questionItem, this.width = 120, this.optionWidth = 100, this.hasDecor = false});

  @override
  State createState() {
    return QuestionMultipleWidgetState();
  }
}

class QuestionMultipleWidgetState extends State<QuestionMultipleWidget>{

  final List<int> selectedIndexs = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: widget.hasDecor? containerDecoration(): BoxDecoration(),
      margin: widget.isChild? EdgeInsets.only(left: 20): EdgeInsets.symmetric(vertical: 5, horizontal: 8),
      constraints: BoxConstraints(
          minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          widget.questionItem.title.isEmpty? Container(): SelectableText(widget.questionItem.title, style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),),
          Wrap(
            children: List.generate(widget.questionItem.options.length, (index) {
              return GestureDetector(
                onTap: (){

                  var select = false;
                  setState(() {

                    if(selectedIndexs.contains(index)){
                      select = false;
                      selectedIndexs.remove(index);
                    }else{
                      selectedIndexs.add(index);
                      select = true;
                    }
                  });

                  widget.callback(index, select, selectedIndexs);
                },
                child: Container(
                width: widget.optionWidth,
                  margin: EdgeInsets.only(right: 20),
                  child: Row(
                    children: [
                      selectedIndexs.contains(index)? Icon(Icons.check_box, size: checkboxSize,): Icon(Icons.check_box_outline_blank, size: checkboxSize,),
                      SizedBox(width: 4,),
                      Expanded(child: SelectableText(widget. questionItem.options[index], style: TextStyle(fontSize: 11),)),
                    ],
                  ),
                ),
              );
            }),
          )
        ],
      ),

    );
  }
}