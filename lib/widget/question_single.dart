import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionSingle extends StatefulWidget {
  final Function(int) callback;
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final List<String> mainOptions; // = [AppString.btnYes, AppString.btnNo];
  final int showIndex;

  QuestionSingle(
      {this.showIndex = 0,
      this.mainOptions,
      this.title,
      this.callback,
      this.width = 120,
      this.optionWidth = 100,
      this.hasDecor = false});

  @override
  State createState() {
    return QuestionSingleState();
  }
}

class QuestionSingleState extends State<QuestionSingle> {

  List<String> mainOptions = [];
  int showIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if (widget.mainOptions == null || widget.mainOptions.length == 0) {
      mainOptions = ['Có', 'Không'];
    } else {
      mainOptions = widget.mainOptions;
    }

    showIndex = widget.showIndex;
  }

  int selectedIndex = -1;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: widget.hasDecor ? containerDecoration() : BoxDecoration(),
      constraints: BoxConstraints(
        minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            child: SelectableText(widget.title),
          ),
          Row(
            children: List.generate(mainOptions.length, (index) {
              return GestureDetector(
                onTap: () {
                  widget.callback(index);
                  setState(() {
                    selectedIndex = index;
                  });
                },
                child: Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Row(
                    children: [
                      selectedIndex == index
                          ? Icon(
                              Icons.radio_button_checked,
                              size: checkboxSize,
                            )
                          : Icon(
                              Icons.radio_button_unchecked,
                              size: checkboxSize,
                            ),
                      SizedBox(
                        width: 4,
                      ),
                      SelectableText(
                        mainOptions[index],
                        style: TextStyle(fontSize: 11),
                      ),
                    ],
                  ),
                ),
              );
            }),
          ),
        ],
      ),
    );
  }
}
