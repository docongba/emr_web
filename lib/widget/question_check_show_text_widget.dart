import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionCheckShowTextWidget extends StatefulWidget{

  final Function(int, List<int>) callback;
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final String inputTitle;
  final bool showParentCheck;


  QuestionCheckShowTextWidget({this.showParentCheck = true, this.title, this.inputTitle, this.callback, this.width = 120, this.optionWidth = 100, this.hasDecor = false});

  @override
  State createState() {
    return QuestionCheckShowTextWidgetState();
  }
}

class QuestionCheckShowTextWidgetState extends State<QuestionCheckShowTextWidget>{

  final List<int> selectedIndexs = [];

  ValueNotifier<bool> _parentCheck = ValueNotifier<bool>(false);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.showParentCheck == false){
      _parentCheck.value = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: widget.hasDecor? containerDecoration(): BoxDecoration(),
      constraints: BoxConstraints(
          minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: (){
              _parentCheck.value = !_parentCheck.value;
            },
            child: Row(
              children: [

                widget.showParentCheck?
                Row(
                  children: [
                    ValueListenableBuilder<bool>(
                      valueListenable: _parentCheck,
                      builder: (_, value, child){
                        return value? Icon(Icons.check_box, size: checkboxSize,): Icon(Icons.check_box_outline_blank, size: checkboxSize,);
                      },
                    ),
                    SizedBox(width: 4,),
                  ],
                ): Container(),

                widget.title.isEmpty? Container(): Expanded(child: SelectableText(widget.title, style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),)),
              ],
            ),
          ),

          Container(
            width: 150,
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
              valueListenable: _parentCheck,
              builder: (_, value, child){
                return value? Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      widget.inputTitle.isNotEmpty? SelectableText(
                        widget.inputTitle,
                        style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),
                      ):Container(),
                      TextFormField(
                        style: TextStyle(fontSize: 12, color: Colors.black87),
                        initialValue: '',
                        decoration: customInputDecoration(error: '', hint: AppString.titleNhap),
                        onChanged: (value) {
                        },
                      ),
                    ],
                  ),
                ) :Container();
              },
            ),
          )
        ],
      ),

    );
  }
}