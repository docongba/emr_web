import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionSingleWidget extends StatefulWidget{

  final Function(int) callback;
  final QuestionItem questionItem;
  final int selectedIndex;
  final horizontal;
  QuestionSingleWidget({this.callback, @required this.questionItem, this.selectedIndex = -1, this.horizontal = true});

  @override
  State createState() {
    return QuestionSingleWidgetState();
  }
}

class QuestionSingleWidgetState extends State<QuestionSingleWidget>{

  int selectedIndex = -1;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    selectedIndex = widget.selectedIndex;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
      constraints: BoxConstraints(
          minWidth: 120,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          widget.questionItem.title.isEmpty? Container(): SelectableText(widget.questionItem.title, style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),),
          widget.horizontal? Row(
            children: List.generate(widget.questionItem.options.length, (index) {
              return GestureDetector(
                onTap: (){
                  if(widget.callback != null){
                    widget.callback(index);
                  }

                  setState(() {
                    selectedIndex = index;
                  });
                },
                child: Container(
                  margin: EdgeInsets.only(right: 20),
                  child: Row(
                    children: [
                      selectedIndex == index
                          ? Icon(
                        Icons.radio_button_checked,
                        size: checkboxSize,
                      )
                          : Icon(
                        Icons.radio_button_unchecked,
                        size: checkboxSize,
                      ),
                      SizedBox(width: 4,),
                      SelectableText(widget. questionItem.options[index], style: TextStyle(fontSize: 11),),
                    ],
                  ),
                ),
              );
            }),
          ): Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: List.generate(widget.questionItem.options.length, (index) {
              return GestureDetector(
                onTap: (){
                  if(widget.callback != null){
                    widget.callback(index);
                  }

                  setState(() {
                    selectedIndex = index;
                  });
                },
                child: Container(
                  margin: EdgeInsets.only(right: 20),
//                  width: widget.optionWidth,
                  child: Row(
                    children: [
                      selectedIndex == index
                          ? Icon(
                        Icons.radio_button_checked,
                        size: checkboxSize,
                      )
                          : Icon(
                        Icons.radio_button_unchecked,
                        size: checkboxSize,
                      ),
                      SizedBox(width: 4,),
                      Expanded(child: SelectableText(widget. questionItem.options[index], style: TextStyle(fontSize: 11),)),
                    ],
                  ),
                ),
              );
            })
          )
        ],
      ),

    );
  }
}