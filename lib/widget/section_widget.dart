
import 'package:flutter/material.dart';

class SectionWidget extends StatelessWidget{

  final String title;
  final double size;
  final Color color;
  SectionWidget({this.title, this.size = 14, this.color = Colors.grey});

  @override
  Widget build(BuildContext context) {
    return Container(

      color: this.color,
      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10),
      child: SelectableText(title, style: TextStyle(fontSize: size, color: Colors.black, fontWeight: FontWeight.w500),),
    );
  }
}