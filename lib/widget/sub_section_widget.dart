
import 'package:flutter/material.dart';

class SubSectionWidget extends StatelessWidget{

  final String title;
  final double size;
  final Color color;
  SubSectionWidget({this.title, this.size = 11, this.color = Colors.grey});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10),
      child: SelectableText(title, style: TextStyle(fontSize: size, color: Colors.black),),
    );
  }
}