import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionYNShowTextFieldWidget extends StatefulWidget {
  final Function(int, List<int>) callback;
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final List<String> subs;
  final bool horizontal;
  final List<String> yesno;
  final correctIndex;

  QuestionYNShowTextFieldWidget(

      {
        this.horizontal = false, this.correctIndex = 0,  this.yesno, this.subs, this.title, this.callback, this.width = 120, this.optionWidth = 100, this.hasDecor = false});

  @override
  State createState() {
    return _QuestionYNShowTextFieldWidgetState();
  }
}

class _QuestionYNShowTextFieldWidgetState extends State<QuestionYNShowTextFieldWidget> {
  final List<int> selectedIndexs = [];

  ValueNotifier<int> _yesno = ValueNotifier<int>(-1);
  int correctIndex;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    childs = widget.subs;
    correctIndex = widget.correctIndex;

    if(widget.yesno == null){
      yesno = ['Có', 'Không'];
    }else{
      yesno = widget.yesno;
    }
  }

  int selectedIndex = -1;

  List<String> yesno = ['Có', 'Không'];
  List<String> childs = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: widget.hasDecor ? containerDecoration() : BoxDecoration(),
      constraints: BoxConstraints(
        minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10),
      margin: EdgeInsets.only(top: 4),
      child: widget.horizontal? Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          _mainQuestion(),
          _childsQuestion(),
        ],
      ):  Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _mainQuestion(),
          _childsQuestion(),
        ],
      ),
    );
  }


  _mainQuestion(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          child: SelectableText(widget.title, style: TextStyle(fontSize: 11),),
        ),
        Row(
          children: List.generate(yesno.length, (index) {
            return GestureDetector(
              onTap: () {
                setState(() {
                  selectedIndex = index;
                  _yesno.value = index;
                });
              },
              child: Container(
                margin: EdgeInsets.only(top: 10, right: 10),
                child: Row(
                  children: [
                    selectedIndex == index
                        ? Icon(
                      Icons.radio_button_checked,
                      size: checkboxSize,
                    )
                        : Icon(
                      Icons.radio_button_unchecked,
                      size: checkboxSize,
                    ),
                    SizedBox(
                      width: 4,
                    ),
                    SelectableText(
                      yesno[index],
                      style: TextStyle(fontSize: 11),
                    ),
                  ],
                ),
              ),
            );
          }),
        ),
      ],
    );
  }

  _childsQuestion(){
    return ValueListenableBuilder<int>(
      valueListenable: _yesno,
      builder: (_, value, child) {
        return value == correctIndex && childs != null
            ? Wrap(
          children: List.generate(childs.length, (index) {
            return Container(
              margin: EdgeInsets.only(left: 20),
              child: QuestionTextWidget(questionItem: QuestionItem(title: childs[index]), callback: (value){
                if(childs[index] == 'HC M'){
                  DataFirstDate().ctmStore[0] = value;
                }

                if(childs[index] == 'Hb g/'){
                  DataFirstDate().ctmStore[1] = value;
                }

                if(childs[index] == 'BC (N % L %)'){
                  DataFirstDate().ctmStore[2] = value;
                }

                if(childs[index] == 'TC'){
                  DataFirstDate().ctmStore[3] = value;
                }
                print("Nemo $value");
              },),
            );
          }),
        )
            : Container();
      },
    );
  }
}
