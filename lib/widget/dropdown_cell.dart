import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/widget/style.dart';

class DropDownCell extends StatelessWidget{

  final List<String> options;
  final double height;

  DropDownCell({this.options, this.height = 30});

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        decoration: containerDecoration(radius: 0, borderColor: Colors.blue),
        height: height,
        alignment: Alignment.centerLeft,
        child: DropdownButton<String>(
          items: options.map((String value) {
            return DropdownMenuItem<String>(
              value: value,
              child: SelectableText(value, style: TextStyle(fontSize: 10),),
            );
          }).toList(),
          onChanged: (_) {},
        ),
      ),
    );
  }
}