import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/set_first_date.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/question_single_widget.dart';
import 'package:flutter_app/widget/question_text_widget.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionMultiTextFieldWidget extends StatefulWidget {
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final List<String> subs;
  final bool horizontal;

  QuestionMultiTextFieldWidget(

      {
        this.horizontal = false, this.subs, this.title, this.width = 120, this.optionWidth = 100, this.hasDecor = false});

  @override
  State createState() {
    return _QuestionMultiTextFieldWidgetState();
  }
}

class _QuestionMultiTextFieldWidgetState extends State<QuestionMultiTextFieldWidget> {


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    childs = widget.subs;

  }

  int selectedIndex = -1;

  List<String> yesno = ['Có', 'Không'];
  List<String> childs = [];

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: widget.hasDecor ? containerDecoration() : BoxDecoration(),
      constraints: BoxConstraints(
        minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0, horizontal: 10),
      margin: EdgeInsets.only(top: 4),
      child: widget.horizontal? Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text(widget.title, style: TextStyle(fontSize: 11),),
          ),
          _childsQuestion(),
        ],
      ):  Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            alignment: Alignment.centerLeft,
            child: Text(widget.title, style: TextStyle(fontSize: 11),),
          ),
          _childsQuestion(),
        ],
      ),
    );
  }

  _childsQuestion(){
    return Wrap(
      children: List.generate(childs.length, (index) {
        return Container(
          margin: EdgeInsets.only(left: 20),
          child: QuestionTextWidget(questionItem: QuestionItem(title: childs[index]), callback: (value){
            if(childs[index] == 'HC M'){
              DataFirstDate().ctmStore[0] = value;
            }

            if(childs[index] == 'Hb g/'){
              DataFirstDate().ctmStore[1] = value;
            }

            if(childs[index] == 'BC (N % L %)'){
              DataFirstDate().ctmStore[2] = value;
            }

            if(childs[index] == 'TC'){
              DataFirstDate().ctmStore[3] = value;
            }
            print("Nemo $value");
          },),
        );
      }),
    );
  }
}
