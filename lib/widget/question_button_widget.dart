import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionButtonWidget extends StatelessWidget {
  final Function callback;
  final QuestionItem questionItem;

  QuestionButtonWidget({@required this.questionItem, this.callback});

  @override
  Widget build(BuildContext context) {
    return FlatButton(
      onPressed: callback,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
        constraints: BoxConstraints(minWidth: 120, maxWidth: 150),
        padding: EdgeInsets.symmetric(vertical: 6.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SelectableText(
              questionItem.title,
              style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),
            ),
            Container(
              width: 120,
              decoration: containerDecoration(borderColor: Colors.grey),
              padding: EdgeInsets.symmetric(horizontal: 12, vertical: 8),
              child: Text(
                "Tính",
                style: TextStyle(fontSize: 11),
              ),
            )
          ],
        ),
      ),
    );
  }
}
