import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app/data/question_item.dart';
import 'package:flutter_app/data/sizes.dart';
import 'package:flutter_app/data/strings.dart';
import 'package:flutter_app/widget/style.dart';

class QuestionCheckShowMultipleWidget extends StatefulWidget{

  final Function(int, List<int>) callback;
  final double width, optionWidth;
  final bool hasDecor;
  final String title;
  final List<String> options;
  final bool showParentCheck;


  QuestionCheckShowMultipleWidget({this.showParentCheck = true, this.options, this.title, this.callback, this.width = 120, this.optionWidth = 100, this.hasDecor = false});

  @override
  State createState() {
    return QuestionCheckShowMultipleWidgetState();
  }
}

class QuestionCheckShowMultipleWidgetState extends State<QuestionCheckShowMultipleWidget>{

  final List<int> selectedIndexs = [];

  ValueNotifier<bool> _parentCheck = ValueNotifier<bool>(false);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    if(widget.showParentCheck == false){
      _parentCheck.value = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: widget.hasDecor? containerDecoration(): BoxDecoration(),
      constraints: BoxConstraints(
          minWidth: widget.width,
      ),
      padding: EdgeInsets.symmetric(vertical: 6.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          GestureDetector(
            onTap: (){
              _parentCheck.value = !_parentCheck.value;
            },
            child: Row(
              children: [

                widget.showParentCheck?
                Row(
                  children: [
                    ValueListenableBuilder<bool>(
                      valueListenable: _parentCheck,
                      builder: (_, value, child){
                        return value? Icon(Icons.check_box, size: checkboxSize,): Icon(Icons.check_box_outline_blank, size: checkboxSize,);
                      },
                    ),
                    SizedBox(width: 4,),
                  ],
                ): Container(),

                widget.title.isEmpty? Container(): Expanded(child: SelectableText(widget.title, style: TextStyle(fontSize: 11, color: Colors.black87, fontWeight: FontWeight.w400),)),
              ],
            ),
          ),

          Container(
            margin: EdgeInsets.only(left: 40),
            child: ValueListenableBuilder<bool>(
              valueListenable: _parentCheck,
              builder: (_, value, child){
                return value? Wrap(
                  children: List.generate(widget.options.length, (index) {
                    return GestureDetector(
                      onTap: (){

                        setState(() {
                          if(selectedIndexs.contains(index)){
                            selectedIndexs.remove(index);
                          }else{
                            selectedIndexs.add(index);
                          }
                        });

                        widget.callback(index, selectedIndexs);
                      },
                      child: Container(
                        width: widget.optionWidth,
                        margin: EdgeInsets.only(right: 20),
                        child: Row(
                          children: [
                            selectedIndexs.contains(index)? Icon(Icons.check_box, size: checkboxSize,): Icon(Icons.check_box_outline_blank, size: checkboxSize,),
                            SizedBox(width: 4,),
                            Expanded(child: SelectableText(widget.options[index], style: TextStyle(fontSize: 11),)),
                          ],
                        ),
                      ),
                    );
                  }),
                ):Container();
              },
            ),
          )
        ],
      ),

    );
  }
}